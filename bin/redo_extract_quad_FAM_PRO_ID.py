#	input:	the work folder which contains a PED subfolder where all family PED files were copied
#	output:	only for quad families
#		FAM_IDs.txt and FAM_PRO.txt
#
#	checks that the there are exactly two kids in the PED file and both are affected
#	checks that the there are exactly two parents in the PED file and both are unafefcted
#
#       Author: MH
#       last modified: MAY 06, 2022



import sys
import os
from collections import defaultdict


def go(work_dir):

    out_fam_file = work_dir + '/quad_FAM_IDs.txt'
    out_f_p_file = work_dir + '/quad_FAM_PRO.txt'

    out_fam_han = open(out_fam_file,'w')
    out_f_p_han = open(out_f_p_file,'w')

    cntr_fam = 0
    cntr_f_p = 0


    # go over the PED folder in the working dir and process each PED file
    ped_dir = work_dir + '/PED'
    print ""
    print "Processing the PED files (in %s) to extract FAM_ID and FAM_PRO files" % (ped_dir)

    for file in os.listdir(ped_dir):					# per each PED file
        if file.endswith(".ped"):
#            print(os.path.join(ped_dir, file))

            print "  %s" % (file)
            in_file = os.path.join(ped_dir, file)
            in_han = open(in_file,'r')

            FAM_PRO_DICT = defaultdict(list)		# key: family_id: value: list of affected proband IDs
            AFF_PROBANDS = []
            UNAFF_PARENTS = []

            for line in in_han:
                data = [x.strip() for x in line.strip().split('\t')]

                x_plate,x_fam = data[0].split('_')			# in the internal PED file, family_id is plateID_familyID, will keep only clean family_id, which corresponds to DECIPHER ID
                y_indi,y_fam = data[1].split('_')			# in the internal PED file, indi_id is indiID_familyID, split

                if x_fam != y_fam:
                    print "ERROR: family ID mismatch"
                    print line
                    raise SystemExit

                pro_fam_id = data[1]
                par_1 = data[2]
                par_2 = data[3]
                aff = int(data[5])

                if (par_1 != "0") and (par_2 != "0"):   # a proband
                    if aff != 2:                        # not affected proband
                        print "ERROR: Found unaffected proband"
                        print line
                        raise SystemExit
                    else:
                        CHILD_ID = y_indi
                        FAM_ID = y_fam
                        FAM_PRO_DICT[FAM_ID].append(CHILD_ID)
                        AFF_PROBANDS.append(pro_fam_id)

                elif ((par_1 == "0") and (par_2 != "0")) or ((par_1 != "0") and (par_2 == "0")):   # a proband with one parent missing
                    if aff == 2:                        # affected proband
                        print "WARNING: Found affected proband with one missing parent"
                        print "MUST edit the PED file = %s" % (in_file)
                        print line
                        CHILD_ID = y_indi
                        FAM_ID = y_fam
                        FAM_PRO_DICT[FAM_ID].append(CHILD_ID)
                        AFF_PROBANDS.append(pro_fam_id)
                    else:
                        print "ERROR: found an unaffected proband (also missing one parent)"
                        print line
                        raise SystemExit


                elif (par_1 == "0") and (par_2 == "0"):   # a parent
                    if aff != 1:                          # affected parent
                        print "ERROR: Found affected parent"
                        print line
                        raise SystemExit
                    else:
                        UNAFF_PARENTS.append(pro_fam_id)

                else:						# most likely half-sibling, see familyID = 451427, plateID = 19400
                    print "ERROR: cannot determine if proband/parent"
                    print line
                    raise SystemExit




            in_han.close()


            if len(AFF_PROBANDS) != 2:
                print "ERROR: Could not find exactly 2 affected probands"
                raise SystemExit

            if len(UNAFF_PARENTS) != 2:
                print "ERROR: Could not find exactly 2 unaffected parents"
                raise SystemExit

            # passed all checks, write it out
            out_fam_han.write('%s\n' % (list(FAM_PRO_DICT)[0]))
            cntr_fam += 1

            CHILD_IDS = FAM_PRO_DICT[list(FAM_PRO_DICT)[0]]
            out_f_p_han.write('%s\t%s\n' % (list(FAM_PRO_DICT)[0],CHILD_IDS))
            cntr_f_p += 1

    out_fam_han.close()
    out_f_p_han.close()


    print ""
    print "Quad Families:"
    print "   %s FAM_IDs --> %s" % (cntr_fam, out_fam_file)
    print "   %s FAM_PRO --> %s" % (cntr_f_p, out_f_p_file)
    print ""




if __name__ == '__main__':
    if len(sys.argv) == 2:
        go(sys.argv[1])
    else:
        print "Suggested use: time python /home/u035/u035/shared/scripts/extract_quad_FAM_PRO_ID.py /home/u035/u035/shared/analysis/work/<PROJECT_ID>"
        raise SystemExit

