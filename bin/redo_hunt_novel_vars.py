#	Given the previously generated DECIPHER upload file
#	and the DECIPHER upload file generated as a result of this reanalysis
#	check if new variants are discovered by the reanalysis
#	and if so, record them in a novel_<indi_id>_<family_id>_DECIPHER_v11.xlsx file
#
#
#	OLD DECIPHER file formats
#       -------------------------
#	Based on the email from Louise on 21/08/2023
#	these are the old file format, listed in descending order of time and preference
#	INDI_FAM_DECIPHER_v11.xlsx
#	INDI_FAM_DECIPHER_v10.xlsx
#	INDI_FAM_DEC_FLT.csv
#	This will also be the picking order for Alison when she copies the previous_DECIPHER_files
#
#
#	NEW DECIPHER file formats
#	-------------------------
#	At the moment, we are returning DECIPHER v11 files for NHS upload
#	WIll hard-code this extention, *********must change************ if DECIPHER version changes
#
#
#       Author: MH
#       last modified: AUG 21, 2023






import sys
import os
import shutil
import csv
import xlsxwriter
import xlrd


def go(old_DEC_DIR,new_DEC_DIR,INDI_ID,FAMILY_ID):

    INDI_FAM = "%s_%s" % (INDI_ID,FAMILY_ID)

    # establish the old file
    old_xlsx_11 = "%s_DECIPHER_v11.xlsx" % (INDI_FAM)
    old_xlsx_10 = "%s_DECIPHER_v10.xlsx" % (INDI_FAM)
    old_csv = "%s_DEC_FLT.csv" % (INDI_FAM)

    if os.path.exists("%s/%s" % (old_DEC_DIR,old_xlsx_11)):
        old_DEC_file = "%s/%s" % (old_DEC_DIR,old_xlsx_11)
    elif os.path.exists("%s/%s" % (old_DEC_DIR,old_xlsx_10)):
        old_DEC_file = "%s/%s" % (old_DEC_DIR,old_xlsx_10)
    elif os.path.exists("%s/%s" % (old_DEC_DIR,old_csv)):
        old_DEC_file = "%s/%s" % (old_DEC_DIR,old_csv)
    else:
        print ('ERROR: cannot establish old DECIPHER files for ',INDI_FAM)
        raise SystemExit

    # establish the new file - hard-code the DECIPHER filename - to be changed if there is any change in the DECIPHER upload file format!!!!
    new_DEC_file = '%s/%s_DECIPHER_v11.xlsx' % (new_DEC_DIR,INDI_FAM)

    print ('old decipher file = ',old_DEC_file)
    print ('new decipher file = ',new_DEC_file)



    # based on the version of the old decipher file
    # read its info differently
    # and store the variant info in old_keys
    old_keys = {}

    if old_DEC_file.endswith('.csv'):
        with open(IN_DEC_FILE,'r') as tsvfile:
            reader = csv.reader(tsvfile, delimiter=',', quotechar='"')
            for row in reader:
                if row[0] == 'Internal reference number':      # ignore the header line
                    continue

            # if here, not the header, parse the row info and check if an OK variant
            chr = str(row[1])
            start = str(row[2])
            ref = str(row[4])
            alt = str(row[5])
            key = (chr,pos,ref,alt)
            old_keys[key] = 1



    else:	# i.e. xlsx file
        old_wb = xlrd.open_workbook(old_DEC_file)
        old_ws = old_wb.sheet_by_index(0)
        for i in range(1, old_ws.nrows):
            chr = old_ws.cell_value(i,4)
            pos = old_ws.cell_value(i,5)
            ref = old_ws.cell_value(i,6)
            alt = old_ws.cell_value(i,7)
            key = (chr,pos,ref,alt)
            old_keys[key] = 1




    # create a DECIPHER xlsx (v11) file to store any novel variants found during the reanalysis
    NOVEL_DEC_FILE = "%s/novel_%s_%s_DECIPHER_v11.xlsx" % (new_DEC_DIR,INDI_ID,FAMILY_ID)

    # create the workbook
    novel_workbook = xlsxwriter.Workbook(NOVEL_DEC_FILE)

    # create the worksheet
    novel_worksheet = novel_workbook.add_worksheet('Sequence Variants')

    # create and write the header (v11 format)
    header = ('Patient internal reference number or ID','Shared','Assembly','HGVS code','Chromosome','Genomic start','Ref sequence','Alt sequence','Gene name','Transcript','Is intergenic','Genotype','Inheritance','Pathogenicity','Pathogenicity evidence','Contribution','Genotype groups')
    cntr = 0
    novel_worksheet.write_row(cntr,0,header)



    # now, open and iterate over the new DECIPHER file and check if it contains any novel variants compared to the old DECIPHER file
    new_wb = xlrd.open_workbook(new_DEC_file)
    new_ws = new_wb.sheet_by_index(0)

    for i in range(1, new_ws.nrows):
        id = new_ws.cell_value(i,0)
        shared = new_ws.cell_value(i,1)
        assembly = new_ws.cell_value(i,2)
        HGVS = ''
        chr = new_ws.cell_value(i,4)
        pos = new_ws.cell_value(i,5)
        ref = new_ws.cell_value(i,6)
        alt = new_ws.cell_value(i,7)
        key = (chr,pos,ref,alt)
        if key not in old_keys:
            print ('Found a novel variant ', key, 'in' , INDI_FAM)
            gene = new_ws.cell_value(i,8)
            trans = new_ws.cell_value(i,9)
            inter = new_ws.cell_value(i,10)
            genotype = new_ws.cell_value(i,11)
            inher = new_ws.cell_value(i,12)
            patho = ''
            evid = ''
            cont = ''
            gt_groups = ''
            data = (id,shared,assembly,HGVS,chr,pos,ref,alt,gene,trans,inter,genotype,inher,patho,evid,cont,gt_groups)

            # write it
            cntr += 1
            novel_worksheet.write_row(cntr,0,data)

    # close the workbook (the new DECIPHER_v11 file)
    novel_workbook.close()
    print ('Novel DECIPHER file = ',NOVEL_DEC_FILE)









if __name__ == '__main__':
    if len(sys.argv) == 5:
        go(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        print ('Suggested use: time python3 /home/u035/u035/shared/rerun_bulk/hunt_novel_vars.py previous_DECIPHER_upload redo_decipher_folder INDI_ID FAMILY_ID')
        raise SystemExit

