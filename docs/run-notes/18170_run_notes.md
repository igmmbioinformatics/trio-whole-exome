# Run 18170

There are 5 samples on this run that failed on run 18840, all are from different families. We’ve put the whole family into the PED file. The rest of the family’s data are on run 18840. The repeated samples on this run are:

* 125778 (family 428004)
* 125467 (family 427734)
* 101846 (family 429213)
* 125303 (family 427554)
* 125783 (family 428055)

Other analysis notes:
* 430321 – Quad, 2 affected children. Please perform trio-based analysis per child, the 2 are not similarly affected.
* 429107 – Quad, 2 similarly affected children. Please perform shared variant analysis between 88303 and 80901, and trio-based analysis per child.
* 429453 – Trio, mother & child similarly affected. Please perform trio-based analysis and shared variant analysis between 126602 and 126605.
* 429187 – Duo, for Congenica analysis only.

## Read set up

```
cd /scratch/u035/project/trio_whole_exome/analysis/reads
mkdir 18170_Ansari_Morad
cd 18170_Ansari_Morad

cp ../17710_Ansari_Morad/*428004* ./
cp ../17710_Ansari_Morad/*427734* ./
cp ../17710_Ansari_Morad/*429213* ./
cp ../17710_Ansari_Morad/*427554* ./
cp ../17710_Ansari_Morad/*428055* ./

ls -lh 125778_428004*
ls -lh 125467_427734*
ls -lh 101846_429213*
ls -lh 125303_427554*
ls -lh 125783_428055*

rm 125778_428004*
rm 125467_427734*
rm 101846_429213*
rm 125303_427554*
rm 125783_428055*
```

## Parameter file generation

```
cd /scratch/u035/project/trio_whole_exome/analysis/params

version=v1
project_id=18170_Ansari_Morad
sample_suffix=_WESTwist_IDT-A
ped_file=18957_WES_PED_110621.txt 

ln -s $ped_file $project_id.ped
```

Edit the parameter generation script to exit after running the Perl script for producing the PED files.

```
/home/u035/project/scripts/prepare_bcbio_config.sh \
   /home/u035/project/scripts/trio_whole_exome_config.sh \
   $project_id $version $sample_suffix &> ${version}_${project_id}.log
```

Copy the PED files for the above families from the previous run and edit the batch id.

```
for family in 428004 427734 429213 427554 428055
do
  cp 17710_Ansari_Morad_18840_${family}.ped 18170_Ansari_Morad_18957_${family}.ped
  perl -pi -e 's/18840/18957/' 18170_Ansari_Morad_18957_${family}.ped
done
```

Re-generate the family ids list.

```
cut -f 1 /scratch/u035/project/trio_whole_exome/analysis/params/18170_Ansari_Morad.ped | sort -u  | awk '{ print "18957_" $0 }' > 18170_Ansari_Morad.family_ids.txt
```

Edit the parameter generation script to skip running the Perl script for producing the PED files.

```
/home/u035/project/scripts/prepare_bcbio_config.sh \
   /home/u035/project/scripts/trio_whole_exome_config.sh \
   $project_id $version $sample_suffix &> ${version}_${project_id}.log
```

Uncomment out the skipped section of the parameter generation script.

Fix the config files for these families.

```
for family in 428004 427734 429213 427554 428055
do
  cp v1_17710_Ansari_Morad_18840_${family}.yaml v1_18170_Ansari_Morad_18957_${family}.yaml
  perl -pi -e 's/18840/18957/' v1_18170_Ansari_Morad_18957_${family}.yaml
  perl -pi -e 's/17710/18170/' v1_18170_Ansari_Morad_18957_${family}.yaml
done
```
