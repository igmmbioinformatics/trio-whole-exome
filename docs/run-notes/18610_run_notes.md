# EdGe run 18610 (batch 19082)

No repeats from previous runs.

* Family 433675 – this is a similarly affected duo, please perform shared variant analysis between the 2
* Family 433108 – please perform trio analysis and shared variant analysis between proband and father
* Family 434759 – this is a quad, please perform shared analysis between 2 sibs and trio analysis per-sib.

## For Congenica analysis only:

* Family 430428 (duo)
* Family 50232 (singleton)
