#!/usr/bin/perl -w

=head1 NAME

add_plate_and_family_id_to_ped.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Prepends <plate_id>_ to family id column of a PED file, and
appends <family_id> to individual ids in a PED file.

=cut

use strict;

use Getopt::Long;
use IO::File;

my $usage = qq{USAGE:
$0 [--help]
  --plate_id  Plate id prefix
  --family_id Family id suffix
};

my $help = 0;
my $plate_id;
my $family_id;

GetOptions(
    'help'        => \$help,
    'plate_id=s'  => \$plate_id,
    'family_id=s' => \$family_id
) or die $usage;

if ($help || !$plate_id || !$family_id)
{
    print $usage;
    exit(0);
}

# Read in the PED file
while (my $line = <>)
{
	chomp $line;

	my @tokens = split(/\t/, $line);
	$tokens[0] = $plate_id . "_" . $tokens[0];

	for (my $i = 1; $i <= 3; $i++)
	{
		if ($tokens[$i] ne "0" && $tokens[$i] ne "-1")
		{
			$tokens[$i] = $tokens[$i] . "_" . $family_id;
		}
	}
	printf "%s\n", join("\t", @tokens);
}

