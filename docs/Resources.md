# Resource files on sdf-cs1 (ultra2)

# Exome targets

The TWIST target BED file is at: `/home/u035/u035/shared/resources/exome_targets/Twist_Exome_RefSeq_targets_hg38.plus15bp.bed`

To generate the target BED file, first copy the file `Twist_Exome_RefSeq_targets_hg38.bed` from NHS Clinical Genetics Services to `/home/u035/u035/shared/resources/exome_targets`, then pad it by 15bp each side.

```
cd /home/u035/u035/shared/resources/exome_targets
PATH=/home/u035/u035/shared/software/bcbio/tools/bin:$PATH

bedtools slop -g $REFERENCE_GENOME.fai -i Twist_Exome_RefSeq_targets_hg38.bed -b 15 | \
  bedtools merge > Twist_Exome_RefSeq_targets_hg38.plus15bp.bed
```

## Other exome targets

Retained for reference in `/home/u035/u035/shared/resources/exome_targets/archive`.

## Resources for variant prioritization

See [Setup for variant prioritization](Setup_variant_prioritization.md).
