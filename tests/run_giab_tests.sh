#!/bin/bash

source scripts/nextflow_detached.sh

test_exit_status=0
nextflow -c "$NEXTFLOW_CONFIG" clean -f

common_args="-ansi-log false -c $NEXTFLOW_CONFIG -profile testing -resume"

echo "--- Reduced GiaB data: trios ---" &&
echo Variant calling &&
nextflow ../main.nf \
    $common_args \
    --workflow variant_calling \
    --pipeline_project_id giab_test_trios \
    --pipeline_project_version v1 \
    --ped_file assets/input_data/ped_files/giab_test_trios.ped \
    --sample_sheet assets/input_data/sample_sheets/giab_test_trios.tsv &&

echo Variant prioritisation setup &&
nextflow ../main.nf \
    $common_args \
    --workflow variant_prioritisation_setup \
    --pipeline_project_id giab_test_trios \
    --variant_prioritisation_version v1 \
    --ped_file assets/input_data/ped_files/giab_test_trios.ped &&

cp assets/input_data/sample_sheets/DECIPHER_INTERNAL_IDs_giab_trios.txt outputs/giab_test_trios_v1/prioritization/v1 &&

echo Variant prioritisation &&
nextflow run ../main.nf \
    $common_args \
    --workflow variant_prioritisation \
    --pipeline_project_id giab_test_trios \
    --variant_prioritisation_version v1 \
    --ped_file assets/input_data/ped_files/giab_test_trios.ped &&
    
echo Cram compression &&
nextflow run ../main.nf \
    $common_args \
    --workflow cram_compression \
    --compress_dir outputs/giab_test_trios_v1

test_exit_status=$(( $test_exit_status + $? ))

echo "--- Reduced GiaB data: non-trios ---" &&
nextflow run ../main.nf \
    $common_args \
    --workflow variant_calling \
    --pipeline_project_id giab_test_non_trios \
    --pipeline_project_version v1 \
    --ped_file $PWD/assets/input_data/ped_files/giab_test_non_trios.ped \
    --sample_sheet $PWD/assets/input_data/sample_sheets/giab_test_non_trios.tsv

test_exit_status=$(( $test_exit_status + $? ))

echo "Tests finished with exit status $test_exit_status"

