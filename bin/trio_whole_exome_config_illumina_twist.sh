#!/usr/bin/bash
#
# Basic configuration options for trio WES pipeline
#

# primary locations
BASE=/home/u035/u035/shared
SCRIPTS=$BASE/scripts/bin
DOWNLOAD_DIR=$BASE/data
OUTPUT_DIR=$BASE/results

# resource locations
BCBIO_TEMPLATE=$BASE/scripts/assets/trio_whole_exome_bcbio_template.yaml
TARGET=$BASE/resources/exome_targets/hg38_Twist_ILMN_Exome_2.0_Plus_Panel_annotated_June2022.plus15bp.bed
REFERENCE_GENOME=$BASE/software/bcbio/genomes/Hsapiens/hg38/seq/hg38.fa

# temporary working files
PARAMS_DIR=$BASE/analysis/params
READS_DIR=$BASE/analysis/reads
CONFIG_DIR=$BASE/analysis/config
WORK_DIR=$BASE/analysis/work
LOGS_DIR=$BASE/analysis/logs

export PATH=$BASE/software/bcbio/tools/bin:$PATH
