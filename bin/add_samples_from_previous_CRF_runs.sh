#!/bin/bash
#
# add_samples_from_previous_runs.sh <config.sh> <project_id> <version> <samples>
# 
#

CONFIG_SH=$1
PROJECT_ID=$2
VERSION=$3
SAMPLES=$4

source $CONFIG_SH

cd $PARAMS_DIR

# create reads directory for project and symlink directory underneath
mkdir -p $READS_DIR/$PROJECT_ID/symlinks

SHORT_PROJECT_ID=`echo $PROJECT_ID | cut -f 1 -d '_'`

N=`wc -l $SAMPLES | awk '{ print $1 }'`

for ((i = 1; i <= $N; i = i + 1))
do
  FAMILY_ID=`head -n $i $SAMPLES | tail -n 1 | cut -f 1`
  SAMPLE=`head -n $i $SAMPLES | tail -n 1 | cut -f 2`
  SEX=`head -n $i $SAMPLES | tail -n 1 | cut -f 3`
  PHENOTYPE=`head -n $i $SAMPLES | tail -n 1 | cut -f 4`
  ORIGINAL_PROJECT_ID=`head -n $i $SAMPLES | tail -n 1 | cut -f 5`
  ORIGINAL_SAMPLE_ID=`head -n $i $SAMPLES | tail -n 1 | cut -f 6`
  
  PREFIX=${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}
  
  for FILE in `ls $DOWNLOAD_DIR/$ORIGINAL_PROJECT_ID/*${ORIGINAL_SAMPLE_ID}*.gz`
  do
    echo "$FILE,$SAMPLE,$FAMILY_ID,$SEX,$PHENOTYPE,$TARGET" >> $PREFIX.csv
  done

done
