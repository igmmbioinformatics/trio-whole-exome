#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=2GB
#SBATCH --time=6:00:00
#SBATCH --job-name=trio_whole_exome_project_checksums
#SBATCH --output=trio_whole_exome_project_checksums.%A.out
#SBATCH --error=trio_whole_exome_project_checksums.%A.err

# Expects environment variables to be set
# PROJECT_ID - e.g. 12345_LastnameFirstname
# VERSION - e.g. v1, v2
# CONFIG_SH - absolute path to configuration script setting environment variables

source $CONFIG_SH

# calculate checksums on all files for this project except the families directory

SHORT_PROJECT_ID=`echo $PROJECT_ID | cut -f 1 -d '_'`

cd $OUTPUT_DIR/${SHORT_PROJECT_ID}_${VERSION}

rm md5sum.txt 2> /dev/null

for file in `find . -type f | grep -v families`
do
  md5sum $file >> md5sum.txt
done
