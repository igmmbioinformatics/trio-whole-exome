#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=2GB
#SBATCH --time=01:00:00
#SBATCH --job-name=redo_gather_solo_results
#SBATCH --output=redo_gather_solo_results.%A_%a.out
#SBATCH --error=redo_gather_solo_results.%A_%a.err


#       Author: MH
#       last modified: AUG 29, 2023



SCRIPTS_DIR=/home/u035/u035/shared/scripts/bin
#^#SCRIPTS_DIR=/home/u035/u035/shared/rerun_bulk


PYTHON3=/home/u035/u035/shared/software/bcbio/anaconda/bin/python3



# check input given to script
echo "INPUT_DIR = ${INPUT_DIR}"         # the path to the bulk reanalysis folder with the links to source VCF and PED files,
                                        # e.g. /home/u035/u035/shared/results/20220418_reanalysis
echo "PROJECT_ID = ${PROJECT_ID}"       # this the the folder (${BASE}/${PROJECT_ID}) where the prioritization analysis will be done




### folder structure for the downstream analysis - created by redo_setup_solo.sh ###
BASE=/home/u035/u035/shared/analysis/work
WORK_DIR=${BASE}/${PROJECT_ID}
NHS_DIR=${WORK_DIR}/results
#^#BAM_DIR=${WORK_DIR}/BAM





# other files to be used
FAMILY_IDS=${WORK_DIR}/solo_FAM_IDs.txt			# created by redo_setup_solo.sh
CHILD_IDS=${WORK_DIR}/solo_PRO_IDs.txt			# created by redo_setup_solo.sh


# check if ${NHS_DIR} already exists - if not, exit and ask to be created
if [ ! -d "${NHS_DIR}" ]; then
  echo "${NHS_DIR} does not exist - need to create it before running this script!!!!"
  exit
fi


FAMILY_ID=`head -n ${SLURM_ARRAY_TASK_ID} ${FAMILY_IDS} | tail -n 1`				# contains only the family IDs (e.g.385295)
PROBAND_ID=`head -n ${SLURM_ARRAY_TASK_ID} ${CHILD_IDS} | tail -n 1`				# contains only the proband IDs (e.g. 107060)


# split the variants to OK and artefacts and clean the DECIPHER upload file
time ${PYTHON3} ${SCRIPTS_DIR}/redo_split_trio_solo_variants.py ${WORK_DIR}/DECIPHER ${PROBAND_ID} ${FAMILY_ID}


# create the family folder for the results
FAM_DIR=${NHS_DIR}/${FAMILY_ID}
mkdir ${FAM_DIR}


# copy the DECIPHER-to-INTERNAL ID mapping
cp ${WORK_DIR}/solo_DECIPHER_INTERNAL_IDs.txt ${FAM_DIR}


# copy the LOG files
cp ${WORK_DIR}/LOG/redo_solo.*_${SLURM_ARRAY_TASK_ID}.err ${FAM_DIR}
cp ${WORK_DIR}/LOG/redo_solo.*_${SLURM_ARRAY_TASK_ID}.out ${FAM_DIR}


# copy the G2P family html report
cp ${WORK_DIR}/G2P/${FAMILY_ID}_LOG_DIR/${FAMILY_ID}.report.html ${FAM_DIR}


# copy the DECIPHER file for bulk upload
#cp ${WORK_DIR}/DECIPHER/${PROBAND_ID}_${FAMILY_ID}_DEC_FLT.csv ${FAM_DIR}
#cp ${WORK_DIR}/DECIPHER/${PROBAND_ID}_${FAMILY_ID}_DECIPHER_v10.xlsx ${FAM_DIR}
cp ${WORK_DIR}/DECIPHER/${PROBAND_ID}_${FAMILY_ID}_DECIPHER_v11.xlsx ${FAM_DIR}


# copy the variant snapshots
cp ${WORK_DIR}/DECIPHER/IGV/${FAMILY_ID}/*.png ${FAM_DIR}
cp -r ${WORK_DIR}/DECIPHER/IGV/${FAMILY_ID}/artefacts ${FAM_DIR}/


# copy proband coverage files
cp ${WORK_DIR}/COV/${PROBAND_ID}_${FAMILY_ID}.DD15.COV.txt ${FAM_DIR}


# to make the script usable for both DD and Fetal (for which we do not compute coverage over recurrent variants) panels
REC_FILE=${WORK_DIR}/COV/${PROBAND_ID}_${FAMILY_ID}.REC_SNP_COV.txt
if [ -f "${REC_FILE}" ]; then
    cp ${REC_FILE} ${FAM_DIR}
fi


# gather the hard-filtered SB variants
#cp ${WORK_DIR}/VCF/${FAMILY_ID}.SB_DDG2P.vcf.gz ${FAM_DIR}




echo ""
echo ""
echo "OK: Results for ${FAMILY_ID} are stored in ${FAM_DIR}"

