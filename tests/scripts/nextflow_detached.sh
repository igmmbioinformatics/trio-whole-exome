
function run_nextflow {
    # Run NextFlow in the background, wait for termination and return an exit status based on
    # error occurrence. We have to do this because NextFlow doesn't like CI environments with
    # detached stdin/stdout.

    nextflow run -bg $*
    nextflow_pid=$(cat .nextflow.pid)

    while kill -0 $nextflow_pid 2> /dev/null
    do
        sleep 1
    done
    rm .nextflow.pid

    # janky, but couldn't find any other way of getting the exit status
    return "$(grep -c ERROR .nextflow.log)"
}
