* Families 451528, 452965 and 453088 are all straightforward trios.
* Family 423278 is the complicated one – we previously sequenced samples 123747 (proband), 123745 (dad), 123746 (mum) on run 18422. We have now received a similarly affected sibling, sample 131699, which is going on this run. Would it be possible to perform a shared analysis between 123747 and 131699 please? As I’m writing this, I realise that run 18422 was performed in December 2020, so I’m not sure how easy it’s going to be to access this data?
* Family 438810 is a duo for Congenica only (we just put this on to fill the run).

Initial run - 3 trios + duo (for QC only)

Quad retrieved from archive and run.
