#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=4GB
#SBATCH --time=24:00:00
#SBATCH --job-name=get_data
#SBATCH --output=get_data.%A_%a.out
#SBATCH --error=get_data.%A_%a.err


# setup the connection
PROJ_CONN="ftps://transfer.genomics.ed.ac.uk/${TOKEN}/."
echo ${PROJ_CONN}

## set up an EPCC folder for this project
#mkdir /home/u035/u035/shared/data/$PROJECT
#cd /home/u035/u035/shared/data/$PROJECT
cd /home/u035/u035/shared/data

# download the data
#wget -crnH --cut-dirs=1 -i - <<<'ftps://transfer.genomics.ed.ac.uk/${TOKEN}/.'
wget -crnH --cut-dirs=1 -i - <<<${PROJ_CONN}



# perform the md5_check
cd /home/u035/u035/shared/data/$PROJECT/raw_data

rm md5_check.txt 2> /dev/null
for DATE in 20*[0-9]
do
  cd $DATE
  md5sum --check md5sums.txt >> ../md5_check.txt
  cd ..
done
