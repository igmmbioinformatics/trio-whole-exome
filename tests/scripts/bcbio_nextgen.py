#!/usr/bin/env python
"""
Fake BCBio for testing with the empty datasets from tests/assets/
"""

import argparse

def main():
    a = argparse.ArgumentParser()
    a.add_argument('positionals', nargs='+')
    a.add_argument('-w')
    a.add_argument('-n', type=int, default=16)
    a.add_argument('-t', default='local')
    args = a.parse_args()

    if args.w:
        open(args.positionals[1].split('-')[0] + '.yaml', 'w').close()


if __name__ == '__main__':
    main()
