nextflow.enable.dsl = 2

include {read_inputs} from './inputs.nf'
include {validation} from './validation.nf'


process merge_fastqs {
    label 'small_medium'

    input:
    tuple(val(indv_id), val(family_id), path(r1), path(r2))

    output:
    tuple(
        val(family_id),
        path("${indv_id}_R1.fastq.gz"),
        path("${indv_id}_R2.fastq.gz")
    )

    script:
    // todo: pigz if gzip becomes a bottleneck
    """
    zcat ${r1.join(' ')} | gzip -c > ${indv_id}_R1.fastq.gz &
    zcat ${r2.join(' ')} | gzip -c > ${indv_id}_R2.fastq.gz
    """
}

process write_bcbio_csv {
    label 'small'

    input:
    tuple(val(family_id), val(individual_info))
    path(target_bed)

    output:
    tuple(val(family_id), path("${family_id}.csv"))

    script:
    """
    #!/usr/bin/env python3
    import os

    target_bed = os.path.realpath('${target_bed}')
    individual_info = '$individual_info'
    lines = individual_info.lstrip('[').rstrip(']').split('], [')

    with open('${family_id}.csv', 'w') as f:
        f.write('samplename,description,batch,sex,phenotype,paternal_id,maternal_id,variant_regions\\n')
        for l in lines:
            f.write(l.replace(', ', ',') + ',' + target_bed + '\\n')
    """
}


process bcbio_family_processing {
    label 'large'
    label 'long'

    input:
    tuple(val(family_id), val(individuals), path(family_csv), path(merged_fastq1), path(merged_fastq2))
    path(bcbio)
    path(bcbio_template)

    output:
    tuple(val(family_id), val(individuals), path("${family_id}"))

    script:
    """
    ${bcbio}/anaconda/bin/bcbio_nextgen.py -w template ${bcbio_template} $family_csv $merged_fastq1 $merged_fastq2 &&
    cd ${family_id}

    # fix numeric ids where underscores have been stripped out

    # family id
    family_id_1=`echo $family_id | cut -f 1 -d '_'`
    family_id_2=`echo $family_id | cut -f 2 -d '_'`
    echo \${family_id_1}\${family_id_2} ${family_id} > id_map.txt

    # individual ids
    for indv_id in `grep -v samplename ../$family_csv | cut -f 1 -d ','`
    do
      indv_id_1=`echo \$indv_id | cut -f 1 -d '_'`
      indv_id_2=`echo \$indv_id | cut -f 2 -d '_'`
      echo \${indv_id_1}\${indv_id_2} \${indv_id} >> id_map.txt
    done

    sample_id_cleanup.pl --map id_map.txt --input config/${family_id}.yaml

    ../${bcbio}/anaconda/bin/bcbio_nextgen.py config/${family_id}.yaml -n $task.cpus -t local
    """

    stub:
    """
    output_dir=${family_id}/results
    family_dir="${family_id}/results/\$(date '+%Y-%m-%d')_${family_id}"
    mkdir -p \$family_dir
    mkdir ${family_id}/{config,provenance}
    touch ${family_id}/config/${family_id}{.csv,.yaml,-template.yaml}
    touch ${family_id}/provenance/programs.txt
    cd \$family_dir
    touch "\$(echo ${family_id} | sed 's/_//g')-gatk-haplotype-annotated.vcf.gz"{,.tbi} bcbio-nextgen{,-commands}.log data_versions.csv
    touch project-summary.yaml metadata.csv
    mkdir multiqc
    touch list_files_final.txt multiqc_config.yaml multiqc_report.html
    mkdir multiqc_data report
    cd ..

    for i in ${individuals.collect().join(' ')}
    do
        mkdir -p \$i/qc
        cd \$i
        touch \$i-{callable.bed,ready.bam,ready.bam.bai}
        cd qc
        mkdir contamination coverage fastqc peddy samtools variants
        touch contamination/\$i-verifybamid.{out,selfSM}
        touch coverage/cleaned-Twist_Exome_RefSeq_targets_hg38.plus15bp-merged-padded.bed
        touch fastqc/{\$i.zip,fastqc_data.txt,fastqc_report.html}
        touch peddy/{\$i.ped_check.csv,\$i.peddy.ped,\$i.sex_check.csv}
        touch samtools/{\$i-idxstats.txt,\$i.txt}
        touch variants/\${i}_bcftools_stats.txt
        cd ../..
    done
    """
}


process format_bcbio_individual_outputs {
    input:
    tuple(val(family_id), val(individuals), path(bcbio_output_dir))
    path(bcbio)
    path(reference_genome)

    output:
    tuple(val(family_id), path('individual_outputs'))

    script:
    """
    samtools=${bcbio}/anaconda/bin/samtools &&
    mkdir individual_outputs
    for i in ${individuals.join(' ')}
    do
        indv_input=\$PWD/${bcbio_output_dir}/results/\$i
        indv_output=individual_outputs/\$i &&
        mkdir -p \$indv_output &&

        for f in \$i-ready.bam \$i-ready.bam.bai \${i}-callable.bed qc
        do
            ln -s \$indv_input/\$f \$indv_output/\$f
        done
    done
    """
}


process format_bcbio_family_outputs {
    label 'small'

    input:
    tuple(val(family_id), val(individuals), path(bcbio_output_dir), path(formatted_individual_outputs))

    output:
    // we don't use bcbio_output_dir here, but we need it later
    tuple(val(family_id), path("*_${family_id}"), path(bcbio_output_dir))

    script:
    """
    bcbio_family_output="\$(ls -d \$(readlink -f $bcbio_output_dir)/results/????-??-??_* | head -n 1)" &&
    run_date="\$(basename \$bcbio_family_output | sed -E 's/^([0-9]{4}-[0-9]{2}-[0-9]{2})_.+\$/\\1/g')" &&

    outdir="\$(basename \$bcbio_family_output | sed 's/-merged\\/*\$//g')" &&
    mkdir \$outdir &&

    for suffix in gatk-haplotype-annotated.vcf.gz{,.tbi}
    do
        src=\$(ls \$bcbio_family_output/*-\${suffix} | head -n 1)
        if [ -f \$src ]
        then
            ln -s \$src \$outdir/${family_id}-\${suffix}
        fi
    done &&

    ln -s \$(readlink -f $bcbio_output_dir)/provenance/programs.txt \$outdir/ &&

    for f in \$bcbio_family_output/*{.log,.csv,.yaml,multiqc}
    do
        ln -s \$f \$outdir/
    done &&

    for i in ${individuals.join(' ')}
    do
        ln -s \$(readlink -f $formatted_individual_outputs)/\$i \$outdir/\$i
    done &&

    cd \$outdir &&
    for f in \$(find . -type f | grep -v '\\.bam')
    do
        md5sum \$f >> md5sum.txt
    done
    """
}


process collate_pipeline_outputs {
    label 'small'

    publishDir "${params.output_dir}", mode: 'move', pattern: "${params.pipeline_project_id}_${params.pipeline_project_version}"

    input:
    val(family_ids)
    val(bcbio_family_output_dirs)
    val(raw_bcbio_output_dirs)
    path(ped_file)
    path(samplesheet)
    path(bcbio)

    output:
    path("${params.pipeline_project_id}_${params.pipeline_project_version}")

    script:
    """
    outputs=${params.pipeline_project_id}_${params.pipeline_project_version}
    mkdir \$outputs &&
    mkdir \$outputs/{config,families,params,prioritization,qc} &&

    for d in ${bcbio_family_output_dirs.join(' ')}
    do
        cp -rL \$d \$outputs/families/\$(basename \$d)
    done &&

    for f in ${family_ids.join(' ')}
    do
        grep \$f ${ped_file} > \$outputs/params/\$f.ped
    done &&

    cd \$outputs/families &&

    # todo: make multiqc its own process
    ../../${bcbio}/anaconda/bin/multiqc \
        --title "Trio whole exome QC report: ${params.pipeline_project_id}_${params.pipeline_project_version}" \
        --outdir ../qc \
        --filename ${params.pipeline_project_id}_${params.pipeline_project_version}_qc_report.html \
        . &&

    peddy_validation_output=../qc/${params.pipeline_project_id}_${params.pipeline_project_version}.ped_check.txt &&
    peddy_validation.pl \
        --output \$peddy_validation_output \
        --ped ../../${ped_file} \
        --families . &&

    # no && here - exit status checked below
#    grep -v 'False\$' \$peddy_validation_output
#    if [ \$? -ne 0 ]
#    then
#        echo "Found Peddy mismatches in \$peddy_validation_output"
#        exit 1
#    fi &&

    cd ../.. &&

    for d in ${raw_bcbio_output_dirs.join(' ')}
    do
        family_id_merged=\$(basename \$d)
        family_id=\$(echo \$family_id_merged | sed 's/-merged//') &&
        dest_basename=${params.pipeline_project_id}_${params.pipeline_project_version}_\$family_id &&
        cp -L \$d/config/\$family_id_merged.csv \$outputs/params/\$dest_basename.csv &&
        cp -L \$d/config/\$family_id_merged.yaml \$outputs/config/\$dest_basename.yaml &&
        cp -L ${ped_file} \$outputs/params/ &&
        cp -L ${samplesheet} \$outputs/params/
    done
    """

    stub:
    """
    outputs=${params.pipeline_project_id}_${params.pipeline_project_version}
    mkdir \$outputs &&
    mkdir \$outputs/{config,families,params,prioritization,qc} &&

    for d in ${bcbio_family_output_dirs.join(' ')}
    do
        cp -rL \$d \$outputs/families/\$(basename \$d)
    done &&

    for f in ${family_ids.join(' ')}
    do
        grep \$f ${ped_file} > \$outputs/params/\$f.ped
    done &&

    cd \$outputs/families &&

    # todo: make multiqc its own process
    echo "Trio whole exome QC report: ${params.pipeline_project_id}_${params.pipeline_project_version}" > ../qc/${params.pipeline_project_id}_${params.pipeline_project_version}_qc_report.html &&

    peddy_validation_output=../qc/${params.pipeline_project_id}_${params.pipeline_project_version}.ped_check.txt &&
    peddy_validation.pl \
        --output \$peddy_validation_output \
        --ped ../../${ped_file} \
        --families . &&

    cd ../.. &&

    for d in ${raw_bcbio_output_dirs.join(' ')}
    do
        family_id_merged=\$(basename \$d)
        family_id=\$(echo \$family_id_merged | sed 's/-merged//') &&
        dest_basename=${params.pipeline_project_id}_${params.pipeline_project_version}_\$family_id &&
        cp -L \$d/config/\$family_id_merged.csv \$outputs/params/\$dest_basename.csv &&
        cp -L \$d/config/\$family_id_merged.yaml \$outputs/config/\$dest_basename.yaml &&
        cp -L ${ped_file} \$outputs/params/ &&
        cp -L ${samplesheet} \$outputs/params/
    done
    """
}


workflow process_families {
    /*
    - For each individual in the family, merge all R1 and R2 fastqs
    - For the family:
      - Read the relevant information from the samplesheet, ped files and merged fastqs
      - Write a BCBio config file
      - Run BCBio on it
      - Move files around into the right places
      - Run CRAM compression, MultiQC and MD5 checks
    */

    take:
        /*
        ch_individuals: [
            [
                indv_id,
                family_id,
                father_id,
                mother_id,
                sex,
                affected,
                [r1_fastqs],
                [r2_fastqs]
            ],
            ...
        ]
        */
        ch_individuals

        // value channels
        ch_ped_file
        ch_samplesheet

    main:
        // collect() can make a queue channel of paths into a value channel
        ch_bcbio = Channel.fromPath(params.bcbio, checkIfExists: true).collect()
        ch_bcbio_template = Channel.fromPath(params.bcbio_template, checkIfExists: true).collect()
        ch_target_bed = Channel.fromPath(params.target_bed, checkIfExists: true).collect()
        ch_reference_genome = Channel.fromPath(params.reference_genome, checkIfExists: true).collect()

        /*
        ch_merged_fastqs: [
            [indv_id, indv_id_merged_r1.fastq.gz, indv_id_merged_r2.fastq.gz],
            ...
        ]
        */
        ch_merged_fastqs = merge_fastqs(
            ch_individuals.map(
                { indv, family, father, mother, sex, affected, r1, r2 ->
                    [ indv, family, r1, r2 ]
            })
        ).groupTuple()

        ch_families = ch_individuals.map(
            { sample_id, family_id, father, mother, sex, phenotype, r1, r2 ->
                [family_id, [sample_id, sample_id, family_id, sex, phenotype, father, mother]]
            }
        ).groupTuple()

        ch_indv_by_family = ch_individuals.map(
            { sample_id, family_id, father, mother, sex, phenotype, r1, r2 ->
                [family_id, sample_id]
            }
        ).groupTuple()

        /*
        ch_bcbio_csvs: [
            [family_id, family_id.csv],
            ...
        ]
        */
        ch_bcbio_csvs = write_bcbio_csv(
            ch_families,
            ch_target_bed
        )

        ch_bcbio_inputs = ch_indv_by_family.join(ch_bcbio_csvs.join(ch_merged_fastqs))

        /*
        ch_bcbio_family_outputs: [
            [family_id, [indv1_id, indv2_id, ...], family_id-merged/],
            ...
        ]
        */
        ch_bcbio_family_outputs = bcbio_family_processing(
            ch_bcbio_inputs,
            ch_bcbio,
            ch_bcbio_template
        )

        /*
        ch_individual_folder: [
            [family_id, individual_outputs/],
            ...
        ]
        */
        ch_individual_folders = format_bcbio_individual_outputs(
            ch_bcbio_family_outputs,
            ch_bcbio,
            ch_reference_genome

        )

        /*
        ch_formatted_bcbio_outputs: [
            [family_id, 2022-07-04_family_id, family_id-merged/],
            ...
        ]

        */
        ch_formatted_bcbio_outputs = format_bcbio_family_outputs(
            ch_bcbio_family_outputs.join(ch_individual_folders)
        )

        collate_pipeline_outputs(
            ch_formatted_bcbio_outputs.map({it[0]}).collect(),
            ch_formatted_bcbio_outputs.map({it[1]}).collect(),
            ch_formatted_bcbio_outputs.map({it[2]}).collect(),
            ch_ped_file,
            ch_samplesheet,
            ch_bcbio
        )
}


workflow var_calling {
    read_inputs()

    validation(read_inputs.out.ch_individuals)
    process_families(
        read_inputs.out.ch_individuals,
        read_inputs.out.ch_ped_file,
        read_inputs.out.ch_samplesheet
    )
}

