nextflow.enable.dsl = 2
include {var_calling} from './pipeline/var_calling.nf'
include {prioritisation_setup; prioritisation} from './pipeline/variant_prioritisation.nf'
include {compress; decompress} from './pipeline/cram_compression.nf'

workflow {
    switch (params.workflow) {
        case 'variant_calling':
            var_calling()
            break

        case 'variant_prioritisation_setup':
            prioritisation_setup()
            break

        case 'variant_prioritisation':
            prioritisation()
            break

        case 'cram_compression':
            compress()
            break

        case 'cram_decompression':
            decompress()
            break

        default:
            exit 1, "Invalid params.workflow: ${params.workflow}"
    }
}
