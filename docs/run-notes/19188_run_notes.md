# Trio whole exome - 19188 (EdGe)

* Fam 435981 - urgent trio
* Fam 404190, Individual 128589 is a repeat maternal sample - other 2 samples are from 14110_Ansari_Morad
* Fam 433855, Individual 128681 is a repeat proband sample - other 2 samples are from 18610_Ansari_Morad
* Fam 428986 affected duo, for shared variant analysis
* Fam 435139 quad – similarly affected brothers. Shared & trio analysis.
* Fam 434642 duo for Congenica
* Fam 434889 duo for Congenica

## QC notes

**Note:** family 433924 had to be re-started and is not included in these notes yet.

* All passed pedigree checks
* Coverage a little low for:
  * 128450_434883 - 35X - unaffected father
  * 128360_434801 - 37X - proband
  * 84873_435139 - 37X - proband
  * 128102_429275 - 43X - unaffected father
  * 128540_434918 - 43X - unaffected mother
* Slight contamination for:
  * 128540_434918 - 2.9% - unaffected mother (also with slightly low coverage 43X above)
* Relatedness looks fine
* All correct sex
* 120967_434490 (male proband) has slightly odd X/Y mapping percentages at 80/20. Most males are in a tight range of 88-90%, it's a clear outlier.
* 128102_429275 (unaffected father, also with slightly low coverage 43X above) has very odd chromosome mapping percentages and GC content distribution.