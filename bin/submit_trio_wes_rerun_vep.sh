#!/bin/bash
#SBATCH --cpus-per-task=16
#SBATCH --mem=8GB
#SBATCH --time=8:00:00
#SBATCH --job-name=trio_whole_exome_rerun_vep
#SBATCH --output=trio_whole_exome_rerun_vep.%A_%a.out
#SBATCH --error=trio_whole_exome_rerun_vep.%A_%a.err

# Expects environment variables to be set
# CONFIG_SH - absolute path to configuration script setting environment variables
# FAMILIES - list of family folders (no path prefixes required) to have VEP re-run on

source $CONFIG_SH

FAMILY_DIR=`head -n $SLURM_ARRAY_TASK_ID $FAMILIES | tail -n 1`

# 2023-08-19_20200824_v3_18035_414631
SHORT_PROJECT_ID=`echo $FAMILY_DIR | cut -d '_' -f 2`
VERSION=`echo $FAMILY_DIR | cut -d '_' -f 3`
FAMILY_ID=`echo $FAMILY_DIR | cut -d '_' -f 4,5`

VCF_FILE=$OUTPUT_DIR/${SHORT_PROJECT_ID}_${VERSION}/families/$FAMILY_DIR/${FAMILY_ID}-gatk-haplotype-annotated.vcf.gz
ORIGINAL_VCF_FILE=${VCF_FILE%.vcf.gz}-original.vcf.gz

if [ ! -f $ORIGINAL_VCF_FILE ]
then
  mv $VCF_FILE $ORIGINAL_VCF_FILE
  mv $VCF_FILE.tbi $ORIGINAL_VCF_FILE.tbi
fi

NOANN_VCF_FILE=${ORIGINAL_VCF_FILE%.vcf.gz}-noann.vcf.gz
bcftools annotate -x INFO/ANN $ORIGINAL_VCF_FILE | bgzip -c > $NOANN_VCF_FILE
tabix $NOANN_VCF_FILE

#--no_stats \ - after --species normally, instead of --stats_file
#--stats_file ${VCF_FILE%.vcf.gz}.stats.txt --stats_text \ - alternate if crashing with --no_stats

unset PERL5LIB && \
export PATH=/mnt/e1000/home/u035/u035/shared/software/bcbio/anaconda/bin:"$PATH" && \
/home/u035/u035/shared/software/bcbio/anaconda/bin/vep \
--vcf \
-o stdout \
-i $NOANN_VCF_FILE \
--fork 16 \
--species homo_sapiens \
--stats_file ${VCF_FILE%.vcf.gz}.stats.txt --stats_text \
--cache \
--offline \
--dir /mnt/e1000/home/u035/u035/shared/software/bcbio/genomes/Hsapiens/hg38/vep \
--total_length \
--gene_phenotype \
--regulatory \
--af \
--max_af \
--af_1kg \
--af_esp \
--af_gnomad \
--pubmed \
--variant_class \
--allele_number \
--fasta /mnt/e1000/home/u035/u035/shared/software/bcbio/genomes/Hsapiens/hg38/seq/hg38.fa.gz \
--plugin LoF,human_ancestor_fa:false,loftee_path:/mnt/e1000/home/u035/u035/shared/software/bcbio/anaconda/share/ensembl-vep-100.4-0 \
--plugin G2P,file:/mnt/e1000/home/u035/u035/shared/analysis/work/variation/G2P.csv \
--plugin MaxEntScan,/mnt/e1000/home/u035/u035/shared/software/bcbio/anaconda/share/maxentscan-0_2004.04.21-1 \
--plugin SpliceRegion \
--sift b \
--polyphen b \
--humdiv \
--hgvsg \
--hgvs \
--shift_hgvs 1 \
--flag_pick_allele_gene \
--appris \
--biotype \
--canonical \
--ccds \
--domains \
--numbers \
--protein \
--symbol \
--tsl \
--uniprot \
--merged | \
sed '/^#/! s/;;/;/g' | \
bgzip -c > $VCF_FILE

tabix -f -p vcf $VCF_FILE

rm $NOANN_VCF_FILE*
