# Standard operating procedure - Connecting to Edinburgh Parallel Compute Centre sdf-cs1 (ultra2) system with FileZilla

This procedure assumes the user has registered with [EPCC SAFE](https://safe.epcc.ed.ac.uk/) and has access to the u035 project on the ultra2 system. It further assumes the user has generated an SSH public/private key pair and [uploaded the public key to SAFE](https://epcced.github.io/safe-docs/safe-for-users/#how-to-add-an-ssh-public-key-to-your-account). The private key must be accessible on the system on which the user is running FileZilla.

## Software requirements

- [FileZilla](https://filezilla-project.org/)

## Procedure

1. Install FileZilla.

2. Open FileZilla.

3. Go to the FileZilla > Settings menu and navigate to the SFTP tab. Click the 'Add key file' button and select the *private* key for the SSH key pair to be used. Click the OK button.

![Add private key](images/filezilla_add_private_key.png){width=500px}

4. Go to the File > Site Manager menu and enter the following information:

- Protocol: SFTP - SSH File Transfer Protocol
- Host: sdf-cs1.epcc.ed.ac.uk
- Logon Type: Ask for password
- User: User id for u035 project (see SAFE)

![SFTP site manager profile](images/filezilla_add_site.png){width=500px}

5. Go to the Advanced tab and enter the following information:

- Default remote directory - /home/u035/u035/<user id>

![Set default remote directory](images/filezilla_set_default_remote_dir.png){width=300px}

6. Edit the entry under 'My sites' to say 'ultra2'. Click the OK button.

7. To test connecting, go to the File > Site Manager menu and select the 'ultra2' profile under 'My Sites'. Click the Connect button.
