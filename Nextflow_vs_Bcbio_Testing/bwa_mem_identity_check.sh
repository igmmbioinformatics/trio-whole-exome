#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=10GB
#SBATCH --time=24:00:00
#SBATCH --job-name=bwa_identity
#SBATCH --output=bwa_identity.out
#SBATCH --error=bwa_identity.err

SAMTOOLS=/home/u035/u035/shared/software/bcbio/anaconda/bin/samtools

#We know that running bwa mem multiple times on identical input produces identical output
#We also wish to test that the nextflow and bcbio pipeline produce identical bam files when provided with identical fastqs
#In this example, nextflow is run with the trimmed fastqs produced by the bcbio pipeline (via fastp)
#We now wish to test that the resulting bam files are identical following bamsormadup (sort and mark duplicates)

#Convert bams to sam - remove read group tags as naming convention differs slightly between pipelines

#bcbio bam
$SAMTOOLS view -x RG 158063_519317-sort.bam > 158063_519317-sort.noRG.sam

#nextflow bam
$SAMTOOLS view -x RG 158063.bam > 158063.noRG.sam

#Generate md5sums - these should be identical
md5sum 158063_519317-sort.noRG.sam > 158063_519317-sort.noRG.sam.md5
md5sum 158063.noRG.sam > 158063.noRG.sam.md5


