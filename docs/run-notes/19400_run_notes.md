## Notes from facility

* 438869 – Shared variant analysis between two brothers (130264 and 130453). Have sequenced mother as well (130268) in case duo analysis required on Congenica.
* 451427 – Shared variant analysis between two half brothers (130701 and 130700) and trio analysis for 130701.
* 436427 – Proband failed on run 19285, repeated on this run. Have put whole trio in the PED file.
 
Duo/singleton for Congenica only:
* 434754
* 438175
* 438038
* 436376

## Re-running

Missed the note about 436427 family needing parent sequences from run 19285, re-submitting.

## QC

* family 438170: 130112 (child) and 130113 (father) failed PED check
* family 434857: 0x coverage for 129816 (mother) and 129817 (father) - FASTQ files are basically empty
* family 438173: 130110 (father) has 9.4% contamination
* family 435809: 128425 (proband) has 7.8% contamination

