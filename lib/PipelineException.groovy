import org.codehaus.groovy.GroovyException

class PipelineException extends GroovyException {
    /*
    Simple exception class that can be thrown at pipeline runtime.
    Maybe there's a better way of raising errors?
    */
    public PipelineException(String message) {
        super(message)
    }

}
