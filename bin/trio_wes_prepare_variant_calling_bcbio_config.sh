#!/bin/bash
#
# prepare_bcbio_config.sh <config.sh> <project_id> <old_version> <new_version>
# 
# Given a <project_id>.ped file for a set of families in the 
# folder $PARAMS_DIR, creates the files <project_id>.family_ids.txt
# and <project>.sample_ids.txt in the same folder.
#
# Assumes that CRAM files for the samples are in the paths
# $OUTPUT_DIR/<short_project_id>_<old_version>/families/*<family_id>/<sample_id>
# $OUTPUT_DIR is specified in the <config.sh> file.
#
# Runs bcbio sample preparation and configuration file generation,
# assuming the template configuration file is at $BCBIO_TEMPLATE,
# specified in the <config.sh> file.
#
# Assumes bcbio is on the PATH (set in <config.sh>).
#

CONFIG_SH=$1
PROJECT_ID=$2
OLD_VERSION=$3
NEW_VERSION=$4

source $CONFIG_SH

SHORT_PROJECT_ID=`echo $PROJECT_ID | cut -f 1 -d '_'`

mkdir -p ${OUTPUT_DIR}/${SHORT_PROJECT_ID}_${NEW_VERSION}/params
mkdir -p ${OUTPUT_DIR}/${SHORT_PROJECT_ID}_${NEW_VERSION}/config
mkdir -p ${OUTPUT_DIR}/${SHORT_PROJECT_ID}_${NEW_VERSION}/families

# Copy the PED files
cd $PARAMS_DIR
cp $OUTPUT_DIR/${SHORT_PROJECT_ID}_${OLD_VERSION}/params/*.ped ./
cp $OUTPUT_DIR/${SHORT_PROJECT_ID}_${OLD_VERSION}/params/*.txt ./

chmod u+w *.ped *.txt

for FAMILY_ID in `cat ${PROJECT_ID}.family_ids.txt`
do
  PREFIX=${SHORT_PROJECT_ID}_${NEW_VERSION}_${FAMILY_ID}
  
  echo "description,batch,sex,phenotype,variant_regions" > ${PREFIX}.csv
  COUNT=`wc -l ${PROJECT_ID}_${FAMILY_ID}.ped | awk '{ print $1 }'`

  for ((i=1; i<=$COUNT; i=i+1))
  do
    SAMPLE=`head -n $i ${PROJECT_ID}_${FAMILY_ID}.ped | tail -n 1 | cut -f 2`
    SEX=`head -n $i ${PROJECT_ID}_${FAMILY_ID}.ped | tail -n 1 | cut -f 5`
    PHENOTYPE=`head -n $i ${PROJECT_ID}_${FAMILY_ID}.ped | tail -n 1 | cut -f 6`

    echo "$SAMPLE,$FAMILY_ID,$SEX,$PHENOTYPE,$TARGET" >> ${PREFIX}.csv
  done

  BARE_FAMILY_ID=`echo $FAMILY_ID | cut -d '_' -f 2`

  bcbio_nextgen.py -w template $BCBIO_TEMPLATE ${PREFIX}.csv $WORK_DIR/${SHORT_PROJECT_ID}_${NEW_VERSION}/${BARE_FAMILY_ID}

  mv ${PREFIX}/config/${PREFIX}.yaml $CONFIG_DIR/

  COMPRESSED_ID=`echo "$FAMILY_ID" | perl -pe "s/\_//"`

  perl -pi -e "s/${COMPRESSED_ID}/${FAMILY_ID}/" $CONFIG_DIR/${PREFIX}.yaml
  perl -pi -e "s/\'\[/\[/" $CONFIG_DIR/${PREFIX}.yaml
  perl -pi -e "s/\]\'/\]/" $CONFIG_DIR/${PREFIX}.yaml
  perl -pi -e "s|\./results|$OUTPUT_DIR/${SHORT_PROJECT_ID}_${NEW_VERSION}/families|" $CONFIG_DIR/${PREFIX}.yaml

  rm -r ${PREFIX}

  cp ${PREFIX}.csv ${OUTPUT_DIR}/${SHORT_PROJECT_ID}_${NEW_VERSION}/params/
  cp ${PROJECT_ID}_${FAMILY_ID}.ped ${OUTPUT_DIR}/${SHORT_PROJECT_ID}_${NEW_VERSION}/params/
  cp $CONFIG_DIR/$PREFIX.yaml ${OUTPUT_DIR}/${SHORT_PROJECT_ID}_${NEW_VERSION}/config/

done
