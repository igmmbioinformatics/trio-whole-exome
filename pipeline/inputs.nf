nextflow.enable.dsl = 2


workflow read_inputs {
    /*
    Read the input files into channels. This has to be a workflow so the channels
    can be included in other scripts
    */

    main:
        /*
        ch_family_info - tuple channel of family members:
        [
            [
                family: family1,
                indv: indv1,
                father: indv2,
                mother: indv3,
                sex: 2,
                affected: 2
            ],
            ...
        ]
        */
        ch_ped_file = Channel.fromPath(params.ped_file, checkIfExists: true)
        ch_ped_file_info = ch_ped_file.splitCsv(sep: '\t')
            .map(
                { line ->
                    [
                        line[1],  // indv ID
                        line[0],  // family ID
                        line[2],  // father
                        line[3],  // mother
                        line[4],  // sex
                        line[5]   // affected
                    ]
                }
            )


        /*
        ch_samplesheet_info - Tuple channel of individual ID, r1 fastqs and r2 fastqs:
        [
            [
                indv1,
                [/abs/path/to/lane_1_r1.fastq.gz, /abs/path/to/lane_2_r1.fastq.gz],
                [/abs/path/to/lane_1_r2.fastq.gz, /abs/path/to/lane_2_r2.fastq.gz]
            ],
            [
                indv2,
                ...
            ]
        ]
        */
        ch_samplesheet = Channel.fromPath(params.sample_sheet, checkIfExists: true)
        ch_samplesheet_info = ch_samplesheet.splitCsv(sep:'\t', header: true)
            .map(
                { line -> [line.individual_id, file(line.read_1), file(line.read_2)] }
            )
            .groupTuple()

        /*
        ch_individuals - merge of samplesheet and ped file information:
        [
            [
                indv_id,
                family_id,
                father_id,
                mother_id,
                sex,
                affected,
                [r1_fastqs],
                [r2_fastqs]
            ],  
        ...
        ]
        */
        ch_individuals = ch_ped_file_info.join(ch_samplesheet_info)

        /*
        ch_individuals_by_family - as ch_individuals, but grouped by family:
        [
            [
                family_id,
                [
                    indv_id,
                    family_id,
                    father_id,
                    mother_id,
                    sex,
                    affected,
                    [r1_fastqs],
                    [r2_fastqs]
                ]
            ],
            ...
        ]
        */
        ch_individuals_by_family = ch_individuals.map({[it[1], it]})
    
    emit:
        ch_ped_file = ch_ped_file
        ch_ped_file_info = ch_ped_file_info
        ch_samplesheet = ch_samplesheet
        ch_samplesheet_info = ch_samplesheet_info
        ch_individuals = ch_individuals
        ch_individuals_by_family = ch_individuals_by_family
}
