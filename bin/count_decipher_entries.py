#	Counts the number of variants in a DECIPHER Excel upload file (v10 or v11).
#
#       Author: AM
#       last modified: SEP 01, 2023






import sys
import os
import shutil
import csv
import xlsxwriter
import xlrd


def go(DEC_file):

    if not os.path.exists(DEC_file):
        print ('ERROR: cannot find ', DEC_file)
        raise SystemExit

    wb = xlrd.open_workbook(DEC_file)
    ws = wb.sheet_by_index(0)
    print (DEC_file, ' ', ws.nrows)
    
if __name__ == '__main__':
    if len(sys.argv) == 2:
        go(sys.argv[1])
    else:
        print ('Suggested use: time python3 count_decipher_entries.py DECIPHER_v11.xlsx')


        raise SystemExit
