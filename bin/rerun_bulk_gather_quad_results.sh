#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=2GB
#SBATCH --time=01:00:00
#SBATCH --job-name=gather_bulk_quad_results
#SBATCH --output=gather_bulk_quad_results.%A_%a.out
#SBATCH --error=gather_bulk_quad_results.%A_%a.err


#       Author: MH
#       last modified: AUG 16, 2022


SCRIPTS_DIR=/home/u035/u035/shared/scripts/bin
#^#SCRIPTS_DIR=/home/u035/u035/shared/reanalysis_scripts

PYTHON3=/home/u035/u035/shared/software/bcbio/anaconda/bin/python3



# check input given to script
echo "INPUT_DIR = ${INPUT_DIR}"         # the path to the bulk reanalysis folder with the links to source VCF and PED files,
                                        # e.g. /home/u035/u035/shared/results/20220418_reanalysis
echo "PROJECT_ID = ${PROJECT_ID}"       # this the the folder (${BASE}/${PROJECT_ID}) where the prioritization analysis will be done


### folder structure for the downstream analysis - created by reanalysis_setup_quad.sh ###
BASE=/home/u035/u035/shared/analysis/work
WORK_DIR=${BASE}/${PROJECT_ID}
NHS_DIR=${WORK_DIR}/results
BAM_DIR=${WORK_DIR}/BAM


# other files to be used
FAMILY_IDS=${WORK_DIR}/quad_FAM_IDs.txt                                                      # created by reanalysis_setup_quad.sh
FAM_PRO_FILE=${WORK_DIR}/quad_FAM_PRO.txt


# check if ${NHS_DIR} already exists - if not, exit and ask to be created
if [ ! -d "${NHS_DIR}" ]; then
  echo "${NHS_DIR} does not exist - need to create it before running this script!!!!"
  exit
fi




FAMILY_ID=`head -n ${SLURM_ARRAY_TASK_ID} ${FAMILY_IDS} | tail -n 1`				# contains only the family IDs (e.g.385295)


# split the variants to OK and artefacts and clean the DECIPHER upload file
time ${PYTHON3} ${SCRIPTS_DIR}/rerun_bulk_split_quad_variants.py ${WORK_DIR}/DECIPHER ${FAM_PRO_FILE} ${FAMILY_ID}


# create the family folder for the shared results for this family
FAM_DIR=${NHS_DIR}/${FAMILY_ID}
if [ -d "${FAM_DIR}" ]; then
  echo "${FAM_DIR} already exists - delete if you want to overwrite!!!!"
  exit
fi
mkdir ${FAM_DIR}


# copy the DECIPHER-to-INTERNAL ID mapping
cp ${WORK_DIR}/quad_DECIPHER_INTERNAL_IDs.txt ${FAM_DIR}

# copy the LOG files
cp ${WORK_DIR}/LOG/rerun_quad.*_${SLURM_ARRAY_TASK_ID}.err ${FAM_DIR}
cp ${WORK_DIR}/LOG/rerun_quad.*_${SLURM_ARRAY_TASK_ID}.out ${FAM_DIR}


# copy the G2P html reports for the two trios and the affected sib-pair
cp ${WORK_DIR}/G2P/${FAMILY_ID}_*_LOG_DIR/${FAMILY_ID}_*.report.html ${FAM_DIR}


# copy (VASE) de novo variants in each proband VCF file
cp ${WORK_DIR}/VASE/${FAMILY_ID}_*.ready.denovo.vcf ${FAM_DIR}


# copy the DECIPHER files for bulk upload
cp ${WORK_DIR}/DECIPHER/*_${FAMILY_ID}_DECIPHER_v11.xlsx ${FAM_DIR}
cp ${WORK_DIR}/DECIPHER/*_${FAMILY_ID}_shared_DECIPHER_v11.xlsx ${FAM_DIR}


# copy the variant snapshots
IGV_SNAP_DIR=${FAM_DIR}/IGV_snapshots
mkdir ${IGV_SNAP_DIR}
cp -r ${WORK_DIR}/DECIPHER/IGV/${FAMILY_ID}_* ${IGV_SNAP_DIR}
#?#cp -r ${WORK_DIR}/DECIPHER/IGV/${FAMILY_ID}_*/artefacts ${IGV_SNAP_DIR}/


# copy proband coverage files
cp ${WORK_DIR}/COV/*_${FAMILY_ID}.DD15.COV.txt ${FAM_DIR}

# to make the script usable for both DD and Fetal (for which we do not compute coverage over recurrent variants) panels
for REC_FILE in ${WORK_DIR}/COV/*_${FAMILY_ID}.REC_SNP_COV.txt; do
    [ -f "${REC_FILE}" ] || continue
    cp ${REC_FILE} ${FAM_DIR}
done









###############################################################################
####   Check if BAM file had to be created for each member of this family   ###
####   if so (determined by existance of CRAMs)                             ###
####   delete the BAMs                                                      ###
###############################################################################
#
## identify all folders (one for each individual) for this family containing cram/bam files (format: <INDI_ID>_<FAM_ID>)
#FAM_BAM_DIR=${INPUT_DIR}/families/*_${FAMILY_ID}/
#cd ${FAM_BAM_DIR}
#for ITEM in `ls -l`
#do
#    if test -d $ITEM && [[ "$ITEM" == *"_"* ]]
#    then
#        echo "    $ITEM is a CRAM/BAM folder..."
#        BAM=${ITEM}/${ITEM}-ready.bam
#        CRAM=${ITEM}/${ITEM}-ready.cram
#
#        # and check if we have CRAM file for this individual - if yes, delete BAM; if not - keep BAM
#        if [ -f "$CRAM" ]; then
#            echo "$CRAM exists --> delete BAM"
#            rm ${BAM}
#            rm ${BAM}.bai
#        else
#            echo "$CRAM does not exist --> keep BAM"
#        fi
#    fi
#done

### Delete the BAM files and its index ###
rm ${BAM_DIR}/*_${FAMILY_ID}-ready.bam
rm ${BAM_DIR}/*_${FAMILY_ID}-ready.bam.bai



echo ""
echo ""
echo "OK: Results for ${FAMILY_ID} are stored in ${FAM_DIR}"

