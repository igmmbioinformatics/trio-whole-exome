#	Based on the suffix in the name of the regular IGV snapshots (not the bamouts)
#	splits them to OK (ending with _OK.png) and artefact variants (not ending with OK)
#	moves both the regular and bamout snapshots for the artefact variants to folder called "artefacts"
#	the snapshots of the OK variants stay as they are
#	and cleans the decipher upload file to contain only OK variants
#	# old decipher file = *DECIPHER_v10.xlsx - need to isntall stuff (openpyxl|xlrd/...)  <- maybe address this!!!
#	old decipher file = *DEC_FLT.csv
#	clean decipher file = * DECIPHER_v11.xlsx
#
#
#       Author: MH
#       last modified: AUG 16, 2022




import sys
import os
import shutil
import csv
import xlsxwriter
from collections import defaultdict



def go(DEC_DIR,FAM_PRO_FILE,FAMILY):

    # read the quad_FAM_PRO.txt file generated by redo_setup_quad.sh
    # to get a FAM_PRO dict, where the key is the FAMILY_ID and the value is a list of PROBAND_IDs
    FAM_PRO_DICT = read_quad_FAM_PRO(FAM_PRO_FILE)

    # get the kid IDs for this quad
    FAMILY_ID = FAMILY
    KIDS = FAM_PRO_DICT[FAMILY_ID]





    ##########################################
    # process each proband as part of a trio #
    ##########################################

    for INDI_ID in KIDS:

        print ('')
        print ('..........................................')
        print ('Processing proband INDIVIDIAL ID = ', INDI_ID, ', as part of a trio in FAMILY ID = ', FAMILY_ID)
        print ('Spliting the variants to OK and aretfacts (artefacts go to folder "artefacts")')
        print ('and cleaning the decipher upload file to conatin only info about OK variants')
        print ('The cleaned DECIPHER upload file is called ', INDI_ID, '_', FAMILY_ID, '_DECIPHER_v11.xlsx')
        print ('..........................................')
        print ('')

        MAIN_DECIPHER_DIR = "%s" % (DEC_DIR)
        MAIN_IGV_DIR = "%s/IGV" % (DEC_DIR)
        #0#FAMILY_IGV_DIR = "%s/%s_%s_%s" % (MAIN_IGV_DIR,PLATE_ID,FAMILY_ID,INDI_ID)
        FAMILY_IGV_DIR = "%s/%s_%s" % (MAIN_IGV_DIR,FAMILY_ID,INDI_ID)
        IN_DEC_FILE = "%s/%s_%s_DEC_FLT.csv" % (MAIN_DECIPHER_DIR,INDI_ID,FAMILY_ID)
        OUT_DEC_FILE = "%s/%s_%s_DECIPHER_v11.xlsx" % (MAIN_DECIPHER_DIR,INDI_ID,FAMILY_ID)

        print ('Main DECIPHER folder = ', MAIN_DECIPHER_DIR)
        print ('Main IGV folder = ', MAIN_IGV_DIR)
        print ('FAMILY_IGV_DIR = ', FAMILY_IGV_DIR)
        print ('IN_DEC_FILE = ', IN_DEC_FILE)
        print ('OUT_DEC_FILE = ', OUT_DEC_FILE)
        print ('')
        print ('')

        # create a fresh folder to keep the IGV snapshots for the artefact variants (not bamout, not ending with OK png files)
        arte = os.path.join(FAMILY_IGV_DIR, "artefacts")

        if os.path.exists(arte) and os.path.isdir(arte):
            shutil.rmtree(arte)
            print ('WARNING: ', arte, 'found to be created previously and thus deleted')

        try:
            os.mkdir(arte)
            print ('Created a fresh ', arte, ' folder')
            print ('    to store the IGV snapshots for the artefact variants')
        except OSError as error:
            print(error)



        # go over all the not bamout snapshots for this kid and split them to OK and artefacts
        OK_VARS = []		# <indi_id>_<family_id>_chr_pos_ref_alt
        ARTE_VARS = []		# <indi_id>_<family_id>_chr_pos_ref_alt

        for file in os.listdir(FAMILY_IGV_DIR):
            if file == "artefacts":				# ignore the aretfacts folder just created
                continue
            if file.startswith("bamout_"):
                continue
            elif file.endswith("_OK.png"):
                print ('OK variant = ', file)
                data = [x.strip() for x in file.strip().split('_')]
                var = "%s_%s_%s_%s_%s_%s" % (data[0],data[1],data[2],data[3],data[4],data[5])
                OK_VARS.append(var)
            else:
                data = [x.strip() for x in file.strip().split('_')]
                var = "%s_%s_%s_%s_%s_%s" % (data[0],data[1],data[2],data[3],data[4],data[5])
                ARTE_VARS.append(var)

                # move the file to to artefacts folder
                sour = os.path.join(FAMILY_IGV_DIR,file)
                dest = os.path.join(FAMILY_IGV_DIR,"artefacts",file)
                shutil.move(sour, dest)
                print ('Aretfact variant ', file, ' moved to the artefacts folder')

        print ('There are ', len(OK_VARS), ' OK and ', len(ARTE_VARS), ' ARTEFACT variants in PROBAND = ', INDI_ID, ', FAMILY = ', FAMILY_ID)



        # move artefact bamout variants for this kid to the artefact folder
        for file in os.listdir(FAMILY_IGV_DIR):
            if file == "artefacts":                         	# ignore the aretfacts folder just created
                continue
            if file.startswith("bamout_"):
                # check if OK or artefact
                data = [x.strip() for x in file.strip().split('_')]
                var = "%s_%s_%s_%s_%s_%s" % (data[1],data[2],data[3],data[4],data[5],data[6][:-4])		# remove the .png
                if var in ARTE_VARS:
                    sour = os.path.join(FAMILY_IGV_DIR,file)
                    dest = os.path.join(FAMILY_IGV_DIR,"artefacts",file)
                    shutil.move(sour, dest)
                    print ('Aretfact variant = ', file, ' moved to the artefacts folder')


        # read, clean and write the new DECIPHER upload file

        # create the workbook
        workbook = xlsxwriter.Workbook(OUT_DEC_FILE)

        # create the worksheet
        worksheet = workbook.add_worksheet('Sequence Variants')

        header = ('Patient internal reference number or ID','Shared','Assembly','HGVS code','Chromosome','Genomic start','Ref sequence','Alt sequence','Gene name','Transcript','Is intergenic','Genotype','Inheritance','Pathogenicity','Pathogenicity evidence','Contribution','Genotype groups')
        worksheet.write_row(0,0,header)

        # now, open and read the old file, for each variant collecting the information required for v11 and writing it in the v11 file
        cntr = 0
        cntr_bad = 0

        with open(IN_DEC_FILE,'r') as tsvfile:
            reader = csv.reader(tsvfile, delimiter=',', quotechar='"')
            for row in reader:
                if row[0] == 'Internal reference number or ID':      # ignore the header line
                    continue

                # if here, not the header, parse the row info and check if an OK variant
                id = str(row[0])
                shared = 'NHS-SCE'
                assembly = 'GRCh38'
                HGVS = ''
                chr = str(row[1])
                start = str(row[2])
                ref = str(row[4])
                alt = str(row[5])

                # check if OK or artefact variant
                key = "%s_%s_chr%s_%s_%s_%s" % (INDI_ID,FAMILY_ID,chr,start,ref,alt)
                if key in ARTE_VARS:		#  an aretfact variant, ignore it
                    cntr_bad += 1
                    continue

                elif key in OK_VARS:		# an OK variant, update cntr, finish composing the info
                    cntr += 1
                    gene = str(row[7])
                    trans = str(row[6])
                    inter = str(row[8])
                    genotype = str(row[19])
                    inher = str(row[15])
                    if inher == 'Maternally inherited, constitutive in mother':
                        inher = 'Maternally inherited'
                    elif inher == 'Paternally inherited, constitutive in father':
                        inher = 'Paternally inherited'
                    patho = ''
                    evid = ''
                    cont = ''
                    gt_groups = ''
                    data = (id,shared,assembly,HGVS,chr,start,ref,alt,gene,trans,inter,genotype,inher,patho,evid,cont,gt_groups)

                    # write it
                    worksheet.write_row(cntr,0,data)

                else:
                    print ('ERROR: could not find key = ', key, ' nor in OK_VARS = ', OK_VARS)
                    print ('nor in ARTE_VARS = ', ARTE_VARS)
                    raise SystemExit()


        # close the workbook (the new DECIPHER_v11 file)
        workbook.close()

        print ('Found ', cntr, ' OK and ', cntr_bad, ' artefact variants in INDI_ID = ', INDI_ID, ' as part of trio in FAMILY_ID = ', FAMILY_ID)
        print ('Recorded in: ',OUT_DEC_FILE)
        sys.stdout.flush()

        # done with this proband as part of a trio #


    ###############################################
    # done processing the 2 kids as part of trios #
    ###############################################














    ######################################
    # process the two probands as shared #
    ######################################


    print ('')
    print ('..........................................')
    print ('Shared analysis of probands = ', KIDS, ' in FAMILY ID = ', FAMILY_ID)
    print ('Spliting the variants to OK and aretfacts (artefacts go to folder "artefacts")')
    print ('and cleaning the decipher upload file to conatin only info about OK variants')
    print ('The cleaned DECIPHER upload files is called <INDI_ID>_', FAMILY_ID, '_shared_DECIPHER_v11.xlsx')
    print ('..........................................')
    print ('')


    MAIN_DECIPHER_DIR = "%s" % (DEC_DIR)
    MAIN_IGV_DIR = "%s/IGV" % (DEC_DIR)
    #0#FAMILY_IGV_DIR = "%s/%s_%s_shared" % (MAIN_IGV_DIR,PLATE_ID,FAMILY_ID)
    FAMILY_IGV_DIR = "%s/%s_shared" % (MAIN_IGV_DIR,FAMILY_ID)
    print ('Main DECIPHER folder = ', MAIN_DECIPHER_DIR)
    print ('Main IGV folder = ', MAIN_IGV_DIR)
    print ('FAMILY_IGV_DIR = ', FAMILY_IGV_DIR)

    # create a fresh folder to keep the IGV snapshots for the artefact variants (not bamout, not ending with OK png files)
    arte = os.path.join(FAMILY_IGV_DIR, "artefacts")

    if os.path.exists(arte) and os.path.isdir(arte):
        shutil.rmtree(arte)
        print ('WARNING: ', arte, 'found to be created previously and thus deleted')

    try:
        os.mkdir(arte)
        print ('Created a fresh ', arte, ' folder')
        print ('    to store the IGV snapshots for the artefact variants')
    except OSError as error:
        print(error)


    for INDI_ID in KIDS:

        print ('')
        print ('..........................................')
        print ('  Processing proband INDIVIDIAL ID = ', INDI_ID)
        print ('  The cleaned DECIPHER upload file is called ', INDI_ID, '_', FAMILY_ID, '_shared_DECIPHER_v11.xlsx')
        print ('..........................................')
        print ('')

        IN_DEC_FILE = "%s/%s_%s_shared_DEC_FLT.csv" % (MAIN_DECIPHER_DIR,INDI_ID,FAMILY_ID)
        OUT_DEC_FILE = "%s/%s_%s_shared_DECIPHER_v11.xlsx" % (MAIN_DECIPHER_DIR,INDI_ID,FAMILY_ID)

        print ('IN_DEC_FILE = ', IN_DEC_FILE)
        print ('OUT_DEC_FILE = ', OUT_DEC_FILE)
        print ('')
        print ('')

        # go over all the not bamout snapshots for this kid and split them to OK and artefacts
        OK_VARS = []            # <indi_id>_<family_id>_chr_pos_ref_alt
        ARTE_VARS = []          # <indi_id>_<family_id>_chr_pos_ref_alt

        for file in os.listdir(FAMILY_IGV_DIR):
            if file == "artefacts":                             # ignore the aretfacts folder just created
                continue
            if file.startswith("bamout_"):
                continue
            if not file.startswith(INDI_ID):                    # this is a snapshot for the other kid, ignore
                continue
            elif file.endswith("_OK.png"):
                print ('OK variant = ', file)
                data = [x.strip() for x in file.strip().split('_')]
                var = "%s_%s_%s_%s_%s_%s" % (data[0],data[1],data[2],data[3],data[4],data[5])
                OK_VARS.append(var)
            else:
                data = [x.strip() for x in file.strip().split('_')]
                var = "%s_%s_%s_%s_%s_%s" % (data[0],data[1],data[2],data[3],data[4],data[5])
                ARTE_VARS.append(var)

                # move the file to to artefacts folder
                sour = os.path.join(FAMILY_IGV_DIR,file)
                dest = os.path.join(FAMILY_IGV_DIR,"artefacts",file)
                shutil.move(sour, dest)
                print ('Aretfact variant ', file, ' moved to the artefacts folder')

        print ('There are ', len(OK_VARS), ' OK and ', len(ARTE_VARS), ' ARTEFACT variants in PROBAND = ', INDI_ID, ', FAMILY = ', FAMILY_ID)


        # move artefact bamout variants for this kid to the artefact folder
        for file in os.listdir(FAMILY_IGV_DIR):
            if file == "artefacts":                             # ignore the aretfacts folder just created
                continue

            kid_bamout_prefix = "bamout_%s" % (INDI_ID)
            if not file.startswith(kid_bamout_prefix):          # this is a snapshot for the other kid, ignore
                continue
            else:
                # check if OK or artefact
                data = [x.strip() for x in file.strip().split('_')]
                var = "%s_%s_%s_%s_%s_%s" % (data[1],data[2],data[4],data[5],data[6],data[7][:-4])              # remove the .png
                if var in ARTE_VARS:
                    sour = os.path.join(FAMILY_IGV_DIR,file)
                    dest = os.path.join(FAMILY_IGV_DIR,"artefacts",file)
                    shutil.move(sour, dest)
                    print ('Aretfact variant = ', file, ' moved to the artefacts folder')

        # read, clean and write the new DECIPHER upload file

        # create the workbook
        workbook = xlsxwriter.Workbook(OUT_DEC_FILE)

        # create the worksheet
        worksheet = workbook.add_worksheet('Sequence Variants')

        header = ('Patient internal reference number or ID','Shared','Assembly','HGVS code','Chromosome','Genomic start','Ref sequence','Alt sequence','Gene name','Transcript','Is intergenic','Genotype','Inheritance','Pathogenicity','Pathogenicity evidence','Contribution','Genotype groups')
        worksheet.write_row(0,0,header)

        # now, open and read the old file, for each variant collecting the information required for v11 and writing it in the v11 file
        cntr = 0
        cntr_bad = 0

        with open(IN_DEC_FILE,'r') as tsvfile:
            reader = csv.reader(tsvfile, delimiter=',', quotechar='"')
            for row in reader:
                if row[0] == 'Internal reference number or ID':      # ignore the header line
                    continue

                # if here, not the header, parse the row info and check if an OK variant
                id = str(row[0])
                shared = 'NHS-SCE'
                assembly = 'GRCh38'
                HGVS = ''
                chr = str(row[1])
                start = str(row[2])
                ref = str(row[4])
                alt = str(row[5])

                # check if OK or artefact variant
                key = "%s_%s_chr%s_%s_%s_%s" % (INDI_ID,FAMILY_ID,chr,start,ref,alt)
                if key in ARTE_VARS:            #  an aretfact variant, ignore it
                    cntr_bad += 1
                    continue

                elif key in OK_VARS:            # an OK variant, update cntr, finish composing the info
                    cntr += 1
                    gene = str(row[7])
                    trans = str(row[6])
                    inter = str(row[8])
                    genotype = str(row[19])
                    inher = str(row[15])
                    if inher == 'Maternally inherited, constitutive in mother':
                        inher = 'Maternally inherited'
                    elif inher == 'Paternally inherited, constitutive in father':
                        inher = 'Paternally inherited'
                    patho = ''
                    evid = ''
                    cont = ''
                    gt_groups = ''
                    data = (id,shared,assembly,HGVS,chr,start,ref,alt,gene,trans,inter,genotype,inher,patho,evid,cont,gt_groups)

                    # write it
                    worksheet.write_row(cntr,0,data)

                else:
                    print ('ERROR: could not find key = ', key, ' nor in OK_VARS = ', OK_VARS)
                    print ('nor in ARTE_VARS = ', ARTE_VARS)
                    raise SystemExit()


        # close the workbook (the new DECIPHER_v11 file)
        workbook.close()

        print ('Found ', cntr, ' OK and ', cntr_bad, ' artefact variants in INDI_ID = ', INDI_ID, ' as part of the shared proband analysis of FAMILY_ID = ', FAMILY_ID)
        print ('Recorded in: ',OUT_DEC_FILE)
        sys.stdout.flush()


      # done with this proband as part of the shared analysis







def read_quad_FAM_PRO(in_file):
   # the quad_FAM_PRO.txt has the following format
   # 453214  ['129373', '66644']
   # i.e. FAM_ID \t ['kid_1_id', 'kid_2_id']

    fam_pro_dict = defaultdict(list)            # key: family_id: value: list of affected proband IDs

    in_han = open(in_file,'r')
    for line in in_han:
        data = [x.strip() for x in line.strip().split('\t')]
        fam_id = data[0]
        pro_ids = data[1].replace('[', '').replace(']', '').replace('\'', '').replace(' ', '').split(',')
        for pro_id in pro_ids:
            fam_pro_dict[fam_id].append(pro_id)
    in_han.close()
    return fam_pro_dict




if __name__ == '__main__':
    if len(sys.argv) == 4:
        go(sys.argv[1],sys.argv[2],sys.argv[3])
    else:
        print ('Suggested use: time python3 /home/u035/u035/shared/split_scripts/split_quad_variants.py the_main_decipher_folder quad_FAM_PRO.txt FAMILY_ID')
        raise SystemExit

