nextflow.enable.dsl = 2


process bam_to_cram {
    input:
    path(bam)

    output:
    path("${bam.getBaseName()}.cram")

    script:
    """
    samtools=${params.bcbio}/anaconda/bin/samtools &&
    bam_flagstat=${bam}.flagstat.txt &&
    cram="${bam.getBaseName()}.cram" &&
    cram_flagstat=\$cram.flagstat.txt &&

    if [ ! -f \$bam_flagstat ]
    then
        \$samtools flagstat $bam > \$bam_flagstat
    fi

    \$samtools view -@ ${task.cpus} -T ${params.reference_genome} -C -o \$cram ${bam} &&
    \$samtools index \$cram &&
    \$samtools flagstat \$cram > \$cram_flagstat

    if ! diff \$bam_flagstat \$cram_flagstat > /dev/null 2>&1
    then
        echo "Unexpected flagstat results in \$bam_flagstat and \$cram_flagstat"
        exit 1
    fi

    cp \$cram \$(dirname \$(readlink -f $bam))&&
    rm \$(readlink -f $bam)
    """

    stub:
    """
    cram="${bam.getBaseName()}.cram"
    cp ${bam} \$cram &&
    touch \$cram.crai &&
    touch ${bam}.flagstat.txt \$cram.flagstat.txt &&
    cp \$cram \$(dirname \$(readlink -f $bam)) &&
    rm \$(readlink -f $bam)
    """
}

process cram_to_bam {
    input:
    path(cram)

    output:
    path("${cram.getBaseName()}.bam")

    script:
    """
    samtools=${params.bcbio}/anaconda/bin/samtools &&
    bam="${cram.getBaseName()}.bam" &&
    bam_flagstat=\${bam}.flagstat.txt &&
    cram_flagstat=${cram}.flagstat.txt &&

    if [ ! -f \$cram_flagstat ]
    then
        \$samtools flagstat $cram > \$cram_flagstat
    fi

    \$samtools view -@ ${task.cpus} -T ${params.reference_genome} -b -o \$bam ${cram} &&
    \$samtools index \$bam &&
    \$samtools flagstat \$bam > \$bam_flagstat

    if ! diff \$bam_flagstat \$cram_flagstat > /dev/null 2>&1
    then
        echo "Unexpected flagstat results in \$bam_flagstat and \$cram_flagstat"
        exit 1
    fi

    cp \$bam \$(dirname \$(readlink -f $cram)) &&
    rm \$(readlink -f $cram)
    """

    stub:
    """
    bam="${cram.getBaseName()}.bam"
    cp ${cram} \$bam &&
    touch \$bam.crai &&
    touch ${cram}.flagstat.txt \$bam.flagstat.txt &&
    cp \$bam \$(dirname \$(readlink -f $cram)) &&
    rm \$(readlink -f $cram)
    """
}


workflow compress {
    ch_bams = Channel.fromPath(
        "${params.compress_dir}/**/*.bam"
    )
    bam_to_cram(ch_bams)
}


workflow decompress {
    ch_crams = Channel.fromPath(
        "${params.compress_dir}/**/*.cram"
    )
    cram_to_bam(ch_crams)
}
