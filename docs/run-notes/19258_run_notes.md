# Trio whole exome batch 19285 (EdGe)

* The urgent family is 438158.
* We know that sample 129421 (proband from family 436427) is going to fail – there was an issue during set up for this one.
* Family 438023 – Quad, for shared analysis between two sibs and trio analysis per sib (129760 and 129850).

Duos/singleton – for Congenica only
* 435163 (duo)
* 436090 (duo)
* 436525 (singleton)

# QC

* Pedigree check ok
* Samples with slightly low coverage:
  * 128940_435163	43X
  * 128993_429270	47X
  * 126656_429218   48X
* Contamination check ok
* Sample 129421 was not delivered - don't analyze family 436427, parents QC'd ok
* Sequencing QC check ok

# Additional run with remove_lcr: false

Get an idea of how many additional candidate variants are returned if LCR filtering is turned off.
