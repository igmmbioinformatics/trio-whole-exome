#	given a quad family PED file and the ids of the two kids
#	generate a PED file for the two siblings, setting their parents to 0 (unknown)
#
#       Author: MH
#       last modified: MAY 20, 2022



import sys
import os
import csv
import gzip
import glob


def go(inout_dir,fam_id,kid_1_id,kid_2_id):

    quad_ped_file = ''
    for file in glob.glob('%s/*_%s.ped' % (inout_dir,fam_id)):
        if quad_ped_file == '':
            quad_ped_file = file
        else:
            print "ERROR: more than one PED file matching the family id = %s" % (fam_id)
            raise SystemExit

    shared_ped_file = '%s_shared.ped' % (quad_ped_file[:-4])
    out_han = open(shared_ped_file,'w')
    out_cntr = 0

    in_han = open("%s" % (quad_ped_file),'r')
    in_cntr = 0

    for line in in_han:
        in_cntr += 1
        data = [x.strip() for x in line.strip().split('\t')]
        indi_fam_id = data[1]
        if ((indi_fam_id.startswith(kid_1_id)) or (indi_fam_id.startswith(kid_2_id))):
            new_line = '%s\t%s\t%s\t%s\t%s\t%s\n' % (data[0],data[1],0,0,data[4],data[5])
            out_han.write(new_line)
            out_cntr += 1

    in_han.close()
    out_han.close()
    print "Found %s individuals in the quad ped file = %s/%s" % (in_cntr,inout_dir,quad_ped_file)
    print "Recorded %s individuals in shared ped file = %s" % (out_cntr,shared_ped_file)





if __name__ == '__main__':
    if len(sys.argv) == 5:
        go(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4])
    else:
        print "Suggested use: time $PYTHON /home/u035/u035/shared/scripts/NHS_WES_generate_aff_sib_ped.py ${PED_DIR} ${quad_ped_file} ${KID_1_ID} ${KID_2_ID}"
        raise SystemExit

