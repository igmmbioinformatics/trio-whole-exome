#	given a multi-sample VCF
#	reset the GT to ./. (no-call) for indi variant with num_ALT < num_ALT_THERSH or VAF < VAF_THRESH
#
#
#       Author: MH
#       last modified: APR 25, 2022



import sys
import os
import gzip



num_ALT_THERSH = int(3)
VAF_THRESH = float(0.2)
BLACKLIST = {}			# key: 'chr:pos:ref:alt'; value: irrelevant



def go(black_file,in_file,out_file):

    read_blacklist(black_file)

    print ""
    print "  Reading the blacklist file: Found %s blacklisted variants in %s" % (len(BLACKLIST),black_file)
    print ""

    cntr_sites = 0
    cntr_vars = 0
    cntr_reset = 0

    in_han = open(in_file,'r')
    out_han = open(out_file,'w')

    for line in in_han:
        if line.startswith('#'):
            out_han.write(line)
            continue

        cntr_sites += 1

        data = [x.strip() for x in line.strip().split('\t')]

        # check if the variant is in the blacklist - if so, ignore it
        chr = data[0]
        pos = data[1]
        ref = data[3]
        alt = data[4]
        key = '%s:%s:%s:%s' % (chr,pos,ref,alt)
        if key in BLACKLIST:
            print "    INFO: %s is a blacklisted variant, excluding it ..." % (key)
            continue


        FMT = [y.strip() for y in data[8].split(':')]
        try:
            GT_IDX = FMT.index('GT')
            if GT_IDX != 0:
                print "ERROR: GT at weird place"
                print line
                raise SystemExit
        except:
            print "ERROR: Cannot find the GT label in the FORMAT field"
            print line
            raise SystemExit
        try:
            AD_IDX = FMT.index('AD')
            if AD_IDX != 1:
                print "ERROR: AD at weird place"
                print line
                raise SystemExit
        except:
            print "ERROR: Cannot find the AD label in the FORMAT field"
            print line
            raise SystemExit


        new_line = ''
        for x in xrange(0,9):
            new_line = new_line + '%s\t' % (data[x])


        # check if GT needs reseting
        for y in xrange(9,len(data)):
            cntr_vars += 1
            needs_reset = False
            this_var = [z.strip() for z in data[y].split(':')]
            this_VAR_GT = this_var[GT_IDX]

            if this_VAR_GT == './.':				# no need to reset, it is already a no-call
                new_line = new_line + '%s\t' % (data[y])
                continue

            this_VAR_AD = this_var[AD_IDX]

            if this_VAR_AD == '.':				# there are calls such as ./.:.:.:.:.
                num_ref = int(0)
                num_alt = int(0)
            else:
                num_ref,num_alt = this_VAR_AD.split(',')
                if num_ref == '.':
                    num_ref = int(0)
                else:
                    num_ref = int(num_ref)
                if num_alt == '.':
                    num_alt = int(0)
                else:
                    num_alt = int(num_alt)

            if (num_alt + num_ref) == 0:
                needs_reset = True
            else:
                VAF = float(num_alt)/float(num_alt+num_ref)

            if num_alt < num_ALT_THERSH:
                needs_reset = True

            if VAF < VAF_THRESH:
                needs_reset = True

            # the variant has LQ, but still called something different from 0/0 - we do not trust them: reset the GT to ./. (no-call)!
            if (needs_reset) and (this_VAR_GT != '0/0'):
                cntr_reset += 1
                new_var = './.:'
                for u in xrange(1,len(this_var)):
                    new_var = new_var + '%s:' % (this_var[u])
                new_var = new_var[:-1] + '\t'
                new_line = new_line + new_var
            else:
                new_line = new_line + '%s\t' % (data[y])

        # write the new line
        new_line = new_line[:-1] + '\n'
        out_han.write(new_line)

    in_han.close()
    out_han.close()

    perc_reset = (float(cntr_reset)/float(cntr_vars))*100.0
    print ""
    print ""
    print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
    print "Read a total of %s sites and %s individual GT; %s of them (%.2f%%) were LQ non-ref and were reset to no-call (./.)" % (cntr_sites,cntr_vars,cntr_reset,perc_reset) 
    print "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
    print ""
    print ""






def read_blacklist(in_file):
    in_han = open(in_file,'r')
    for line in in_han:
        data = [x.strip() for x in line.strip().split('\t')]
        chr = data[0]
        pos = data[1]
        ref = data[2]
        alt = data[3]
        key = '%s:%s:%s:%s' % (chr,pos,ref,alt)
        if key not in BLACKLIST:
            BLACKLIST[key] = 1
        else:
            print "ERROR: duplicate entry in the blacklist = %s" % (key)
            raise SystemExit
    in_han.close()








if __name__ == '__main__':
    if len(sys.argv) == 4:
        go(sys.argv[1],sys.argv[2],sys.argv[3])
    else:
        print "Suggested use: time $PYTHON2 filter_LQ_GT.py ${BLACKLIST} ${VCF_DIR}/${PLATE_ID}_${FAMILY_ID}.AC0.vcf ${VCF_DIR}/${PLATE_ID}_${FAMILY_ID}.clean.vcf"
        raise SystemExit

