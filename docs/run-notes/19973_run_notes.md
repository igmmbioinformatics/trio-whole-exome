# Run 19973

## Pre-run info

### Other family members on previous runs
* 453234, parents (134865 and 134864) are on this run, proband was on run 19690 (132524)
* 414996, proband (135612) and father (117093) are on this run, mother was on run 18203 (121256)
* 457300, proband (134113) and mother (134114) are on this run, father was on run 19875 (134116)

### Singletons
* 457456 (FFPE tissue, might fail)
* 480173

### Duos (for singleton analysis)
* 457600 (proband: 134484)
* 458099 (proband: 129313)

### Priority
* 414996, 480290, 457300, 481401, 481293

## QC summary

* 457546
  * 135245 - proband - 0.3M reads, contamination 14%, C>T/G>A deamination artefacts (probably 30% of variants) - fail, do not proceed with prioritization
* 457300
  * 134113 - proband - 27X - low but usable
  * 134116 - father - 33X - low but usable, failed per-tile sequence quality in FastQC (unusual)
* 481089
  * 105222 - mother - 42X - borderline low, fine to analyze
* 473144
  * 135054 - father - 45X - borderline low, fine to analyze
* 414996
  * 117093 - father - contamination 10% - proceed with prioritization with caution
