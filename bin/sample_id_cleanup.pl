#!/usr/bin/perl -w

=head1 NAME

sample_id_cleanup.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@ed.ac.uk)

=head1 DESCRIPTION

Fixes batch, maternal, and paternal ids that have had underscores stripped unintentionally.

    batch: 19875457300
    maternal_id: 134114457300
    paternal_id: 134116457300


=cut

use strict;

use Cwd;
use Getopt::Long;
use IO::File;

my $usage = qq{USAGE:
$0 [--help]
  --map   Id map
  --input Input YAML
};

my $help = 0;
my $map_file;
my $input_file;

GetOptions(
    'help'       => \$help,
    'map=s'      => \$map_file,
    'input=s'    => \$input_file
) or die $usage;

if ($help || !$map_file)
{
    print $usage;
    exit(0);
}

# Read in the map file
my $in_fh = new IO::File;
$in_fh->open($map_file, "r") or die "Could not open $map_file\n$!";

my $i = 0;
`cp $input_file temp.yaml.$i`;
while (my $line = <$in_fh>)
{
    chomp $line;
    my ($old_id, $new_id) = split(/\s+/, $line);

    my $j = $i;
    $i++;
    `sed -e 's/$old_id/$new_id/' temp.yaml.$j > temp.yaml.$i`;
}


$in_fh->close();

`mv temp.yaml.$i $input_file`;
`rm temp.yaml.*`;
