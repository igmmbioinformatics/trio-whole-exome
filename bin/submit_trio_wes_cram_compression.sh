#!/bin/bash
#SBATCH --cpus-per-task=16
#SBATCH --mem=8GB
#SBATCH --time=48:00:00
#SBATCH --job-name=trio_whole_exome_cram_compression
#SBATCH --output=trio_whole_exome_cram_compression.%A_%a.out
#SBATCH --error=trio_whole_exome_cram_compression.%A_%a.err

# Expects environment variables to be set
# PROJECT_ID - e.g. 12345_LastnameFirstname
# VERSION - e.g. v1, v2
# CONFIG_SH - absolute path to configuration script setting environment variables

source $CONFIG_SH

FAMILY_ID=`head -n $SLURM_ARRAY_TASK_ID $PARAMS_DIR/$PROJECT_ID.family_ids.txt | tail -n 1`

SHORT_PROJECT_ID=`echo $PROJECT_ID | cut -f 1 -d '_'`

# This assumes that ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID} is unique, and it should be -
# if there was a re-run of a family, it should have a new project id and version.
cd $OUTPUT_DIR/${SHORT_PROJECT_ID}_${VERSION}/families/*_${FAMILY_ID}

for BAM in */*.bam
do

  # 1. Compress to CRAM format without quality score binning
  CRAM=${BAM%.bam}.cram
  samtools view -@ $SLURM_CPUS_PER_TASK -T $REFERENCE_GENOME -C -o $CRAM $BAM

  # 2. Index the CRAM file - good sanity check
  samtools index $CRAM
  
  # 3. Compare the stats from the BAM and CRAM files
  samtools flagstat $BAM > $BAM.flagstat.txt
  samtools flagstat $CRAM > $CRAM.flagstat.txt
  diff $BAM.flagstat.txt $CRAM.flagstat.txt

done
