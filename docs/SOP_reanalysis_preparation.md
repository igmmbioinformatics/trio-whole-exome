# Standard operating procedure - Preparation for re-analysis of previously analyzed trio whole exome samples at the Edinburgh Parallel Computing Centre

This SOP applies to batches of family/trio samples where trio whole exome sequencing has been performed by Edinburgh Genomics (EdGE) or the Edinburgh Clinical Research Facility (ECRF). It assumes that these families have previously been successfully analyzed.

Definitions, Software and data requirements, PED file, Working directories are the same as in [SOP alignment and variant calling](SOP_alignment_variant_annotation.md).

## Project configuration

A [configuration script](../trio_whole_exome_variant_calling_config.sh) sets environment variables common to scripts used in this SOP.

## Template for bcbio configuration

Bcbio requires a [template file in YAML format](../trio_whole_exome_bcbio_variant_calling_template.yaml) to define the procedures run in the pipeline.

## Output

Per family: VCF file, CRAM files symlinked from previous analysis. Structure is otherwise the same as in [SOP alignment and variant calling](SOP_alignment_variant_annotation.md).

## Procedure - re-running variant calling for projects where variant calling configuration has changed since the previous analysis.

1. Set environment variable project_id and general configuration variables. All steps below can assume that these have been set. Old version is the previous analysis version, new version increments by 1.

```
project_id=<project_id>
short_project_id=`echo $project_id | cut -f 1 -d '_'`
old_version=<old_version>
new_version=<new_version>
source /home/u035/u035/shared/scripts/bin/trio_whole_exome_variant_calling_config.sh
```

2. List CRAM files to regenerate to BAM format

```
cd $WORK_DIR
mkdir ${short_project_id}_${new_version}
cd ${short_project_id}_${new_version}
ls $OUTPUT_DIR/${short_project_id}_${old_version}/families/*/*/*.cram > crams.txt
```

3. Re-generate BAM format files

```
cd $LOGS_DIR
N=`wc -l $WORK_DIR/${short_project_id}_${new_version}/crams.txt | awk '{ print $1 }'`
sbatch --export=PROJECT_ID=$project_id,VERSION=$new_version,CONFIG_SH=$SCRIPTS/trio_whole_exome_variant_calling_config.sh \
  --array=1-$N $SCRIPTS/submit_trio_wes_bam_decompression.sh
```

4. Configure to run variant calling from existing CRAM files.

```
cd $PARAMS_DIR
$SCRIPTS/trio_wes_prepare_variant_calling_bcbio_config.sh \
  $SCRIPTS/trio_whole_exome_variant_calling_config.sh $project_id $old_version $new_version \
  &> ${project_id}_${new_version}_`date +%Y%m%d%H%M`.log
X=`wc -l $PARAMS_DIR/$project_id.family_ids.txt | awk '{print $1}'`
```

5. Run variant calling, submitting the bcbio jobs from the logs folder.

```
cd $LOGS_DIR
sbatch --export=PROJECT_ID=$project_id,VERSION=$new_version,CONFIG_SH=$SCRIPTS/trio_whole_exome_variant_calling_config.sh \
  --array=1-$X $SCRIPTS/submit_trio_wes_bcbio.sh
```

6. On completion, symlink the CRAM files from the old version folder into the new version folder.

```
cd $OUTPUT_DIR/${short_project_id}_${new_version}/families
for ((i = 1; i <= $N; i = i + 1))
do
  cram_file=`head -n $i $WORK_DIR/${short_project_id}_${new_version}/crams.txt | tail -n 1`
  family_id=`dirname ${cram_file#$OUTPUT_DIR/${short_project_id}_${old_version}/families/*${short_project_id}_${old_version}_} | cut -f 1 -d '/'`
  bname=`basename $cram_file -ready.cram`
  link_path_suffix="$bname/$bname-ready.cram"
  link_path_prefix=`ls | grep $family_id$`
  link_path=$link_path_prefix/$link_path_suffix
  ln -s $cram_file $link_path
  ln -s $cram_file.crai $link_path.crai
done
```

7. Clean up.

```
rm -r $WORK_DIR/*$short_project_id* $LOGS_DIR/* $PARAMS_DIR/* $CONFIG_DIR/*
```

## Procedure - preparation of the reanalysis file structure

1. Create the basic file structure

```
cd $OUTPUT_DIR
mkdir `date +%Y%m%d`_reanalysis
cd `date +%Y%m%d`_reanalysis
mkdir families params prioritization previous_DECIPHER_upload
chmod -R g+w *
```

2. Put the project results folders to be used in `params/projects.txt`, e.g.

```
26960_v2
27430_v1
```

3. Link family folders from previous runs

```
cd families
for project in `cat ../params/projects.txt`
do
  for family in `ls ../../$project/families`
  do
    ln -s ../../$project/families/$family $family
  done
done
```

4. Create a tab-delimited text file of the family ids and identify families with multiple entries in the list. Remove all but the most recent run for these.

```
ls | sed -e 's/\_/\t/g' > ../params/families.txt

cd ../params
cut -f 5 families.txt | sort | uniq -d > families_multiple_entries.txt

rm family_entries_removed_from_reanalysis.txt 2> /dev/null
for family in `cat families_multiple_entries.txt`
do
  grep $family$ families.txt | sort -k4 -n | sed -e 's/\t/\_/g' > $family.txt
  count=`wc -l $family.txt | awk '{ print $1 - 1 }'`
  head -n $count $family.txt >> family_entries_removed_from_reanalysis.txt
  rm $family.txt
done

cd ../families
rm `cat ../params/family_entries_removed_from_reanalysis.txt`
ls | grep -v trio | grep -v duo | sed -e 's/\_/\t/g' | cut -f 2-5 | sed -e 's/\tv/_v/' | sort > ../params/all.txt
```

5. Pull in all the relevant PED files into the params folder

```
cd ../params
mkdir all
count=`wc -l all.txt | awk '{ print $1 }'`
for ((i = 1; i <= $count; i = i + 1))
do
  project=`head -n $i all.txt | tail -n 1 | cut -f 1`
  family=`head -n $i all.txt | tail -n 1 | cut -f 3`

  cp ../../$project/params/*$family*.ped ./all/
done
```

6. Make folders for the relevant groups and sort the families based on their PED files

```
mkdir trio singleton quad shared_affected trio_affected_parent
chmod u+w all/*.ped
cd all
X=`wc -l ../all.txt | awk '{ print $1 }'`
rm counts.txt 2> /dev/null
for ((i = 1; i <= $X; i = i + 1))
do
  project=`head -n $i ../all.txt | tail -n 1 | cut -f 1`
  batch=`head -n $i ../all.txt | tail -n 1 | cut -f 2`
  family=`head -n $i ../all.txt | tail -n 1 | cut -f 3`

  echo $project $batch $family `wc -l *$family.ped` >> counts.txt
done

perl $SCRIPTS/classify_ped_files.pl < counts.txt
cd ..

for group in singleton trio quad shared_affected trio_affected_parent
do
  for family in `cut -f 3 $group.txt`
  do
    mv all/*$family*.ped $group/
  done
done
mv all/*.ped unclassified/
```

7. Ensure all parent fields are set to 0 for the shared-affected families

```
cd shared_affected
for file in *.ped
do
  awk '{ print $1 "\t" $2 "\t0\t0\t" $5 "\t" $6 }' $file > temp.out
  mv temp.out $file
done
cd ..
```

8. Copy DECIPHER upload files from previous analyses. If multiple files are possible, select in preference order from the most recent re-analysis (if available), or from the most recent version otherwise.

a. `<INDI>_<FAM>_DECIPHER_v11.xlsx` (the best)
b. `<INDI>_<FAM>_DECIPHER_v10.xlsx` (if we do not have v11 version)
c. `<INDI>_<FAM>_DEC_FLT.csv` (least preferred, if we do not have a xlsx version -> this was the case at the beginning of the NHS Trio WES service)

Create a list of terms to exclude from path matches in `previous_DECIPHER_upload/exclude.txt`, e.g.

```
test
temp
```

```
cd ../previous_DECIPHER_upload

for family_folder in `ls ../families | grep -v duo | grep -v trio`
do
  project=`echo $family_folder | cut -f 2 -d '_'`
  version=`echo $family_folder | cut -f 3 -d '_'`
  batch=`echo $family_folder | cut -f 4 -d '_'`
  family=`echo $family_folder | cut -f 5 -d '_'`

  rm -r ${batch}_${family} 2> /dev/null
  mkdir ${batch}_${family}
  find $OUTPUT_DIR/$project*/prioritization/*/*_results/*$family* -type f -iname '*DECIPHER_v11.xlsx' 2> /dev/null | grep -v -f exclude.txt >> ${batch}_${family}/upload.txt
  find $OUTPUT_DIR/$project*/prioritization/*/*_results/*$family* -type f -iname '*DECIPHER_v10.xlsx' 2> /dev/null | grep -v -f exclude.txt >> ${batch}_${family}/upload.txt
  find $OUTPUT_DIR/$project*/prioritization/*/*_results/*$family* -type f -iname '*DEC_FLT.csv' 2> /dev/null | grep -v -f exclude.txt >> ${batch}_${family}/upload.txt
  find $OUTPUT_DIR/*_reanalysis/prioritization/*/results/*$family* -type f -iname '*DECIPHER_v11.xlsx' 2> /dev/null | grep -v -f exclude.txt >> ${batch}_${family}/upload.txt
  find $OUTPUT_DIR/*_reanalysis/prioritization/*/results/*$family* -type f -iname '*DECIPHER_v10.xlsx' 2> /dev/null | grep -v -f exclude.txt >> ${batch}_${family}/upload.txt
  find $OUTPUT_DIR/*_reanalysis/prioritization/*/results/*$family* -type f -iname '*DEC_FLT.csv' 2> /dev/null | grep -v -f exclude.txt >> ${batch}_${family}/upload.txt

  perl $SCRIPTS/select_decipher_upload_files.pl < ${batch}_${family}/upload.txt
done
```
