#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=8GB
#SBATCH --time=48:00:00
#SBATCH --job-name=coverage
#SBATCH --output=coverage.%A_%a.out
#SBATCH --error=coverage.%A_%a.err

# Expects environment variables to be set
# CONFIG_SH - absolute path to configuration script setting environment variables
# TARGET_FILE - absolute path to BED file of targets
# INPUT_FILES - list of BAM or CRAM format files for input
# OUTPUT_DIR - absolute path to output folder

INPUT_FILE=`head -n $SLURM_ARRAY_TASK_ID $INPUT_FILES | tail -n 1`
BNAME=`basename $INPUT_FILE`
OUTPUT_FILE=$OUTPUT_DIR/$BNAME

source $CONFIG_SH
export PATH=/home/u035/u035/shared/software/bcbio/anaconda/bin:$PATH

gatk DepthOfCoverage \
     -R $REFERENCE_GENOME \
     -L $TARGET_FILE \
     -I $INPUT_FILE \
     -O $OUTPUT/$OUTPUT_FILE \
     --omit-depth-output-at-each-base \
     --min-base-quality 20 \
     --read-filter MappingQualityAvailableReadFilter \
     --read-filter MappingQualityReadFilter \
     --minimum-mapping-quality 20 \
     --summary-coverage-threshold 1 \
     --summary-coverage-threshold 5 \
     --summary-coverage-threshold 10 \
     --summary-coverage-threshold 15 \
     --summary-coverage-threshold 20 \
     --summary-coverage-threshold 25 \
     --summary-coverage-threshold 30 \
     --summary-coverage-threshold 50
