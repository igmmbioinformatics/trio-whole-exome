#!/usr/bin/perl -w

use IO::File;
use strict;

my %all;
my $all_fh = new IO::File;
$all_fh->open("../params/all.txt", "r") or die "Could not open ../params/all.txt\n$!";

while (my $line = <$all_fh>)
{
    chomp $line;
    my ($project, $batch, $family) = split('\t', $line);

    $all{$family} = $batch;
}

$all_fh->close();

my %files;
while (my $line = <>)
{
    chomp $line;
    my @tokens = split('/', $line);
    my $filename = $tokens[-1];
    my $family = $tokens[-2];

    if ($line =~ /20220418_reanalysis/)
    {
	my $bare_family = $family;
	$bare_family =~ s/_shared//;
	
	my $new_family = $all{$bare_family} . "_" . $family;
#	print "Mapping $family to $new_family\n";
	$family = $new_family;
    }

    # get the last modified time of the file
    my $fh = new IO::File;
    $fh->open($line, "r") or die "Could not open $line\n$!";
    my $mtime = (stat($fh))[9];
    $fh->close();

    my $indv = "";
    my $filetype = "";
    if ($filename =~ /(.+)_DECIPHER_v11.xlsx$/)
    {
	$indv = $1;
	$filetype = "DECIPHER_v11.xlsx";
    }
    elsif ($filename =~ /(.+)_DECIPHER_v10.xlsx/)
    {
	$indv = $1;
	$filetype = "DECIPHER_v10.xlsx";
    }
    elsif ($filename =~ /(.+)_DEC_FLT.csv/)
    {
	$indv = $1;
	$filetype =~ "DEC_FLT.csv";
    }
    
    $files{$family}{$indv}{$filetype}{$filename}{$mtime} = $line;
}

foreach my $family (keys %files)
{
    foreach my $indv (keys %{ $files{$family} })
    {
	if (exists($files{$family}{$indv}{"DECIPHER_v11.xlsx"}))
	{
	    my @filenames = keys %{ $files{$family}{$indv}{"DECIPHER_v11.xlsx"} };
	    if ( scalar(@filenames) > 1)
	    {
		printf STDERR "More than one file for $family $indv DECIPHER_v11.xlsx: %s\n", join(", ", keys %{ $files{$family}{$indv}{"DECIPHER_v11.xlsx"} });
		exit;
	    }
	   
	    my @times = sort { $a <=> $b } keys %{ $files{$family}{$indv}{"DECIPHER_v11.xlsx"}{$filenames[0]} };

	    my $file_to_use = $files{$family}{$indv}{"DECIPHER_v11.xlsx"}{$filenames[0]}{$times[0]};
	    if (scalar(@times) > 0)
	    {
		$file_to_use = $files{$family}{$indv}{"DECIPHER_v11.xlsx"}{$filenames[0]}{$times[-1]};
	    }
	    
#	    `mkdir -p $family/$indv`;
#	    `cp $file_to_use $family/$indv/`;

	    printf "$family\t$indv\tDECIPHER_v11.xlsx\t$filenames[0]\t$times[-1]\t$file_to_use\n";
	}
	elsif (exists($files{$family}{$indv}{"DECIPHER_v10.xlsx"}))
	{
	    my @filenames = keys %{ $files{$family}{$indv}{"DECIPHER_v10.xlsx"} };
	    if ( scalar(@filenames) > 1)
	    {
		printf STDERR "More than one file for $family $indv DECIPHER_v10.xlsx: %s\n", join(", ", keys %{ $files{$family}{$indv}{"DECIPHER_v10.xlsx"} });
		exit;
	    }

	    my @times = sort { $a <=> $b } keys %{ $files{$family}{$indv}{"DECIPHER_v10.xlsx"}{$filenames[0]} };
	    my $file_to_use = $files{$family}{$indv}{"DECIPHER_v10.xlsx"}{$filenames[0]}{$times[0]};
	    if (scalar(@times) > 0)
	    {
		$file_to_use = $files{$family}{$indv}{"DECIPHER_v10.xlsx"}{$filenames[0]}{$times[-1]};
	    }

#	    `mkdir -p $family/$indv`;
#	    `cp $file_to_use $family/$indv/`;

	    printf "$family\t$indv\tDECIPHER_v10.xlsx\t$filenames[0]\t$times[-1]\t$file_to_use\n";
	}
	elsif (exists($files{$family}{$indv}{"DEC_FLT.csv"}))
	{
	    my @filenames = keys %{ $files{$family}{$indv}{"DEC_FLT.csv"} };
	    if ( scalar(@filenames) > 1)
	    {
		printf STDERR "More than one file for $family $indv DEC_FLT.csv: %s\n", join(", ", keys %{ $files{$family}{$indv}{"DEC_FLT.csv"} });
		exit;
	    }
	   
	    my @times = sort { $a <=> $b } keys %{ $files{$family}{$indv}{"DEC_FLT.csv"}{$filenames[0]} };

	    my $file_to_use = $files{$family}{$indv}{"DEC_FLT.csv"}{$filenames[0]}{$times[0]};
	    if (scalar(@times) > 0)
	    {
		$file_to_use = $files{$family}{$indv}{"DEC_FLT.csv"}{$filenames[0]}{$times[-1]};
	    }

#	    `mkdir -p $family/$indv`;
#	    `cp $file_to_use $family/$indv/`;

	    printf "$family\t$indv\tDEC_FLT.csv\t$filenames[0]\t$times[-1]\t$file_to_use\n";
	}
    }
}
