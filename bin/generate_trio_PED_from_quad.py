#	given a quad family PED file, 1 kid and 2 parents ids
#	generate a trio PED
#
#       Author: MH
#       last modified: SEPT 15, 2020



import sys
import os
import csv
import gzip


def go(inout_dir,quad_ped_file,kid_id,par_1_id,par_2_id):

    trio_ped_file = '%s/%s_%s.ped' % (inout_dir,quad_ped_file[:-4],kid_id)
    out_han = open(trio_ped_file,'w')
    out_cntr = 0

    in_han = open("%s/%s" % (inout_dir,quad_ped_file),'r')
    in_cntr = 0

    for line in in_han:
        in_cntr += 1
        data = [x.strip() for x in line.strip().split('\t')]
        indi_fam_id = data[1]
        if ((indi_fam_id.startswith(kid_id)) or (indi_fam_id.startswith(par_1_id)) or (indi_fam_id.startswith(par_2_id))):
            out_han.write(line)
            out_cntr += 1

    in_han.close()
    out_han.close()
    print "Found %s individuals in the quad ped file = %s/%s" % (in_cntr,inout_dir,quad_ped_file)
    print "Recorded %s individuals in %s trio ped file = %s" % (out_cntr,kid_id,trio_ped_file)





if __name__ == '__main__':
    if len(sys.argv) == 6:
        go(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5])
    else:
        print "Suggested use: time $PYTHON /home/u035/u035/shared/scripts/NHS_WES_generate_trio_ped.py ${PED_DIR} ${quad_ped_file} ${KID_ID} ${PAR_1_ID} ${PAR_2_ID}"
        raise SystemExit

