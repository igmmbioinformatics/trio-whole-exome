#!/usr/bin/perl -w

use IO::File;
use strict;

my $singleton_fh = new IO::File;
$singleton_fh->open("../singleton.txt", "w") or die "Could not open singleton.txt\n$!";

my $quad_fh = new IO::File;
$quad_fh->open("../quad.txt", "w") or die "Could not open quad.txt\n$!";

my $trio_fh = new IO::File;
$trio_fh->open("../trio.txt", "w") or die "Could not open trio.txt\n$!";

my $shared_affected_fh = new IO::File;
$shared_affected_fh->open("../shared_affected.txt", "w") or die "Could not open shared_affected.txt\n$!";

my $trio_affected_parent_fh = new IO::File;
$trio_affected_parent_fh->open("../trio_affected_parent.txt", "w") or die "Could not open trio_affected_parent.txt\n$!";

my $unclassified_fh = new IO::File;
$unclassified_fh->open("../unclassified.txt", "w") or die "Could not open unclassified.txt\n$!";

while (my $line = <>)
{
    chomp $line;
    my ($project, $batch, $family, $count, $ped_file) = split(/\s+/, $line);

    my $classified = 0;
    
    if ($count == 1)
    {
	print $singleton_fh "$project\t$batch\t$family\n";
	$classified = 1;
    }

    if ($count == 3 && !$classified)
    {
	my $ped_fh = new IO::File;
	$ped_fh->open($ped_file, "r") or die "Could not open $ped_file\n$!";

	my $affected_proband = 0;
	my $unaffected_parent = 0;
	my $affected_parent = 0;
	while (my $line2 = <$ped_fh>)
	{
	    chomp $line2;
	    my ($family, $indiv, $father, $mother, $sex, $affected) = split(/\t/, $line2);

	    if ($father ne "0" && $mother ne "0" && $affected == 2)
	    {
		$affected_proband++;
	    }
	    elsif ($father eq "0" && $mother eq "0" && $affected == 1)
	    {
		$unaffected_parent++;
	    }
	    elsif ($father eq "0" && $mother eq "0" && $affected == 2)
	    {
		$affected_parent++;
	    }
	}

	if ($affected_proband == 1 && $unaffected_parent == 2)
	{
	    print $trio_fh "$project\t$batch\t$family\n";
	    $classified = 1;
	}
	elsif ($affected_proband == 1 && $unaffected_parent == 1 && $affected_parent == 1)
	{
	    print $trio_affected_parent_fh "$project\t$batch\t$family\n";
	    $classified = 1;
	}
    }

    if ($count == 4 && !$classified)
    {
	my $ped_fh = new IO::File;
	$ped_fh->open($ped_file, "r") or die "Could not open $ped_file\n$!";

	my $affected_proband = 0;
	my $unaffected_parent = 0;
	my $affected_parent = 0;
	while (my $line2 = <$ped_fh>)
	{
	    chomp $line2;
	    my ($family, $indiv, $father, $mother, $sex, $affected) = split(/\t/, $line2);

	    if ($father ne "0" && $mother ne "0" && $affected == 2)
	    {
		$affected_proband++;
	    }
	    elsif ($father eq "0" && $mother eq "0" && $affected == 1)
	    {
		$unaffected_parent++;
	    }
	    elsif ($father eq "0" && $mother eq "0" && $affected == 2)
	    {
		$affected_parent++;
	    }
	}

	if ($affected_proband == 2 && $unaffected_parent == 2)
	{
	    print $quad_fh "$project\t$batch\t$family\n";
	    $classified = 1;
	}
    }
    
    if (!$classified)
    {
	my $ped_fh = new IO::File;
	$ped_fh->open($ped_file, "r") or die "Could not open $ped_file\n$!";

	my $affected_count = 0;
	while (my $line2 = <$ped_fh>)
	{
	    chomp $line2;
	    my ($family, $indiv, $father, $mother, $sex, $affected) = split(/\t/, $line2);

	    if ($affected == 2)
	    {
		$affected_count++;
	    }
	}

	if ($affected_count == $count)
	{
	    print $shared_affected_fh "$project\t$batch\t$family\n";
	    $classified = 1;
	}
	else
	{
	    print $unclassified_fh "$project\t$batch\t$family\n";
	}
    }
}
