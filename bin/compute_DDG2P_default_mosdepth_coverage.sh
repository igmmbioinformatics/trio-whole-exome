#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=16GB
#SBATCH --time=24:00:00
#SBATCH --job-name=DDG2P_default_mosdepth_cov
#SBATCH --output=DDG2P_default_mosdepth_cov.%A_%a.out
#SBATCH --error=DDG2P_default_mosdepth_cov.%A_%a.err


#       Author: MH
#       last modified: SEPT 15, 2023


# setup PATH
export PATH=$PATH:/home/u035/u035/shared/software/bcbio/anaconda/envs/python2/bin:/home/u035/u035/shared/software/bcbio/anaconda/bin
export PERL5LIB=$PERL5LIB:/home/u035/u035/shared/software/bcbio/anaconda/lib/site_perl/5.26.2



export MOSDEPTH_Q0=NO_COVERAGE
export MOSDEPTH_Q1=LOW_COVERAGE
export MOSDEPTH_Q2=CALLABLE

MOSDEPTH=/mnt/e1000/home/u035/u035/shared/software/bcbio/galaxy/../anaconda/bin/mosdepth



## The current TWIST gene panel BED file
#TARGETS=/home/u035/u035/shared/resources/exome_targets/hg38_Twist_ILMN_Exome_2.0_Plus_Panel_annotated_June2022.plus15bp.bed

# The current DDG2P gene panel BED file
TARGETS=/home/u035/u035/shared/resources/G2P/DDG2P.20230817.plus15bp.merged.bed






#################################
######  for each BAM file    ####
#################################
#BAM_FILE=`head -n ${SLURM_ARRAY_TASK_ID} ${BAM_LIST} | tail -n 1`
#
## get the ID (Illumina Exome) to generate the out file name
#IFS='/' read -r -a array <<< "${BAM_FILE}"
#ID="${array[9]}"

#time ${MOSDEPTH} -t 16 -F 1804 -Q 1 --no-per-base --by ${TARGETS} --quantize 0:1:4: --thresholds 20,50 ${ID}.DDG2P ${BAM_FILE}


#################################
#####  for each CRAM file    ####
#################################
CRAM_FILE=`head -n ${SLURM_ARRAY_TASK_ID} ${CRAM_LIST} | tail -n 1`

# get the ID (Illumina Exome) to generate the out file name
IFS='/' read -r -a array <<< "${CRAM_FILE}"
ID="${array[9]}"

REFERENCE_GENOME=/home/u035/u035/shared/software/bcbio/genomes/Hsapiens/hg38/seq/hg38.fa

time ${MOSDEPTH} -t 16 -F 1804 -Q 1 --no-per-base --by ${TARGETS} --quantize 0:1:4: --thresholds 20,50 --fasta ${REFERENCE_GENOME} ${ID}.DDG2P ${CRAM_FILE}





