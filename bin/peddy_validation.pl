#!/usr/bin/perl -w

=head1 NAME

peddy_validation.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@igmm.ed.ac.uk)

=head1 DESCRIPTION

Checks the parent-child and parent-parent relationships from peddy output.

=cut

use strict;

use Cwd;
use Getopt::Long;
use IO::File;

my $usage = qq{USAGE:
$0 [--help]
  --output   Output file
  --families Family directory
  --ped      Pedigree file for project
};

my $help = 0;
my $ped_file;
my $fam_dir;
my $out_file;

GetOptions(
    'help'       => \$help,
    'ped=s'      => \$ped_file,
    'output=s'   => \$out_file,
    'families=s' => \$fam_dir,
) or die $usage;

if ($help || !$ped_file || !$out_file || !$fam_dir)
{
    print $usage;
    exit(0);
}

# Read in the pedigree file
my $in_fh = new IO::File;
$in_fh->open($ped_file, "r") or die "Could not open $ped_file\n$!";

my %ped;
while (my $line = <$in_fh>)
{
	chomp $line;
	my ($family_id, $individual_id, $father_id, $mother_id, $sex, $aff) = split(/\t/, $line);

	if ($family_id !~ /\_/)
	{
	    $individual_id = $individual_id  . "_" . $family_id;
	    $father_id = $father_id  . "_" . $family_id;
	    $mother_id = $mother_id  . "_" . $family_id;
	}
	
	$ped{$family_id}{'count'}++;
	$ped{$family_id}{$individual_id}{'father'} = $father_id;
	$ped{$family_id}{$individual_id}{'mother'} = $mother_id;
	if ($aff == 2)
	{
		$ped{$family_id}{'aff'} = $individual_id;
	}
}

$in_fh->close();

my $out_fh = new IO::File;
$out_fh->open($out_file, "w") or die "Could not open $out_file\n$!";

printf $out_fh "sample_a\tsample_b\tpedigree_parents\tpredicted_parents\tparent_error\n";

my %out_lines;
foreach my $family_id (sort keys %ped)
{
    my $glob_str = sprintf("$fam_dir/*%s*/*%s*/qc/peddy/*.ped_check.csv", $family_id, $ped{$family_id}{'aff'});
    my @peddy_glob = glob($glob_str);
    if (scalar(@peddy_glob) == 0)
    {
	printf STDERR "No ped_check.csv files found for $family_id\n";
	next;
    }

    foreach my $peddy_file (@peddy_glob)
    {
	my $peddy_fh = new IO::File;
	$peddy_fh->open($peddy_file, "r") or die "Could not open $peddy_file\n$!";

	my @headers;
	my %info;
	my @sample_pairs;
	while (my $line = <$peddy_fh>)
	{
		chomp $line;
		if ($line =~ /^sample_a/)
		{
			@headers = split(/,/, $line);
		}
		else
		{
			my @data = split(/,/, $line);
			push(@sample_pairs, sprintf("%s\t%s", $data[0], $data[1]));
			for (my $i = 2; $i < scalar(@headers); $i++)
			{
				$info{$headers[$i]}{sprintf("%s\t%s", $data[0], $data[1])} = $data[$i];
			}
		}
	}

	$peddy_fh->close();

	foreach my $sample_pair (@sample_pairs)
	{
		my ($sample_a, $sample_b) = split(/\t/, $sample_pair);

		if ($ped{$family_id}{$sample_a}{'father'} eq $sample_b ||
		    $ped{$family_id}{$sample_a}{'mother'} eq $sample_b ||
		    $ped{$family_id}{$sample_b}{'father'} eq $sample_a ||
		    $ped{$family_id}{$sample_b}{'mother'} eq $sample_a)
		{
			$info{'pedigree_parents'}{$sample_pair} = 'True';
		}

		$info{'parent_error'}{$sample_pair} = $info{'pedigree_parents'}{$sample_pair} eq $info{'predicted_parents'}{$sample_pair} ? 'False' : 'True';

		my $out_line = sprintf "$sample_pair\t%s\t%s\t%s", 
		    $info{'pedigree_parents'}{$sample_pair}, 
		    $info{'predicted_parents'}{$sample_pair},
		    $info{'parent_error'}{$sample_pair};

		$out_lines{$out_line}++;
	}
    }
}

printf $out_fh join("\n", sort keys %out_lines);

$out_fh->close();
