#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=2GB
#SBATCH --time=01:00:00
#SBATCH --job-name=batch_init
#SBATCH --output=batch_init.%A_%a.out
#SBATCH --error=batch_init.%A_%a.err

#       Author: MH
#       last modified: AUG 02, 2022


### Setup the folder structure for the downstream analysis###
BASE=/home/u035/u035/shared/analysis/work
WORK_DIR=$BASE/${PROJECT_ID}
VCF_DIR=${WORK_DIR}/VCF
PED_DIR=${WORK_DIR}/PED
LOG_DIR=${WORK_DIR}/LOG
G2P_DIR=${WORK_DIR}/G2P
VASE_DIR=${WORK_DIR}/VASE
COV_DIR=${WORK_DIR}/COV
DEC_DIR=${WORK_DIR}/DECIPHER
IGV_DIR=${DEC_DIR}/IGV
CNV_DIR=${WORK_DIR}/CNV
BAMOUT_DIR=${WORK_DIR}/BAMOUT
SCRIPTS_DIR=/home/u035/u035/shared/scripts/bin


### Tools
PYTHON2=/home/u035/u035/shared/software/bcbio/anaconda/envs/python2/bin/python2.7


# print out the input
echo "SOURCE_DIR = ${SOURCE_DIR}"	# the general path to the source VCF, BAM and PED files			i.e. /home/u035/u035/shared/results
echo "BATCH_ID = ${BATCH_ID}"		# the ID of the batch being processed 					e.g. 19650_Ansari_Morad
echo "BATCH_NUM = ${BATCH_NUM}"		# the numerical part of the BATCH_ID					e.g. 19650
echo "PLATE_ID = ${PLATE_ID}" 		# the PCR plate ID of the batch being currently processed, 		e.g. 19285
echo "PROJECT_ID = ${PROJECT_ID}"	# this the the folder (${BASE}/${PROJECT_ID}) where the downstream analysis will be done
echo "VERSION_N = ${VERSION_N}"         # the version of the alignment and genotyping analysis






# create the working dir and the required subfolders
mkdir ${WORK_DIR}
mkdir ${VCF_DIR}
mkdir ${PED_DIR}
mkdir ${LOG_DIR}
mkdir ${G2P_DIR}
mkdir ${VASE_DIR}
mkdir ${COV_DIR}
mkdir ${DEC_DIR}
mkdir ${IGV_DIR}
mkdir ${CNV_DIR}
mkdir ${BAMOUT_DIR}
echo "Created ${WORK_DIR} for this batch and all the required subfolders"




######################################################
###   Copy the VCF and PED file per each family    ###
######################################################

SOURCE_VCF_DIRS=${SOURCE_DIR}/${BATCH_NUM}_${VERSION_N}/families/????-??-??_${BATCH_NUM}_${VERSION_N}_${PLATE_ID}_*
SOURCE_PED_DIR=${SOURCE_DIR}/${BATCH_NUM}_${VERSION_N}/params



for S_VCF_DIR in ${SOURCE_VCF_DIRS}
do
#  echo "  ${S_VCF_DIR}"
  VCF_DIR_NAME="${S_VCF_DIR##*/}"
#  echo "    ${VCF_DIR_NAME}"
  IFS=_ read -ra my_arr <<< "${VCF_DIR_NAME}"
  FAM_ID=${my_arr[-1]}
#  echo "      BATCH = ${BATCH_ID}, PLATE = ${PLATE_ID}, FAM_ID = ${FAM_ID}"
  echo "  FAM_ID = ${FAM_ID}"

  # construct the VCF and PED file names for this family
  S_VCF_FILE=${S_VCF_DIR}/${PLATE_ID}_${FAM_ID}-gatk-haplotype-annotated.vcf.gz
  S_PED_FILE=${SOURCE_PED_DIR}/${BATCH_ID}_${PLATE_ID}_${FAM_ID}.ped


  # copy the trio VCF and PED files
  cp ${S_VCF_FILE} ${VCF_DIR}
  cp ${S_PED_FILE} ${PED_DIR}
  echo "    copied ${S_VCF_FILE} --> ${VCF_DIR}"
  echo "    copied ${S_PED_FILE} --> ${PED_DIR}"

done





######################################################################################
### generate the FAM_IDs.txt, PRO_IDs.txt and FAM_PRO.txt *only for trio* families ###
######################################################################################

time ${PYTHON2} ${SCRIPTS_DIR}/extract_trio_FAM_PRO_ID.py ${WORK_DIR}


######################################################################################
### generate the FAM_IDs.txt, PRO_IDs.txt and FAM_PRO.txt *for singleton* families ###
######################################################################################

time ${PYTHON2} ${SCRIPTS_DIR}/extract_solo_FAM_PRO_ID.py ${WORK_DIR}



echo ""
echo ""
echo "OK: Setup for PROJECT_ID = $PROJECT_ID successful"










