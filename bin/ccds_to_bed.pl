#!/usr/bin/perl -w

=head1 NAME

ccds_to_bed.pl

=head1 AUTHOR

Alison Meynert (alison.meynert@ed.ac.uk)

=head1 DESCRIPTION

Converts CCDS to BED format, appending exon number to CCDS id.

=cut

use strict;

use Getopt::Long;
use IO::File;

my $usage = qq{USAGE:
$0 [--help]
  --input  CCDS text file
  --output BED file
};

my $help = 0;
my $input_file;
my $output_file;

GetOptions(
    'help'     => \$help,
    'input=s'  => \$input_file,
    'output=s' => \$output_file
) or die $usage;

if ($help || !$input_file || !$output_file)
{
    print $usage;
    exit(0);
}

my $in_fh = new IO::File;
$in_fh->open($input_file, "r") or die "Could not open $input_file\n$!";
my $out_fh = new IO::File;
$out_fh->open($output_file, "w") or die "Could not open $output_file\n$!";

# Read in the CCDS file
while (my $line = <$in_fh>)
{
    next if ($line =~ /accession/);
    
    chomp $line;
    # chromosome	nc_accession	gene	gene_id	ccds_id	ccds_status	cds_strand	cds_from	cds_to	cds_locations	match_type
    my ($chr, $acc, $gene, $gene_id, $ccds_id, $ccds_status, $cds_strand, $cds_from, $cds_to, $cds_locations, $match_type)  = split(/\t/, $line);

    next if ($ccds_status ne "Public");
    next if ($cds_locations eq "-");

    $cds_locations =~ /\[(.+)\]/;
    my $locations = $1;
    my @exons = split(", ", $locations);
    if ($cds_strand eq '-')
    {
	@exons = reverse(@exons);
    }

    my $i = 1;
    foreach my $exon (@exons)
    {
	my ($start, $end) = split("-", $exon);
	$start = $start - 1;
	print $out_fh "chr$chr\t$start\t$end\t$gene-$ccds_id.$i\n";
	$i++;
    }
}

$in_fh->close();
$out_fh->close();
