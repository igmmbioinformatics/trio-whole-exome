nextflow.enable.dsl = 2

process check_md5s {
    /*
    For a fastq file, find its md5 file and run md5sum -c. Output the fastq name if
    invalid, otherwise empty string `''`.
    */
    label 'small'
    errorStrategy 'ignore'

    input:
    val(fastq_file)  // getParent() gives null object errors if this is a path

    output:
    stdout

    script:
    """
    edgen_dir=${fastq_file.getParent().getParent()}
    edgen_md5_file=\$edgen_dir/md5sums.txt
    edgen_fastq=${fastq_file.getParent().getName()}/${fastq_file.getName()}

    crf_dir=${fastq_file.getParent()}
    crf_md5_file="\$(find \$crf_dir -name *md5.txt | head -n 1)"
    crf_fastq=${fastq_file.getName()}

    local_md5_file=${fastq_file}.md5

    if [ -f \$edgen_md5_file ]
    then
        cd \$edgen_dir
        md5sum --quiet -c <(cat md5sums.txt | grep \$edgen_fastq) | cut -d ':' -f 1

    elif [ -f \$crf_md5_file ]
    then
        cd \$crf_dir
        md5sum  --quiet -c <(cat \$crf_md5_file | grep \$crf_fastq) | cut -d ':' -f 1

    elif [ -f \$local_md5_file ]
    then
        cd ${fastq_file.getParent()}
        md5sum --quiet -c \$local_md5_file | cut -d ':' -f 1

    else
        echo "Could not find md5 file for $fastq_file"
        exit 1
    fi
    """
}


workflow validation {
    /*
    Take a parsed samplesheet, flatten it and parse into a channel of observed vs.
    expected checksums. Calls check_errors above to raise an exception upon any
    mismatches.
    */

    take:
        ch_indv_info

    main:
        ch_fastqs = ch_indv_info.map(
            { indv, family, father, mother, sex, affected, r1, r2 ->
                [r1, r2]
            }
        )
        .flatten()
        .map({file(it)})

        invalid_md5s = check_md5s(ch_fastqs)
            .filter({it != ''})  // filter out the '' results from valid md5s
            .collect()
            .map(
                {
                    if (it != null) {  // invalid md5s found
                        throw new PipelineException("Invalid md5s detected:\n${it.join('')}")
                    }
                }
            )
}
