#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=16GB
#SBATCH --time=24:00:00
#SBATCH --job-name=compute_mosdepth_with_overlap_cov
#SBATCH --output=compute_mosdepth_with_overlap_cov.%A_%a.out
#SBATCH --error=compute_mosdepth_with_overlap_cov.%A_%a.err


#       Author: MH
#       last modified: SEPT 13, 2023


# setup PATH
export PATH=$PATH:/home/u035/u035/shared/software/bcbio/anaconda/envs/python2/bin:/home/u035/u035/shared/software/bcbio/anaconda/bin
export PERL5LIB=$PERL5LIB:/home/u035/u035/shared/software/bcbio/anaconda/lib/site_perl/5.26.2



export MOSDEPTH_Q0=NO_COVERAGE
export MOSDEPTH_Q1=LOW_COVERAGE
export MOSDEPTH_Q2=CALLABLE

MOSDEPTH=/mnt/e1000/home/u035/u035/shared/software/bcbio/galaxy/../anaconda/bin/mosdepth


# The current TWIST gene panel BED file
TARGETS=/home/u035/u035/shared/resources/exome_targets/hg38_Twist_ILMN_Exome_2.0_Plus_Panel_annotated_June2022.plus15bp.bed


################################
#####  for each BAM file    ####
################################
BAM_FILE=`head -n ${SLURM_ARRAY_TASK_ID} ${BAM_LIST} | tail -n 1`

# get the ID (Illumina Exome) to generate the out file name
IFS='/' read -r -a array <<< "${BAM_FILE}"
ID="${array[9]}"


time ${MOSDEPTH} --fast-mode -t 16 -F 1804 -Q 1 --no-per-base --by ${TARGETS} --quantize 0:1:4: ${ID} ${BAM_FILE}



