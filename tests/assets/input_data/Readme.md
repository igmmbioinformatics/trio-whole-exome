# Test input data

The test data here consists of:

## Edinburgh Genomics test data

Family 000001 - an example of a complete trio. Individuals: 000001, 000002, 000003
Family 000002 - an incomplete trio with only one parent. Individuals: 000004, 000005
Family 000003 - a singleton test case for an unexpected MD5 checksum. Individuals: 000006

Todo: more EG test data from a different download date - maybe a singleton, complete duo and a quad

## CRF

todo: CRF test data
