#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=2GB
#SBATCH --time=6:00:00
#SBATCH --job-name=trio_whole_exome_family_checksums
#SBATCH --output=trio_whole_exome_family_checksums.%A_%a.out
#SBATCH --error=trio_whole_exome_family_checksums.%A_%a.err

# Expects environment variables to be set
# PROJECT_ID - e.g. 12345_LastnameFirstname
# VERSION - e.g. v1, v2
# CONFIG_SH - absolute path to configuration script setting environment variables

source $CONFIG_SH

FAMILY_ID=`head -n $SLURM_ARRAY_TASK_ID $PARAMS_DIR/$PROJECT_ID.family_ids.txt | tail -n 1`

SHORT_PROJECT_ID=`echo $PROJECT_ID | cut -f 1 -d '_'`

# This assumes that ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID} is unique, and it should be -
# if there was a re-run of a family, it should have a new project id and version.
cd $OUTPUT_DIR/${SHORT_PROJECT_ID}_${VERSION}/families/*_${FAMILY_ID}

rm md5sum.txt 2> /dev/null

for file in `find . -type f | grep -v '\.bam'`
do
  md5sum $file >> md5sum.txt
done
