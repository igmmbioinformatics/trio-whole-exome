# 19573 EdGe

## Issues

* 447226 - sample 131873 (father) is very low depth (6X), comes up as non-paternity for 114891 as a result but this can be ignored
* 453955 - sample 132058 (proband) comes up as incorrect sex due to het ratio, annotated as female in PED file, no evidence of Y coverage - will run as female.

## Notes

* no contamination issues
* low depth but still usable samples
  * 438938 - sample 131858 (proband) 33X
  * 433285 - sample 132221 (proband) 36X
* XYY observable in X/Y coverage
  * 452542 - sample 131311 (proband) 
  * 438235 - sample 131376 (father)
* Possible amplification of a region on chr4 - increased coverage observed in QC
  * 438371 samples 131201 (proband), 131202 (mother)
  * 454213 samples 132222 (proband), 132223 (father)
