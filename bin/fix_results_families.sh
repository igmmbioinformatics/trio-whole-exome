#!/usr/bin/bash

nonduos=$1
duos=$2
batch_id=$3

for family_id in `cat ${nonduos}`
do
#    mv *_${family_id} 202[0-9]*${family_id}/
    cd *$family_id
    mv ${batch_id}${family_id}-gatk-haplotype-annotated.vcf.gz ${batch_id}_${family_id}-gatk-haplotype-annotated.vcf.gz
    mv ${batch_id}${family_id}-gatk-haplotype-annotated.vcf.gz.tbi ${batch_id}_${family_id}-gatk-haplotype-annotated.vcf.gz.tbi
    cd ..
done

for family_id in `cat ${duos}`
do
#    mv *_${family_id} 202[0-9]*${family_id}duo/
    cd *${family_id}duo
    mv ${batch_id}${family_id}-gatk-haplotype-annotated.vcf.gz ${batch_id}_${family_id}-gatk-haplotype-annotated.vcf.gz
    mv ${batch_id}${family_id}-gatk-haplotype-annotated.vcf.gz.tbi ${batch_id}_${family_id}-gatk-haplotype-annotated.vcf.gz.tbi
    cd ..
done
