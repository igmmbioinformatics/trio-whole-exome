nextflow.enable.dsl = 2

prioritisation_out = "${params.output_dir}/${params.pipeline_project_id}_${params.pipeline_project_version}/prioritization/${params.prioritisation_version}"
conda_python2 = 'python=2.7'
conda_xlsxwriter = 'xlsxwriter=1.1.5'
conda_vt = 'bioconda::vt=0.57721'
conda_bcftools ='bioconda::bcftools=1.9'
conda_tabix = 'bioconda::tabix=1.11'  // tabix 1.9 isn't available on Conda
conda_vep = 'bioconda::ensembl-vep=100.4'
conda_vase = 'bioconda::vase=0.5.1'  // 0.4.2 not available
conda_gatk4 = 'bioconda::gatk4=4.2.1.0'
conda_gatk3 = 'bioconda::gatk=3.8'
conda_samtools = 'bioconda::samtools=1.9'



def conda_env(env) {
    // if running stub tests, don't use Conda
    if (workflow.profile.contains("stubs")) {
        return null
    } else {
        return env
    }
}



process trio_setup {
    label 'small'

    publishDir "${prioritisation_out}", mode: 'copy'

    input:
    val(family_peds)

    output:
    path('FAM_IDs.txt')
    path('PRO_IDs.txt')
    path('FAM_PRO.txt')

    script:
    """
    extract_trio_FAM_PRO_ID.py ${family_peds.join(' ')} --exclude-families ${params.exclude_families.join(' ')}
    """
}


// Todo: fix affected parents


process process_trio {
    label 'medium'

    conda conda_env("${params.bcbio}/anaconda/envs/java")

    publishDir "${prioritisation_out}", mode: 'copy'

    input:
    tuple(
        val(family_id),  // from FAM_IDs.txt
        path(family_ped),
        path(family_vcf),
        val(plate_id),
        val(proband_id),
        val(proband_bam),
        val(par_1_id),
        val(par1_bam),
        val(par_2_id),
        val(par2_bam)
    )
    path(decipher_file)

    output:
    path("VCF/*", emit: vcf)
    //path("PED/*", emit: ped)
    path("BAMOUT/*", emit: bamout)
    path("COV/*", emit: cov)
    //path("CNV/*", emit: cnv)
    path("DECIPHER/*.*", emit: decipher)
    path("DECIPHER/IGV/*", emit: decipher_igv)
    path("G2P/*", emit: g2p)
    //path("LOG/*", emit: log)
    path("VASE/*", emit: vase)

    script:
    """
    mkdir VCF
    mkdir PED
    mkdir LOG
    mkdir G2P
    mkdir VASE
    mkdir COV
    mkdir -p DECIPHER/IGV
    mkdir CNV
    mkdir BAMOUT

    export FAMILY_ID=$family_id
    export FAMILY_PED=$family_ped
    export FAMILY_VCF=$family_vcf
    export PLATE_ID=$plate_id
    export PROBAND_ID=$proband_id
    export PROBAND_BAM=$proband_bam
    export PAR_1_ID=$par_1_id
    export PAR_1_BAM=$par1_bam
    export PAR_2_ID=$par_2_id
    export PAR_2_BAM=$par2_bam

    export BCBIO=${params.bcbio}
    export REFERENCE_GENOME=${params.reference_genome}
    export BLACKLIST=${params.blacklist}
    export TRANS_MAP=${params.trans_map}
    export REC_SNP=${params.recurrent_snps}
    export G2P_TARGETS=${params.g2p_targets}
    export LIST_24_CHR=${params.list_24_chr}
    export CLINVAR=${params.clinvar}
    export GATK3=${params.gatk3}
    export SCRIPTS_DIR=${params.scripts_dir}
    export DEC_MAP=${decipher_file}

    process_trio.sh $family_id ${params.bcbio}
    """

    stub:
    """
    vcf_base=${family_vcf.getName().replace('-gatk-haplotype-annotated.vcf.gz', '')}
    mkdir VCF G2P VASE COV
    mkdir -p DECIPHER/IGV BAMOUT/${proband_id}
    touch VCF/\$vcf_base.{decomp,norm,DNU,ready}.vcf.gz
    touch VCF/\$vcf_base.{AC0,clean}.vcf
    touch G2P/${plate_id}_${family_id}{_inter_out.txt{,_summary.html},.report.{html,txt}}
    touch G2P/3631209.txt
    touch VASE/\$vcf_base.ready.denovo.vcf
    touch COV/${proband_id}_${family_id}.DD15.COV.txt
    touch COV/${proband_id}_${family_id}.DD15.sample_{cumulative_coverage_{counts,proportions},interval_{statistics,summary},statistics,summary}
    touch COV/${proband_id}_${family_id}.REC_SNP_COV.txt
    for i in \$(cat $family_ped | cut -f 2 | tr '\n' ' ')
    do
        touch "VCF/\$vcf_base.ready.\$i.vcf.gz"
    done

    touch DECIPHER/${proband_id}_${family_id}_{DEC_FLT.csv,DECIPHER_v10.xlsx}
    touch DECIPHER/IGV/${proband_id}_${family_id}.snapshot.FLT.csv
    touch DECIPHER/IGV/bamout_${proband_id}_${family_id}.snapshot.txt

    touch BAMOUT/${family_id}/${family_id}_chr{2,4,7,16,22,X}_12345678.bamout.{bam,bai,vcf,vcf.idx}
    """
}


process dnu_clean {
    conda conda_env("$conda_python2 $conda_vt $conda_bcftools $conda_tabix")

    input:
    val(trio_input)
    path(reference_fa)
    path(reference_fai)
    path(g2p_targets)
    path(blacklist)

    output:
    val(trio_input, emit: trio_data)
    path("${base}.ready.vcf.gz", emit: ready_vcf)
    path("${base}.ready.vcf.gz.tbi", emit: ready_vcf_tbi)
    path("${base}.clean.vcf", emit: clean_vcf)

    publishDir "${prioritisation_out}/VCF", mode: 'copy', pattern: '*.ready.vcf.gz*'

    // DNU and clean the family VCF, e.g. ${PLATE_ID}_${FAMILY_ID}-gatk-haplotype-annotated.vcf.gz,
    // plus deal with strand bias variants
    script:
    base = "${trio_input.plate_id}_${trio_input.family_id}"
    dnu_vcf = "${base}.DNU.vcf.gz"
    sb_exome_vcf = "${base}.SB_exome.vcf.gz"
    sb_ddg2p_vcf = "${base}.SB_DDG2P.vcf.gz"
    sb_vcf = "${base}.SB.vcf.gz"

    """
    vt decompose -s ${trio_input.family_vcf} -o ${base}.decomp.vcf.gz &&
    vt normalize ${base}.decomp.vcf.gz -r ${reference_fa} -o ${base}.norm.vcf.gz &&
    vt uniq ${base}.norm.vcf.gz -o ${dnu_vcf} &&

    # identify variants (exome wide) which fail the strand bias filter: FS >= 60 and SOR >= 3
    bcftools filter --include "INFO/FS>=60 & INFO/SOR>=3" ${dnu_vcf} | bgzip > ${sb_exome_vcf} &&
    tabix -p vcf ${sb_exome_vcf} &&

    # select only those that are within/overlap the DDG2P panel
    bcftools view --regions-file ${g2p_targets} ${sb_exome_vcf} | bgzip > ${sb_ddg2p_vcf} &&
    tabix -p vcf ${sb_ddg2p_vcf} &&

    echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" &&
    echo " FAMILY_ID = ${trio_input.family_id}: the VCF file containing excluded DDG2P variants with strand bias (FS >= 60 and SOR >= 3): " &&
    echo "    ${sb_ddg2p_vcf}" &&
    echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^" &&

    # exclude the strand bias variants
    bcftools filter --exclude "INFO/FS>=60 & INFO/SOR>=3" ${dnu_vcf} | bgzip > ${sb_vcf} &&
    tabix -p vcf ${sb_vcf} &&

    # remove sites with AC=0
    bcftools view --min-ac=1 --no-update ${dnu_vcf} > ${base}.AC0.vcf &&

    # reset GT to no-call if num_ALT < num_ALT_THERSH or VAF < VAF_THRESH and GT != 0/0
    # exclude variants from the blacklist (matching on chr,pos,ref,alt)
    filter_LQ_GT.py ${blacklist} ${base}.AC0.vcf ${base}.clean.vcf &&

    # bgzip and tabix it
    cat ${base}.clean.vcf | bgzip > ${base}.ready.vcf.gz &&
    tabix -p vcf ${base}.ready.vcf.gz
    """

    stub:
    base = "${trio_input.plate_id}_${trio_input.family_id}"
    """
    touch ${base}.ready.vcf.gz{,.tbi}
    touch ${base}.clean.vcf
    """
}


process g2p {
    // run G2P for each family VCF (DD genes), e.g. ${PLATE_ID}_${FAMILY_ID}.clean.vcf

    conda conda_env(conda_vep)

    input:
    val(trio_data)
    path(clean_vcf)
    path(reference_fa)
    path(reference_fai)
    path(g2p_target_csv)
    path(g2p_genes)
    path(vep_cache)
    path(vep_plugins)

    publishDir "${prioritisation_out}/G2P", mode: 'copy'

    output:
    tuple(
        val(trio_data),
        path("${base}_LOG_DIR/${base}_inter_out.txt"),
        path("${base}_LOG_DIR/${base}_inter_out.txt_summary.html"),
        path("${base}_LOG_DIR/${base}.report.html"),
        path("${base}_LOG_DIR/${base}.report.txt"),
        path("${base}_LOG_DIR/*.txt")
    )

    script:
    base = "${trio_data.plate_id}_${trio_data.family_id}"
    """
    G2P_LOG_DIR=${base}_LOG_DIR
    TXT_OUT=\${G2P_LOG_DIR}/${base}.report.txt
    HTML_OUT=\${G2P_LOG_DIR}/${base}.report.html
    VCF_KEYS='gnomADe_r2.1.1_GRCh38|gnomADg_r3.1.1_GRCh38'

    vep \
        -i ${clean_vcf} \
        --output_file \${G2P_LOG_DIR}/${base}_inter_out.txt \
        --force_overwrite \
        --assembly GRCh38 \
        --fasta ${reference_fa} \
        --offline \
        --merged \
        --use_given_ref \
        --cache --cache_version 100 \
        --dir_cache ${vep_cache} \
        --individual all \
        --transcript_filter "gene_symbol in ${g2p_genes}" \
        --dir_plugins ${vep_plugins} \
        --plugin G2P,file='${g2p_target_csv}',af_from_vcf=1,confidence_levels='definitive&strong&moderate',af_from_vcf_keys='gnomADe_r2.1.1_GRCh38|gnomADg_r3.1.1_GRCh38',log_dir=\${G2P_LOG_DIR},txt_report=\${TXT_OUT},html_report=\${HTML_OUT}
    """

    stub:
    base = "${trio_data.plate_id}_${trio_data.family_id}"
    """
    log_dir=${base}_LOG_DIR
    mkdir \$log_dir
    touch \$log_dir/${base}{_inter_out.txt{,_summary.html},.report.{html,txt}}
    touch \$log_dir/3631209.txt
    """
}


process vase {
    // run VASE for each family VCF (de novo)

    conda conda_env("$conda_vase $conda_tabix")

    input:
    val(trio_data)
    path(ready_vcf)
    path(ready_vcf_tbi)

    output:
    val(trio_data, emit: trio_data)
    path("${base}.strict.denovo.vcf", emit: strict_denovo_vcf)

    script:
    base = "${trio_data.plate_id}_${trio_data.family_id}"
    """
    OUT_FILE=${base}.strict.denovo.vcf

    vase \
        -i ${ready_vcf} \
        -o \${OUT_FILE} \
        --log_progress \
        --prog_interval 100000 \
        --freq 0.0001 \
        --gq 30 --dp 10 \
        --het_ab 0.3 \
        --max_alt_alleles 1 \
        --csq all \
        --biotypes all \
        --control_gq 15 --control_dp 5 \
        --control_het_ab 0.01 \
        --control_max_ref_ab 0.05 \
        --de_novo \
        --ped ${trio_data.family_ped}


    # for the cases where one of the parents is also affected, VASE de novo will crash not being able to find parents for the affected parent
    # not producing any output file, which trips the pipeline downstream
    # to handle: check if VASE denovo produced an output and if not (as it will be for such cases)
    # create an empty VCF headered output == no denovos found)

    if [ -f "\${OUT_FILE}" ]; then
        echo "VASE denovo completed successfully"
    else
        echo "WARNING: VASE denovo has not produced an output (e.g. affected parent), generate an empty VCF one (with headers)"
        tabix -H ${ready_vcf} > \${OUT_FILE}
    fi
    """

    stub:
    base = "${trio_data.plate_id}_${trio_data.family_id}"
    """
    touch ${base}.strict.denovo.vcf
    """
}


process filter_denovo_vcfs {
    // run VASE for each family VCF (de novo)

    conda conda_env("$conda_gatk4 $conda_bcftools")

    input:
    val(trio_data)
    path(strict_denovo_vcf)
    path(reference_fa)
    path(reference_fai)
    path(reference_dict)
    path(list_24_chr)

    publishDir "${prioritisation_out}/VASE", mode: 'copy'

    output:
    tuple(
        val(trio_data),
        path("${base}.ready.denovo.vcf")
    )

    script:
    base = "${trio_data.plate_id}_${trio_data.family_id}"
    """
    strict_24chr_denovo_vcf=${base}.strict.24chr.denovo.vcf
    strict_24chr_sort_denovo_vcf=${base}.strict.24chr.sort.denovo.vcf

    # do some filtering on the denovo VCFs - exclude variants not on the 24 chr, as well as variants in LCR and telomere/centromere regions
    ### actually, ignore the filtering of variants in LCR and telomere/centromere regions --> more variants with  “Unknown” status may be classified as “denovo” if enough support
    # index the denovo VCF
    gatk IndexFeatureFile -I ${strict_denovo_vcf}

    # select only variants on the 24 chromosomes
    gatk SelectVariants -R ${reference_fa} -V ${strict_denovo_vcf} -O \$strict_24chr_denovo_vcf -L $list_24_chr --exclude-non-variants

    # sort the VCF (maybe not needed?, but just in case, and it is quick)
    grep '^#' \$strict_24chr_denovo_vcf > \$strict_24chr_sort_denovo_vcf \
    && grep -v '^#' \$strict_24chr_denovo_vcf | LC_ALL=C sort -t \$'\t' -k1,1V -k2,2n >> \$strict_24chr_sort_denovo_vcf
    # index the sorted VCF
    gatk IndexFeatureFile -I \$strict_24chr_sort_denovo_vcf

    # split multi-allelic sites [by -m -any]
    # left-alignment and normalization [by adding the -f]
    bcftools norm -f ${reference_fa} -m -any -Ov -o ${base}.ready.denovo.vcf \$strict_24chr_sort_denovo_vcf
    """

    stub:
    base = "${trio_data.plate_id}_${trio_data.family_id}"
    """
    touch ${base}.ready.denovo.vcf
    """
}


process coverage {
    // run coverage for each proband (DD genes)
    // format: ${BATCH_NUM}_${VERSION_N}/families/????-??-??_${BATCH_NUM}_${VERSION_N}_${PLATE_ID}_${FAMILY_ID}/${INDI_ID}_${FAMILY_ID}/${INDI_ID}_${FAMILY_ID}-ready.bam

    conda conda_env("$conda_python2 $conda_gatk3")

    input:
    val(trio_data)
    path(reference_fa)
    path(reference_fai)
    path(reference_dict)
    path(g2p_targets)
    path(gatk3)
    path(clinvar)

    publishDir "${prioritisation_out}/COV", mode: 'copy'

    output:
    path("${full_proband}*DD15*")

    script:
    full_proband = "${trio_data.proband_id}_${trio_data.family_id}"
    """
    # make sure we are reading the data from the exact batch & plate ID
    OUT_FILE=${full_proband}.DD15

    java -Xmx8g -jar ${gatk3} -T DepthOfCoverage -R ${reference_fa} -o \${OUT_FILE} -I ${trio_data.proband_bam} -L ${g2p_targets} \
        --omitDepthOutputAtEachBase \
        --minBaseQuality 20 \
        --minMappingQuality 20 \
        -ct 20 \
        -jdk_deflater \
        -jdk_inflater \
        --allow_potentially_misencoded_quality_scores &&

    echo "percentage of DD exons (+/-15bp) covered at least 20x in PROBAND_ID = ${full_proband} ..."
    cat \${OUT_FILE}.sample_summary | awk '{print \$7}' &&

    # now compute the coverage per DD exon (+/-15bp) interval, adding the number of P/LP ClinVar variants (assertion criteria provided) in each interval
    get_cov_output.py \${OUT_FILE}.sample_interval_summary ${clinvar} \${OUT_FILE}.COV.txt
    """

    stub:
    full_proband = "${trio_data.proband_id}_${trio_data.family_id}"
    """
    touch ${full_proband}.DD15.COV.txt
    touch ${full_proband}.DD15.sample_{cumulative_coverage_{counts,proportions},interval_{statistics,summary},statistics,summary}
    """
}

process recurrent_denovo_snp_cov {
    // check the coverage per each of the recurent de novo SNPs (padded with 15bp both directions)

    conda conda_env(conda_samtools)
    input:
    val(trio_data)
    path(rec_snp)

    publishDir "${prioritisation_out}/COV", mode: 'copy'

    output:
    path("${trio_data.proband_id}_${trio_data.family_id}.REC_SNP_COV.txt")

    script:
    """
    # we have identified the name of the proband's BAM file above (PROBAND_BAM), reuse it
    # set the name of the file containing info about the coverage of the recurrent SNPs
    REC_OUT_FILE=${trio_data.proband_id}_${trio_data.family_id}.REC_SNP_COV.txt

    while IFS=\$'\t' read -ra var; do
        gene="\${var[0]}"
        chr="\${var[1]}"
        pos="\${var[2]}"
        lo=\$(expr \$pos - 15)
        hi=\$(expr \$pos + 15)
        reg="\$lo-\$hi"
        echo "============================================="
        echo "\$gene : recurrent variant at \$chr:\$pos"
        echo "exploring coverage at \$chr:\$reg"

        echo "---------------------------------------------"
        echo "precisely at the position"
        samtools depth -aa -Q 20 -r \$chr:\$reg ${trio_data.proband_bam} | grep "\$pos"

        echo "---------------------------------------------"
        echo "average in the +/- 15bp region"
        samtools depth -aa -Q 20 -r \$chr:\$reg ${trio_data.proband_bam} | awk '{sum+=\$3} END { print "Average = ",sum/NR}'

        echo "---------------------------------------------"
        echo "detailed in the +/- 15bp region"
        samtools depth -aa -Q 20 -r \$chr:\$reg ${trio_data.proband_bam}
    done < ${rec_snp} > ${trio_data.proband_id}_${trio_data.family_id}.REC_SNP_COV.txt
    """

    stub:
    """
    touch ${trio_data.proband_id}_${trio_data.family_id}.REC_SNP_COV.txt
    """
}

process split_family_vcf {
    conda conda_env(conda_bcftools)

    input:
    val(trio_data)
    path(ready_vcf)
    path(ready_vcf_tbi)
    path(reference_fa)
    path(reference_fai)

    publishDir "${prioritisation_out}/VCF", mode: 'copy'

    output:
    tuple(
        val(trio_data),
        path("${trio_data.plate_id}_${trio_data.family_id}.ready.${trio_data.proband_id}_${trio_data.family_id}.vcf.gz"),  // proband vcf
        path("${trio_data.plate_id}_${trio_data.family_id}.ready.${trio_data.mother_id}_${trio_data.family_id}.vcf.gz"),  // mother_vcf
        path("${trio_data.plate_id}_${trio_data.family_id}.ready.${trio_data.father_id}_${trio_data.family_id}.vcf.gz"),  // father_vcf
    )

    script:
    """
    # split the family VCF to individual VCFs
    # -c1:  minimum allele count (INFO/AC) of sites to be printed
    # split multi-allelic sites (by -m -any)
    # left-alignment and normalization (by adding the -f)

    file=${ready_vcf}
    echo "splitting \$file"
    for indi in `bcftools query -l \$file`; do
        indv_rough_vcf_gz=\${file/.vcf*/.\$indi.rough.vcf.gz}
        bcftools view -c1 -Oz -s \$indi -o \$indv_rough_vcf_gz \$file
        bcftools norm -f ${reference_fa} -m -any -Oz -o \${file/.vcf*/.\$indi.vcf.gz} \$indv_rough_vcf_gz
    done
    """

    stub:
    """
    for i in ${trio_data.proband_id} ${trio_data.mother_id} ${trio_data.father_id}
    do
        touch ${trio_data.plate_id}_${trio_data.family_id}.ready.\${i}_${trio_data.family_id}.vcf.gz
    done
    """
}


process generate_decipher_file {
    // for each proband generate the DECIPHER file
    // ${VCF_DIR}/${VCF_BASE}.ready.vcf.gz - the cleaned family VCF
    // ${VASE_DIR}/${VCF_BASE}.ready.denovo.vcf - the VASE file
    // ${TRANS_MAP} - the current transcript mapping file

    conda conda_env("$conda_python2 $conda_xlsxwriter")

    input:
    val(trio_data)
    path(vase_ready_denovo_vcf)
    path(decipher_internal_ids)
    path(g2p_file)
    path(trans_map)

    publishDir "${prioritisation_out}/DECIPHER", mode: 'copy'

    output:
    val(trio_data, emit: trio_data)
    path("IGV/${full_proband}.snapshot.FLT.txt", emit: snapshot_flt_txt)
    path("${full_proband}_DEC_FLT.csv", emit: dec_flt_csv)
    path("${full_proband}_DECIPHER_v10.xlsx", emit: dec_v10_xlsx)


    script:
    full_proband = "${trio_data.proband_id}_${trio_data.family_id}"
    """
    # VASE file - already split, left-aligned and normalized
    mkdir IGV

    ## call the python script
    generate_DEC_IGV_scripts.py \
        ${decipher_internal_ids} \
        ${trans_map} \
        ${trio_data.family_ped} \
        ${g2p_file} \
        ${vase_ready_denovo_vcf} \
        ${trio_data.plate_id}_${trio_data.family_id} \
        ${trio_data.proband_vcf} \
        ${trio_data.mother_vcf} \
        ${trio_data.father_vcf} \
        ${trio_data.plate_id} \
        ${trio_data.family_id} \
        ${trio_data.proband_bam} \
        ${trio_data.mother_bam} \
        ${trio_data.father_bam} \
        ${full_proband}_DEC_FLT.csv \
        IGV/${full_proband}.snapshot.FLT.txt &&

    ## using the DECIPHER bulk upload file v9 --> generate the DECIPHER bulk upload file v10
    echo "...Generating v10 Decipher bulk upload file for proband = ${trio_data.proband_id}, family_id = ${trio_data.family_id} ..." &&
    convert_DEC_to_v10.py ${full_proband} ${full_proband}_DEC_FLT.csv ${full_proband}_DECIPHER_v10.xlsx
    """

    stub:
    full_proband = "${trio_data.proband_id}_${trio_data.family_id}"
    """
    mkdir IGV
    touch IGV/${full_proband}.snapshot.FLT.txt
    touch ${full_proband}_{DEC_FLT.csv,DECIPHER_v10.xlsx}
    """
}


process igv_snapshot_bamouts {
    // for each variant in the DECIPHER upload file
    // generate a IGV snapshot based on the realigned BAM used by GATK for calling variants
    // first, generate BAMOUTs for each variant (to be stored in the BAMOUT folder)
    // then, generate a batch script for IGV to produce the snapshots based on the BAMOUTs

    conda conda_env(conda_gatk4)

    input:
    val(trio_data)
    path(proband_dec_flt_csv)
    path(reference_fa)
    path(reference_fai)
    path(reference_dict)

    publishDir "${prioritisation_out}/BAMOUT", mode: 'copy', pattern: "${trio_data.family_id}/*.bamout.*"

    output:
    val(trio_data, emit: trio_data)
    path("${trio_data.family_id}/*.bamout.*", emit: bamout, optional: true)
    path(proband_dec_flt_csv, emit: proband_dec_flt_csv)

    script:
    """
    echo "...Generating BAMOUT files for the ${trio_data.family_id} family, proband = ${trio_data.proband_id} ..."

    # gather the trio BAM files
    echo "...kid_bam = ${trio_data.proband_bam}..."
    echo "...par_1_bam = ${trio_data.mother_bam}..."
    echo "...par_2_bam = ${trio_data.father_bam}..."

    # gather the variants in the DECIPHER file for which to generate bamouts
    # chr is the second column - need to add the 'chr' prefix
    # pos is the third column
    # the first line is a header line, starting with 'Internal reference number or ID'
    # file called: <proband_id>_<fam_id>_DEC_FLT.csv
    # and for each run GATK to generate the bamout files
    # to be stored in \${BAMOUT_DIR}/\${FAMILY_ID}

    mkdir -p ${trio_data.family_id} &&

    echo "... reading ${proband_dec_flt_csv} to generate the bamouts..." &&

    grep -v '^Internal' ${proband_dec_flt_csv} |
    while IFS= read -r line
    do
        echo "\$line"
        IFS=, read -ra ary <<<"\$line"
        chr=\${ary[1]}
        pos=\${ary[2]}
        ref=\${ary[4]}
        alt=\${ary[5]}
        echo " --> chr = \$chr, pos = \$pos, ref = \${ref}, alt = \${alt}"

        # generate the bamout file
        echo "...doing the bamout"

        gatk HaplotypeCaller \
            --reference ${reference_fa} \
            --input ${trio_data.proband_bam} \
            --input ${trio_data.mother_bam} \
            --input ${trio_data.father_bam} \
            -L chr\${chr}:\${pos} \
            --interval-padding 500 --active-probability-threshold 0.000 -ploidy 2 \
            --output ${trio_data.family_id}/${trio_data.family_id}_chr\${chr}_\${pos}.bamout.vcf \
            -bamout ${trio_data.family_id}/${trio_data.family_id}_chr\${chr}_\${pos}.bamout.bam
    done
    """

    stub:
    """
    mkdir ${trio_data.family_id}
    touch ${trio_data.family_id}/${trio_data.family_id}_chr{2,4,7,16,22,X}_12345678.bamout.{bam,bai,vcf{,.idx}}
    """
}

process generate_igv_snapshots {
    // write the IGV batch file for this family based on the bamouts
    // to be stored as DECIPHER/IGV/bamout_${PROBAND_ID}_${FAMILY_ID}.snapshot.txt

    input:
    val(trio_data)
    path(proband_dec_flt_csv)

    publishDir "${prioritisation_out}/DECIPHER/IGV", mode: 'copy'

    output:
    path("bamout_${full_proband}.snapshot.txt")

    script:
    full_proband = "${trio_data.proband_id}_${trio_data.family_id}"
    """
    snap_file=bamout_${full_proband}.snapshot.txt &&

    # check if previous version exist, if so - delete it
    if [ -f "\$snap_file" ]; then
        echo "previous version of \$snap_file exist --> deleted"
        rm \$snap_file
    fi &&

    # write the header for the IGV batch file
    echo "new" >> \$snap_file
    echo "genome hg38" >> \$snap_file
    echo "snapshotDirectory \"${trio_data.plate_id}_${trio_data.family_id}\"" >> \$snap_file
    echo "" >> \$snap_file

    # now, go again over the variants in the DECIPHER file and generate one snapshot file for all the variants
    echo "... reading ${proband_dec_flt_csv} to generate the IGV batch file using the bamouts..."

    grep -v '^Internal' ${proband_dec_flt_csv} |
    while IFS= read -r line
    do
        IFS=, read -ra ary <<<"\$line"
        chr=\${ary[1]}
        pos=\${ary[2]}
        ref=\${ary[4]}
        alt=\${ary[5]}
        left=\$((\${pos}-25))
        right=\$((\${pos}+25))

        echo "new" >> \$snap_file
        echo "load ${trio_data.family_id}/${trio_data.family_id}_chr\${chr}_\${pos}.bamout.bam" >> \$snap_file
        echo "preference SAM.SHADE_BASE_QUALITY true" >> \$snap_file

        echo "goto chr\${chr}:\${left}-\${right}" >> \$snap_file
        echo "group SAMPLE" >> \$snap_file
        echo "sort base" >> \$snap_file
        echo "squish" >> \$snap_file
        echo "snapshot bamout_${full_proband}_chr\${chr}_\${pos}_\${ref}_\${alt}.png" >> \$snap_file
        echo "" >> \$snap_file
        echo "" >> \$snap_file

    done

    echo "Generating of the IGV batch files based on bamouts - done!"
    echo "snap_file = \$snap_file"
    """

    stub:
    full_proband = "${trio_data.proband_id}_${trio_data.family_id}"
    """
    touch bamout_${full_proband}.snapshot.txt
    """
}


workflow read_inputs {
    main:
        // ch_family_ids: [family1, family2, ...]
        ch_family_ids = Channel.fromPath(params.ped_file)
            .splitCsv(sep: '\t')
            .map({line -> line[0]})
            .flatten()
            .unique()

        ch_family_peds = ch_family_ids
            .map(
                {
                    it -> [
                        it,
                        "${params.output_dir}/${params.pipeline_project_id}_${params.pipeline_project_version}/params/${it}.ped"
                    ]
                }
            )

    emit:
        ch_family_ids = ch_family_ids
        ch_family_peds = ch_family_peds
}

workflow prioritisation_setup {
    read_inputs()

    trio_setup(
        read_inputs.out.ch_family_peds.map(
            {family_id, ped -> ped}
        ).collect()
    )
    report = """
    Family ID file available at:\n
    ${params.output_dir}/${params.pipeline_project_id}_${params.pipeline_project_version}/prioritization/${params.prioritisation_version}/FAM_IDs.txt

    Download this file for translation to DECIPHER IDs, and upload the resulting DECIPHER_INTERNAL_IDS.txt file to ${params.output_dir}/${params.pipeline_project_id}_${params.pipeline_project_version}/prioritization/${params.prioritisation_version}/
    """
    println report.stripIndent()
}


workflow prioritisation {
    read_inputs()

    pipeline_dir = "${params.output_dir}/${params.pipeline_project_id}_${params.pipeline_project_version}"
    prioritisation_dir = "$pipeline_dir/prioritization/${params.prioritisation_version}"

    ch_probands = Channel.fromPath("$prioritisation_dir/FAM_PRO.txt")
    .splitCsv(sep: '\t')

    ch_vcfs = Channel.fromPath("$pipeline_dir/families/*/*-gatk-haplotype-annotated.vcf.gz")
    .map({ it -> [it.getName().replace('-gatk-haplotype-annotated.vcf.gz', ''), it] })

    ch_bams = Channel.fromPath("$pipeline_dir/families/*/*/*-ready.bam")
    .map({[it.getParent().getName(), it]})

    ch_family_ped_data = Channel.fromPath(params.ped_file)
    .splitCsv(sep: '\t')
    .filter({it[2] != '0' && it[3] != '0'})
    // todo: remove redundancy with FAM_PRO.txt
    .map(
        {
            family_id, proband, father, mother, sex, affected ->
                [proband, father, mother, family_id]
        }
    ).join(ch_bams)
    .map(
        {
            proband, father, mother, family_id, proband_bam ->
                [father, proband, proband_bam, mother, family_id]
        }
    )
    .join(ch_bams)
    .map(
        {
            father, proband, proband_bam, mother, family_id, father_bam ->
            [mother, proband, proband_bam, father, father_bam, family_id]
        }
    )
    .join(ch_bams)
    .map(
        {
            mother, proband, proband_bam, father, father_bam, family_id, mother_bam ->
            [family_id, proband, proband_bam, father, father_bam, mother, mother_bam]
        }
    )

    ch_trio_inputs = read_inputs.out.ch_family_peds.map(
        {full_family_id, ped ->
            [full_family_id.split('_')[1], full_family_id, ped]
    })
    .join(ch_probands)
    .map(
        {short_family_id, full_family_id, ped, proband ->
            [full_family_id, ped, short_family_id, proband]
        }
    )
    .join(ch_vcfs)
    .join(ch_family_ped_data)
    .map(
        {
            full_family_id, ped, short_family_id, proband, vcf, full_proband,
            proband_bam, full_father, father_bam, full_mother, mother_bam ->
            [
                family_id: full_family_id.split('_')[1],
                family_ped: ped,
                family_vcf: vcf,
                plate_id: full_family_id.split('_')[0],
                proband_id: proband,
                proband_bam: proband_bam,
                father_id: full_father.split('_')[0],
                father_bam: father_bam,
                mother_id: full_mother.split('_')[0],
                mother_bam: mother_bam
            ]
        }
    )

    ch_decipher_file = Channel.fromPath("$prioritisation_dir/DECIPHER_INTERNAL_ID*.txt").collect()
    ch_reference_fa = Channel.fromPath(params.reference_genome, checkIfExists: true).collect()
    ch_reference_fai = Channel.fromPath("${params.reference_genome}.fai", checkIfExists: true).collect()
    ch_reference_dict = Channel.fromPath("${params.reference_dict}", checkIfExists: true).collect()
    ch_blacklist = Channel.fromPath(params.blacklist, checkIfExists: true).collect()
    ch_list_24_chr = Channel.fromPath(params.list_24_chr, checkIfExists: true).collect()
    ch_g2p_target_bed = Channel.fromPath(params.g2p_target_bed, checkIfExists: true).collect()
    ch_g2p_target_csv = Channel.fromPath(params.g2p_target_csv, checkIfExists: true).collect()
    ch_g2p_genes = Channel.fromPath(params.g2p_genes, checkIfExists: true).collect()
    ch_gatk3 = Channel.fromPath(params.gatk3, checkIfExists: true).collect()
    ch_clinvar = Channel.fromPath(params.clinvar, checkIfExists: true).collect()
    ch_rec_snp = Channel.fromPath(params.recurrent_snps, checkIfExists: true).collect()
    ch_trans_map = Channel.fromPath(params.trans_map, checkIfExists: true).collect()
    ch_vep_cache = Channel.fromPath(params.vep_cache, checkIfExists: true).collect()
    ch_vep_plugins = Channel.fromPath(params.vep_plugins, checkIfExists: true).collect()

    dnu_clean(ch_trio_inputs, ch_reference_fa, ch_reference_fai, ch_g2p_target_bed, ch_blacklist)
    g2p(dnu_clean.out.trio_data, dnu_clean.out.clean_vcf, ch_reference_fa, ch_reference_fai, ch_g2p_target_csv, ch_g2p_genes, ch_vep_cache, ch_vep_plugins)
    vase(dnu_clean.out.trio_data, dnu_clean.out.ready_vcf, dnu_clean.out.ready_vcf_tbi)
    filter_denovo_vcfs(vase.out.trio_data, vase.out.strict_denovo_vcf, ch_reference_fa, ch_reference_fai, ch_reference_dict, ch_list_24_chr)
    coverage(ch_trio_inputs, ch_reference_fa, ch_reference_fai, ch_reference_dict, ch_g2p_target_bed, ch_gatk3, ch_clinvar)
    recurrent_denovo_snp_cov(ch_trio_inputs, ch_rec_snp)
    split_family_vcf(dnu_clean.out.trio_data, dnu_clean.out.ready_vcf, dnu_clean.out.ready_vcf_tbi, ch_reference_fa, ch_reference_fai)

    split_family_vcf.out.map({it -> [it[0].family_id, it]})
        .join(filter_denovo_vcfs.out.map({trio_data, ready_denovo_vcf -> [trio_data.family_id, ready_denovo_vcf]}))
        .join(
            g2p.out.map(
                {trio_data, inter_out, inter_out_summary, report_html, report_txt, x_txt -> [trio_data.family_id, report_txt]}
            )
        )
        .multiMap(
            { family_id, split_data, ready_denovo, g2p_report ->
                family_id: family_id
                trio_data: [
                    family_id: split_data[0].family_id,
                    family_ped: split_data[0].family_ped,
                    family_vcf: split_data[0].family_vcf,
                    plate_id: split_data[0].plate_id,
                    proband_id: split_data[0].proband_id,
                    proband_bam: split_data[0].proband_bam,
                    proband_vcf: split_data[1],
                    father_id: split_data[0].father_id,
                    father_bam: split_data[0].father_bam,
                    father_vcf: split_data[3],
                    mother_id: split_data[0].mother_id,
                    mother_bam: split_data[0].mother_bam,
                    mother_vcf: split_data[2]
                ]
                ready_denovo_vcf: ready_denovo
                proband_g2p_report: g2p_report
            }
        )
        .set({ch_trios_with_vcfs})

    generate_decipher_file(ch_trios_with_vcfs.trio_data, ch_trios_with_vcfs.ready_denovo_vcf, ch_decipher_file, ch_trios_with_vcfs.proband_g2p_report, ch_trans_map)
    igv_snapshot_bamouts(generate_decipher_file.out.trio_data, generate_decipher_file.out.dec_flt_csv, ch_reference_fa, ch_reference_fai, ch_reference_dict)
    generate_igv_snapshots(igv_snapshot_bamouts.out.trio_data, igv_snapshot_bamouts.out.proband_dec_flt_csv)
}
