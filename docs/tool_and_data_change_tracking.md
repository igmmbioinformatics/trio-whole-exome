Software | bcbio-1.1.5 (ultra) | bcbio-1.2.3 (ultra) | bcbio-1.2.8 (ultra2) | bcbio-1.2.8 (ultra2 tools upgrade) | Notes
---------|-------------|-------------|-------------|------|-------
bcbio-nextgen | 1.1.5-b | **1.2.3** | **1.2.8** | 1.2.8 | Pipeline
bcftools | 1.9 | **1.10.2** | **1.9** | 1.9 | VCF manipulation suite
gatk4 | 4.1.2.0 | **4.1.8.1** | **4.2.1.0** | **4.2.5.0** | Alignment post-processing and variant calling
picard | 2.20.5 | **2.23.3** | **2.25.7** | **2.26.10** | VCF/BAM manipulation suite - only one VCF tool used
samtools | 1.9 | 1.9 | 1.9 | 1.9 | BAM manipulation suite 
variant-effect-predictor | 97.3 | **100.4** | 100.4 | 100.4 | VCF annotations
bamtools | 2.4.0 | 2.4.0 | 2.4.0 | 2.5.1 | BAM manipulation suite
bcbio-variation | 0.2.6 | 0.2.6 | 0.2.6 | 0.2.6 
bedtools | 2.27.1 | 2.27.1 | **2.30.0** | 2.30.0 | BED file manipulation suite
biobambam | 2.0.87 | 2.0.87 | 2.0.87 | 2.0.87 | BAM manipulation suite
bwa | 0.7.17 | 0.7.17 | 0.7.17 | 0.7.17 
fastqc | 0.11.8 | 0.11.8 | 0.11.8 | 0.11.8 | QC checks on FASTQ files
grabix | 0.1.8 | 0.1.8 | 0.1.8 | 0.1.8 |
vt | 2015.11.10 | 2015.11.10 | 2015.11.10 | 2015.11.10 |
vase | 0.2.4 | **0.4** | **0.4.2** | 0.4.2 | Identification of de novo variants
rtg-tools | 3.10.1 | **3.11** | 3.11 | 3.11 | Used for GIAB concordance analysis

Resource | bcbio-1.1.5 (ultra) | bcbio-1.2.3 (ultra) | bcbio-1.2.8 (ultra2) | Notes
---------|-------------|-------------|-------------|------
seq | 1000g-20150219_1 | 1000g-20150219_1 | 1000g-20150219_1 | 
bwa | 1000g-20150219 | 1000g-20150219 | 1000g-20150219 | 
ccds | r20 | r20 | r20 | 
capture_regions | 20161202 | 20161202 | 20161202 | 
coverage | 2018-10-16 | 2018-10-16 | 2018-10-16 | 
prioritize | 20181227 | 20181227 | 20181227 | 
dbsnp | 151-20180418 | **153-20180725** | **154-20210112** | VCF annotations
hapmap_snps | 20160105 | 20160105 | 20160105 | 
1000g_omni_snps | 20160105 | 20160105 | 20160105 | 
ACMG56_genes | 20160726 | 20160726 | 20160726 | 
1000g_snps | 20160105 | 20160105 | 20160105 | 
mills_indels | 20160105 | 20160105 | 20160105 | 
1000g_indels | 2.8_hg38_20150522 | 2.8_hg38_20150522 | 2.8_hg38_20150522 | 
clinvar | 20190513 | 20190513 | **20210110** | VCF annotations, not used
qsignature | 20160526 | 20160526 | 20160526 | 
genesplicer | 2004.04.03 | 2004.04.03 | 2004.04.03 | 
effects_transcript | 2017-03-16 | 2017-03-16 | 2017-03-16 | 
varpon | 20181105 | 20181105 | 20181105 | 
vcfanno | 20190119 | 20190119 | **20210204** | VCF annotations, not used
viral | 2017.02.04 | 2017.02.04 | 2017.02.04 | 
gnomad genomes | 2.1 | **3.0** | **3.1.1** | VCF annotations, used by G2P for MAF thresholding
gnomad exomes | 2.1 | 2.1 | **2.1.1** | VCF annotations, used by G2P for MAF thresholding
dbnsfp | 3.5a | not installed | not installed | VCF annotations, not used
