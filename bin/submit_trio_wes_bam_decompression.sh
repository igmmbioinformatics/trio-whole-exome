#!/bin/bash
#SBATCH --cpus-per-task=16
#SBATCH --mem=8GB
#SBATCH --time=48:00:00
#SBATCH --job-name=trio_whole_exome_bam_decompression
#SBATCH --output=trio_whole_exome_bam_decompression.%A_%a.out
#SBATCH --error=trio_whole_exome_bam_decompression.%A_%a.err

# Expects environment variables to be set
# PROJECT_ID - e.g. 12345_LastnameFirstname
# VERSION - e.g. v1, v2
# CONFIG_SH - absolute path to configuration script setting environment variables

source $CONFIG_SH

SHORT_PROJECT_ID=`echo $PROJECT_ID | cut -f 1 -d '_'`
CRAM_FILE=`head -n $SLURM_ARRAY_TASK_ID $WORK_DIR/${SHORT_PROJECT_ID}_${VERSION}/crams.txt | tail -n 1`

BARE_FAMILY_ID=`basename $CRAM_FILE -ready.cram | cut -f 2 -d '_'`
FAMILY_ID=`echo $CRAM_FILE | sed -e 's|/|\n|g' | grep $BARE_FAMILY_ID | head -n 1 | cut -d '_' -f 5`
BAM_FILE=`basename $CRAM_FILE .cram`.bam

cd $WORK_DIR/${SHORT_PROJECT_ID}_${VERSION}

mkdir -p $FAMILY_ID
cd $FAMILY_ID

samtools view -@ $SLURM_CPUS_PER_TASK -T $REFERENCE_GENOME -hbo $BAM_FILE $CRAM_FILE
samtools index $BAM_FILE
