#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=2GB
#SBATCH --time=01:00:00
#SBATCH --job-name=delete_bam
#SBATCH --output=delete_bam.%A_%a.out
#SBATCH --error=delete_bam.%A_%a.err


#       Author: MH
#       last modified: AUG 04, 2022



### Setup the folder structure for the downstream analysis###
BASE=/home/u035/u035/shared/analysis/work
WORK_DIR=$BASE/${PROJECT_ID}
VCF_DIR=${WORK_DIR}/VCF
PED_DIR=${WORK_DIR}/PED
LOG_DIR=${WORK_DIR}/LOG
G2P_DIR=${WORK_DIR}/G2P
VASE_DIR=${WORK_DIR}/VASE
COV_DIR=${WORK_DIR}/COV
DEC_DIR=${WORK_DIR}/DECIPHER
IGV_DIR=${DEC_DIR}/IGV
CNV_DIR=${WORK_DIR}/CNV
BAMOUT_DIR=${WORK_DIR}/BAMOUT
SCRIPTS_DIR=/home/u035/u035/shared/scripts/bin



echo "SOURCE_DIR = ${SOURCE_DIR}"       # the general path to the source VCF, BAM and PED files                 i.e. /home/u035/u035/shared/results
echo "BATCH_ID = ${BATCH_ID}"           # the ID of the batch being processed                                   e.g. 19650_Ansari_Morad
echo "BATCH_NUM = ${BATCH_NUM}"         # the numerical part of the BATCH_ID                                    e.g. 19650
echo "PLATE_ID = ${PLATE_ID}"           # the PCR plate ID of the batch being currently processed,              e.g. 19285
echo "PROJECT_ID = ${PROJECT_ID}"       # this the the folder (${BASE}/${PROJECT_ID}) where the downstream analysis will be done
echo "VERSION_N = ${VERSION_N}"         # the version of the alignment and genotyping analysis




##############################################################
###   Delete indivdual BAMs (and indexes) iff CRAM found   ###
##############################################################

# make sure we are reading the data from the exact version, batch & plate ID
SOURCE_VCF_DIRS=${SOURCE_DIR}/${BATCH_NUM}_${VERSION_N}/families/????-??-??_${BATCH_NUM}_${VERSION_N}_${PLATE_ID}_*


for S_VCF_DIR in ${SOURCE_VCF_DIRS}
do
  VCF_DIR_NAME="${S_VCF_DIR##*/}"
  IFS=_ read -ra my_arr <<< "${VCF_DIR_NAME}"
  FAM_ID=${my_arr[-1]}
  echo "  FAM_ID = ${FAM_ID}"

  # identify all folders (one for each individual) for this family containing cram/bam files (format: <INDI_ID>_<FAM_ID>)
  cd ${SOURCE_DIR}/${BATCH_NUM}_${VERSION_N}/families/????-??-??_${BATCH_NUM}_${VERSION_N}_${PLATE_ID}_${FAM_ID}
  for ITEM in `ls -l`
  do
    if test -d $ITEM && [[ "$ITEM" == *"_"* ]]
    then
      echo "    $ITEM is a CRAM/BAM folder..."
      BAM=${ITEM}/${ITEM}-ready.bam
      CRAM=${ITEM}/${ITEM}-ready.cram

      #  check if the CRAM file exists, iff yes, delete the BAM file and its index
      if [[ -f "$CRAM" ]]
      then
        echo "      Found ${CRAM}"
        echo "      Removing ${BAM}"
        rm ${BAM}
        echo "      Removing ${BAM}.bai"
        rm ${BAM}.bai
      else
        echo "      ERROR: CRAM file ${CRAM} not found - have not deleted BAM ${BAM}!"
      fi
    fi
  done
done


echo ""
echo ""
echo "OK: Deletion of BAM files and their indexes for PROJECT_ID = $PROJECT_ID successful"
