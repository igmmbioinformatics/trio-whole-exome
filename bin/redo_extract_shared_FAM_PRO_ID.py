#	input:	the work folder which contains a PED subfolder where all family PED files were copied
#	output:	only for shared affected families
#		FAM_IDs.txt and FAM_PRO.txt
#
#       Author: MH
#       last modified: APR 27, 2022



import sys
import os
from collections import defaultdict


def go(work_dir):

    out_fam_file = work_dir + '/shared_FAM_IDs.txt'
    out_f_p_file = work_dir + '/shared_FAM_PRO.txt'

    out_fam_han = open(out_fam_file,'w')
    out_f_p_han = open(out_f_p_file,'w')

    cntr_fam = 0
    cntr_f_p = 0


    # go over the PED folder in the working dir and process each PED file
    ped_dir = work_dir + '/PED'
    print ""
    print "Processing the PED files (in %s) to extract FAM_ID and FAM_PRO files" % (ped_dir)

    for file in os.listdir(ped_dir):					# per each PED file
        if file.endswith(".ped") and (not file.endswith("full.ped")):	# to exclude the original PED files which have been stored as a backup at pre-processing
            print "  %s" % (file)
            in_file = os.path.join(ped_dir, file)
            in_han = open(in_file,'r')

            FAM_PRO_DICT = defaultdict(list)		# key: family_id: value: list of affected proband IDs

            for line in in_han:
                data = [x.strip() for x in line.strip().split('\t')]

                x_plate,x_fam = data[0].split('_')			# in the internal PED file, family_id is plateID_familyID, will keep only clean family_id, which corresponds to DECIPHER ID
                y_indi,y_fam = data[1].split('_')			# in the internal PED file, indi_id is indiID_familyID, split

                if x_fam != y_fam:
                    print "WARNING: family ID mismatch"
                    print line

                try:
                    PAR_1_ID = int(data[2])
                    PAR_2_ID = int(data[3])
                    if (PAR_1_ID != 0) or (PAR_2_ID  != 0):
                        print "WARNING: found a parent ID for proband in %s" % (in_file)
                        print line
                except:
                    print "WARNING: found a parent ID for proband in %s" % (in_file)
                    print line

                CHILD_SEX = int(data[4])
                if (CHILD_SEX == 1) or (CHILD_SEX == 2):
                    pass
                else:
                    print "WARNING: proband sex unknown"
                    print line

                CHILD_STAT = int(data[5])
                if CHILD_STAT != 2:
                    print "WARNING: proband not affected"
                    print line

                CHILD_ID = y_indi
                FAM_ID = y_fam

                FAM_PRO_DICT[FAM_ID].append(CHILD_ID)

            in_han.close()


            if len(FAM_PRO_DICT) != 1:
                print "WARNING: 0 or >1 family IDs found in %s" % (in_file)

            CHILD_IDS = FAM_PRO_DICT[list(FAM_PRO_DICT)[0]]
            if len(CHILD_IDS) < 2:
                print "WARNING: < 2 affected individuals were found in %s" % (in_file)


            # passed all checks, write it out
            out_fam_han.write('%s\n' % (list(FAM_PRO_DICT)[0]))
            cntr_fam += 1
            out_f_p_han.write('%s\t%s\n' % (list(FAM_PRO_DICT)[0],CHILD_IDS))
            cntr_f_p += 1

    out_fam_han.close()
    out_f_p_han.close()



    print ""
    print "Shared Affected Families:"
    print "   %s FAM_IDs --> %s" % (cntr_fam, out_fam_file)
    print "   %s FAM_PRO --> %s" % (cntr_f_p, out_f_p_file)
    print ""





if __name__ == '__main__':
    if len(sys.argv) == 2:
        go(sys.argv[1])
    else:
        print "Suggested use: time python /home/u035/u035/shared/scripts/extract_shared_FAM_PRO_ID.py /home/u035/u035/shared/analysis/work/<PROJECT_ID>"
        raise SystemExit

