#!/bin/bash
#SBATCH --cpus-per-task=16
#SBATCH --mem=2GB
#SBATCH --time=24:00:00
#SBATCH --job-name=reanalyse_setup_solo
#SBATCH --output=reanalyse_setup_solo.%A_%a.out
#SBATCH --error=reanalyse_setup_solo.%A_%a.err


#       Author: MH
#       last modified: AUG 09, 2022


###################################################################################################
### Creates the folder structure needed for prioritization                     		  	###
### Takes as an input the tab separated files containing info about *singleton* families	###
### called: singleton.txt in the format								###
### <BATCH_NUM>_<version> <plate_id> <family_id>, e.g. 11947_v2 \t 17173 \t 388320		###
### Copy the PED and VCF files for the *singleton* samples					###
### BAM to CRAM and BAM clean will be done on-the-fly for each family when processed		###
###################################################################################################


SCRIPTS_DIR=/home/u035/u035/shared/scripts/bin
#^#SCRIPTS_DIR=/home/u035/u035/shared/reanalysis_scripts



# check input given to script
echo "INPUT_DIR = ${INPUT_DIR}"		# the path to the bulk reanalysis folder with the links to source VCF and PED files,
					# e.g. /home/u035/u035/shared/results/20220418_reanalysis
echo "PROJECT_ID = ${PROJECT_ID}"	# this the the folder (${BASE}/${PROJECT_ID}) where the prioritization analysis will be done


### Tools
PYTHON2=/home/u035/u035/shared/software/bcbio/anaconda/envs/python2/bin/python2.7


### Setup the folder structure for the downstream analysis###
BASE=/home/u035/u035/shared/analysis/work
WORK_DIR=$BASE/${PROJECT_ID}
VCF_DIR=${WORK_DIR}/VCF
PED_DIR=${WORK_DIR}/PED
LOG_DIR=${WORK_DIR}/LOG
G2P_DIR=${WORK_DIR}/G2P
VASE_DIR=${WORK_DIR}/VASE
COV_DIR=${WORK_DIR}/COV
DEC_DIR=${WORK_DIR}/DECIPHER
IGV_DIR=${DEC_DIR}/IGV
BAM_DIR=${WORK_DIR}/BAM
BAMOUT_DIR=${WORK_DIR}/BAMOUT


# create the working dir and the required subfolders
mkdir ${WORK_DIR}
mkdir ${VCF_DIR}
mkdir ${PED_DIR}
mkdir ${LOG_DIR}
mkdir ${G2P_DIR}
mkdir ${VASE_DIR}
mkdir ${COV_DIR}
mkdir ${DEC_DIR}
mkdir ${IGV_DIR}
mkdir ${BAM_DIR}
mkdir ${BAMOUT_DIR}
echo "Created ${WORK_DIR} for this batch and all the required subfolders"




##############################################
###   Copy the PED file per each family    ###
##############################################
echo ""
echo "Copying the singleton PED files ..."
while IFS=$'\t' read -r -a myArray
do
#    echo "${myArray[0]}"
#    echo "${myArray[1]}"
#    echo "${myArray[2]}"
    PED_FILE=${INPUT_DIR}/params/singleton/*_${myArray[2]}.ped
    cp ${PED_FILE} ${PED_DIR}
done < ${INPUT_DIR}/params/singleton.txt
echo "... singletons PED files copied"
echo ""




##############################################
###   Copy the VCF file per each family    ###
##############################################
echo ""
echo "Copying the singleton VCF files ..."
while IFS=$'\t' read -r -a myArray
do
#    echo "${myArray[0]}"
#    echo "${myArray[1]}"
#    echo "${myArray[2]}"
    VCF_FILE=${INPUT_DIR}/families/*_${myArray[2]}/*_${myArray[2]}-gatk-haplotype-annotated.vcf.gz
    cp ${VCF_FILE} ${VCF_DIR}
done < ${INPUT_DIR}/params/singleton.txt
echo "... singletons VCF files copied"
echo ""




###########################################################################################
### generate the FAM_IDs.txt, PRO_IDs.txt and FAM_PRO.txt *only for singleton* families ###
###########################################################################################

time ${PYTHON2} ${SCRIPTS_DIR}/extract_solo_FAM_PRO_ID.py ${WORK_DIR}


echo ""
echo ""
echo "OK: Setup for *singleton* PROJECT_ID = $PROJECT_ID successful"






