#!/bin/bash
#
# trio_wes_prepare_bcbio_singleton_from_duo_config.sh <config.sh> <project_id> <version>
# 
# All samples must be annotated with sex (1=male, 2=female) in the
# 5th column and phenotype (1=unaffected, 2=affected) in the 6th
# column of the PED file.
#
# Assumes the duo config and ped file exists with the original family names (i.e. no duo suffix).
#

CONFIG_SH=$1
PROJECT_ID=$2
VERSION=$3

source $CONFIG_SH

cd $PARAMS_DIR

SHORT_PROJECT_ID=`echo $PROJECT_ID | cut -f 1 -d '_'`
COUNT=`wc -l ${PROJECT_ID}.singleton_from_duo.txt | awk '{ print $1 }'`

for ((i = 1; i <= $COUNT; i = i + 1))
do
    FAMILY_ID=`head -n $i ${PROJECT_ID}.singleton_from_duo.txt | tail -n 1`
    PROBAND_ID=`grep 2$ ${PROJECT_ID}_${FAMILY_ID}.ped | cut -f 2`

    # rename the original PED file with duo suffix
    mv ${PROJECT_ID}_${FAMILY_ID}.ped ${PROJECT_ID}_${FAMILY_ID}duo.ped

    # create the proband PED file - set parent ids to 0
    grep $PROBAND_ID ${PROJECT_ID}_${FAMILY_ID}duo.ped | awk '{ print $1 "\t" $2 "\t0\t0\t" $5 "\t" $6 }' > ${PROJECT_ID}_${FAMILY_ID}.ped

    # move into the config folder and rename the original YAML file with duo suffix
    cd $CONFIG_DIR
    mv ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}.yaml ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}duo.yaml

    # create the singleton config file from the duo file
    head -n 1 ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}duo.yaml > ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}.yaml
    affected=`grep -n 'phenotype: 2' ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}duo.yaml | cut -f 1 -d ':'`
    head -n $((affected + 1)) ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}duo.yaml | tail -n 30 >> ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}.yaml
    tail -n 6 ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}duo.yaml >> ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}.yaml

    # change family id to add duo suffix for duo YAML
    perl -pi -e "s/${FAMILY_ID}/${FAMILY_ID}duo/" ${SHORT_PROJECT_ID}_${VERSION}_${FAMILY_ID}duo.yaml

    # move back to params folder
    cd $PARAMS_DIR
done
