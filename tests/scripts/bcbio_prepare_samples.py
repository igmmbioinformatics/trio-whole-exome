#!/usr/bin/env python
"""
Fake BCBio prepare script for testing with the empty datasets from tests/assets/
"""

import os
import argparse

def main():
    a = argparse.ArgumentParser()
    a.add_argument('--out')
    a.add_argument('--csv')
    args = a.parse_args()

    open(args.csv, 'w').close()


if __name__ == '__main__':
    main()
