#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=16GB
#SBATCH --time=24:00:00
#SBATCH --job-name=compute_DDG2P_cov
#SBATCH --output=compute_DDG2P_cov.%A_%a.out
#SBATCH --error=compute_DDG2P_cov.%A_%a.err


#       Author: MH
#       last modified: FEB 17, 2023


# setup PATH
export PATH=$PATH:/home/u035/u035/shared/software/bcbio/anaconda/envs/python2/bin:/home/u035/u035/shared/software/bcbio/anaconda/bin
export PERL5LIB=$PERL5LIB:/home/u035/u035/shared/software/bcbio/anaconda/lib/site_perl/5.26.2


# The current DDG2P gene panel BED file
TARGETS=/home/u035/u035/shared/resources/G2P/DDG2P.20221207.plus15bp.merged.bed

GATK3=/home/u035/u035/shared/software/GenomeAnalysisTK-3.8/GenomeAnalysisTK.jar
SAMTOOLS=/home/u035/u035/shared/software/bcbio/anaconda/bin/samtools
PICARD=/home/u035/u035/shared/software/bcbio/anaconda/bin/picard
REFERENCE_GENOME=/home/u035/u035/shared/software/bcbio/genomes/Hsapiens/hg38/seq/hg38.fa




# echo "CRAM_LIST = ${CRAM_LIST}"


################################
#####  for each CRAM file   ####
################################
CRAM_FILE=`head -n ${SLURM_ARRAY_TASK_ID} ${CRAM_LIST} | tail -n 1`

# get the ID (NHSS)
IFS='/' read -r -a array <<< "${CRAM_FILE}"
ID="${array[8]}"

# convert the CRAM to BAM
BAM_FILE=${ID}-ready.bam
time ${SAMTOOLS} view -@ 16 -T ${REFERENCE_GENOME} -hb -o ${BAM_FILE} ${CRAM_FILE}
time ${PICARD} BuildBamIndex -I ${BAM_FILE} -O ${BAM_FILE}.bai -USE_JDK_DEFLATER true -USE_JDK_INFLATER true -VERBOSITY ERROR -QUIET true

# generate the name of the output file (NHSS)
OUT_FILE=${ID}.DDG2P.COV

time java -Xmx8g -jar ${GATK3} -T DepthOfCoverage -R ${REFERENCE_GENOME} -o ${OUT_FILE} -I ${BAM_FILE} -L ${TARGETS} \
  --omitDepthOutputAtEachBase \
  --minBaseQuality 20 \
  --minMappingQuality 20 \
  -ct 1 \
  -ct 5 \
  -ct 10 \
  -ct 15 \
  -ct 20 \
  -ct 25 \
  -ct 30 \
  -ct 50 \
  -jdk_deflater \
  -jdk_inflater \
  --allow_potentially_misencoded_quality_scores


