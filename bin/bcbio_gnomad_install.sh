#!/bin/bash
#PBS -l walltime=96:00:00
#PBS -l ncpus=1,mem=128gb
#PBS -q uv2000
#PBS -N bcbio_gnomad_install
#PBS -j oe

cd /home/u035/u035/shared/software/bcbio/genomes/Hsapiens/hg38/txtmp

PATH=$PATH:/home/u035/u035/shared/software/bcbio/anaconda/bin

ref=../seq/hg38.fa
fields_to_keep="INFO/"$(cat gnomad_fields_to_keep.txt | paste -s | sed s/"\t"/",INFO\/"/g)

bcftools view -f PASS gnomad.genomes.r3.0.sites.vcf.bgz | bcftools annotate -x "^$fields_to_keep" -Ov | vt decompose -s - | vt normalize -r $ref -n - | vt uniq - | bgzip -c > variation/gnomad_genome.vcf.gz

tabix -f -p vcf variation/gnomad_genome.vcf.gz
tabix -f -p vcf --csi variation/gnomad_genome.vcf.gz

