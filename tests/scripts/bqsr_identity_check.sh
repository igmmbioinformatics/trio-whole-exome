#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=10GB
#SBATCH --time=48:00:00
#SBATCH --job-name=bqsr_test
#SBATCH --output=bqsr_test.out
#SBATCH --error=bqsr_test.err

SAMTOOLS=/home/u035/u035/shared/software/bcbio/anaconda/bin/samtools

#The outputs of bcbio and nf-core bqsr initially appear different
#However, this can be accounted for by arbitrary ordering of reads with identical coordinates
#They can be made identical by a sort operation - samtools sort is not suitable as they are already adequately sorted by samtools' definition

#First, convert to sam

#bcbio recal bam
$SAMTOOLS view 158063_519317-sort-recal.bcbio.bam > 158063_519317-sort-recal.bcbio.sam

#nf-core recal bam
$SAMTOOLS view 158063_519317-recal.nf-core.bam > 158063_519317-recal.nf-core.sam

#Sort the sam files using bash sort to confirm that they are indentical
sort 158063_519317-recal.nf-core.sort.sam > 158063_519317-recal.nf-core.bashsort.sam 
sort 158063_519317-sort-recal.bcbio.sort.sam > 158063_519317-sort-recal.bcbio.bashsort.sam  

#Generate md5sums - these should be identical
md5sum 158063_519317-recal.nf-core.bashsort.sam > 158063_519317-recal.nf-core.bashsort.sam.md5
md5sum 158063_519317-sort-recal.bcbio.bashsort.sam > 158063_519317-sort-recal.bcbio.bashsort.sam.md5


