Urgent sequencing run on NextSeq 2000 from CRF

* family 438214, father is affected - trio based & shared variant analysis

# Trio analysis

## Set up

```
git reset --hard 06a48fb115f1a446a2b855cfa96d3b59bedf06bf
```

Changed father for 20211005_Ansari_Morad_19373_438214 to unaffected in params family PED file

```
export SOURCE_DIR=/scratch/u035/project/trio_whole_exome/analysis/output
export BATCH_ID=20211005_Ansari_Morad
export BATCH_NUM=20211005
export PLATE_ID=19373
export VERSION_N=v1
export PROJECT_ID=20211006_19373
export SOURCE_DIR=${SOURCE_DIR}/${VERSION_N}_${BATCH_NUM}
/home/u035/project/scripts/NHS_WES_trio_setup.sh &> v1_20211005_Ansari_Morad.NHS_WES_trio_setup.log
```

DECIPHER_INTERNAL_IDs.txt created manually from Excel sheet from Morad

## Trio prioritization

```
SOURCE_DIR=/scratch/u035/project/trio_whole_exome/analysis/output
BATCH_ID=20211005_Ansari_Morad
BATCH_NUM=20211005
PLATE_ID=19373
VERSION_N=v1
PROJECT_ID=20211006_19373

qsub -v SOURCE_DIR=${SOURCE_DIR}/${VERSION_N}_${BATCH_NUM},BATCH_ID=${BATCH_ID},PLATE_ID=${PLATE_ID},PROJECT_ID=${PROJECT_ID},VERSION_N=${VERSION_N} -J 1-4 /home/u035/project/scripts/process_NHS_WES_trio.sh
```

## IGV snapshots

`/home/u035/ameynert/igv/prefs.properties`

```
SAM.SHADE_BASE_QUALITY=true
SAM.GROUP_OPTION=SAMPLE
SAM.DOWNSAMPLE_READS=false
SAM.SORT_OPTION=STRAND
SAM.COLOR_BY=READ_STRAND
LAST_TRACK_DIRECTORY=/scratch/u035/project/trio_whole_exome/analysis/20211006_19373/DECIPHER/IGV
DEFAULT_GENOME_KEY=hg38
SAM.SHOW_CENTER_LINE=true

##RNA
SAM.SHADE_BASE_QUALITY=true
SAM.SORT_OPTION=STRAND

##THIRD_GEN
SAM.SHADE_BASE_QUALITY=true
SAM.SORT_OPTION=STRAND
```

```
cd /scratch/u035/project/trio_whole_exome/analysis/20211006_19373/DECIPHER/IGV
cat *.snapshot.FLT.txt > batch.snapshots.txt
cat bamout_*.snapshot.txt > batch.bamout.snapshots.txt
```

```
/home/u035/project/software/IGV_Linux_2.8.9/igv.sh
```

* View > Preferences > Alignments - make sure “Downsample reads” is unchecked
* View > Preferences > Alignments – make sure “Show center line” is checked
* run Tools > Run Batch Scripts for the whole batch – first the one based on the original, then the one based on the realigned BAMs (order is important)

## Gather up results

```
PLATE_ID=19373
PROJECT_ID=20211006_19373
VERSION_N=v1
cd /scratch/u035/project/trio_whole_exome/analysis/${PROJECT_ID}
mkdir ${PLATE_ID}_${VERSION_N}_results
cd LOG
qsub -v PLATE_ID=${PLATE_ID},PROJECT_ID=${PROJECT_ID},VERSION_N=${VERSION_N} -J 1-4 /home/u035/project/scripts/gather_NHS_WES_trio_results.sh
```

# Shared affected analysis

Changed father for 20211005_Ansari_Morad_19373_438214 back to affected in params family PED file and in PED version.

## Set-up

```
export SOURCE_DIR=/scratch/u035/project/trio_whole_exome/analysis/output
export BATCH_ID=20211005_Ansari_Morad
export BATCH_NUM=20211005
export PLATE_ID=19373
export VERSION_N=v1
export PROJECT_ID=20211006_19373_438214
export SOURCE_DIR=${SOURCE_DIR}/${VERSION_N}_${BATCH_NUM}
/home/u035/project/scripts/NHS_WES_trio_setup.sh &> v1_20211005_Ansari_Morad.NHS_WES_trio_setup.log
mv v1_20211005_Ansari_Morad.NHS_WES_trio_setup.log ../20211006_19373_438214/LOG/
```

## Remove other families & copy DECIPHER id map

```
cd ../20211006_19373_438214
cp ../20211006_19373/DECIPHER_INTERNAL_IDs.txt ./
```

Edit `DECIPHER_INTERNAL_IDs.txt` to remove other families.

```
cd PED
rm 20211005_Ansari_Morad_19373_438169.ped 20211005_Ansari_Morad_19373_438236.ped 20211005_Ansari_Morad_19373_451595.ped
cd ../VCF/
rm 19373_438169-gatk-haplotype-annotated.vcf.gz 19373_438236-gatk-haplotype-annotated.vcf.gz 19373_451595-gatk-haplotype-annotated.vcf.gz 
```

## Pre-processing

```
BATCH_ID=20211005_Ansari_Morad
PLATE_ID=19373
FAMILY_ID=438214
cd /scratch/u035/project/trio_whole_exome/analysis/20211006_19373_438214/PED
cp ${BATCH_ID}_${PLATE_ID}_${FAMILY_ID}.ped ${BATCH_ID}_${PLATE_ID}_${FAMILY_ID}.full.ped
```

Edited PED file to only affected individuals (child & father)

```
19373_438214    130221_438214   0   0   1	2
19373_438214    130220_438214   0	0	1	2
```

Copy and pre-process VCF files

```
cd ../VCF
mv 19373_438214-gatk-haplotype-annotated.vcf.gz 19373_438214-gatk-haplotype-annotated.full.vcf.gz
tabix 19373_438214-gatk-haplotype-annotated.vcf.gz

GATK3=/home/u035/project/software/GenomeAnalysisTK-3.8/GenomeAnalysisTK.jar
REF_GENOME=/home/u035/project/resources/hg38.fa
aff_kid_1_id=130221
aff_kid_2_id=130220

time java -Xmx24g -jar ${GATK3} -T SelectVariants \
-R ${REF_GENOME} \
-V ${PLATE_ID}_${FAMILY_ID}-gatk-haplotype-annotated.full.vcf.gz \
-sn ${aff_kid_1_id}_${FAMILY_ID} \
-sn ${aff_kid_2_id}_${FAMILY_ID} \
-jdk_deflater \
-jdk_inflater \
-o ${PLATE_ID}_${FAMILY_ID}-gatk-haplotype-annotated.vcf.gz  \
-env
```

## Shared affected prioritization

```
SOURCE_DIR=/scratch/u035/project/trio_whole_exome/analysis/output
BATCH_ID=20211005_Ansari_Morad
BATCH_NUM=20211005
PLATE_ID=19373
PROJECT_ID=20211006_19373_438214
VERSION_N=v1
FAMILY_ID=438214
DECIPHER_ID=62137_3387_mat

cd /scratch/u035/project/trio_whole_exome/analysis/${PROJECT_ID}/LOG
qsub -v SOURCE_DIR=${SOURCE_DIR}/${VERSION_N}_${BATCH_NUM},BATCH_ID=${BATCH_ID},PLATE_ID=${PLATE_ID},PROJECT_ID=${PROJECT_ID},VERSION_N=${VERSION_N},FAMILY_ID=${FAMILY_ID},DECIPHER_ID=${DECIPHER_ID} /home/u035/project/scripts/process_NHS_WES_aff_probands.sh 
```

Job id 14967

## IGV snapshots

```
/home/u035/project/software/IGV_Linux_2.8.9/igv.sh
```

* View > Preferences > Alignments - make sure “Downsample reads” is unchecked
* View > Preferences > Alignments – make sure “Show center line” is checked
* run Tools > Run Batch Script, for each family

## Gather results

```
PLATE_ID=19373
PROJECT_ID=20211006_19373_438214
VERSION_N=v1
FAMILY_ID=438214
JOB_ID=14966
cd /scratch/u035/project/trio_whole_exome/analysis/${PROJECT_ID}/LOG
mkdir /scratch/u035/project/trio_whole_exome/analysis/${PROJECT_ID}/${PLATE_ID}_${VERSION_N}_results
qsub -v PLATE_ID=${PLATE_ID},PROJECT_ID=${PROJECT_ID},VERSION_N=${VERSION_N},FAMILY_ID=${FAMILY_ID},JOB_ID=${JOB_ID} /home/u035/project/scripts/gather_NHS_WES_aff_probands_results.sh
```

# Copy directly to encrypted USB

```
/scratch/u035/project/trio_whole_exome/analysis/20211006_19373/19373_v1_results/*
/scratch/u035/project/trio_whole_exome/analysis/20211006_19373_438214/19373_v1_results/*
```

# Trio analysis - re-run

This time don't change the father to unaffected in the PED file for 438214

## Set up

```
git reset --hard 06a48fb115f1a446a2b855cfa96d3b59bedf06bf
```

```
export SOURCE_DIR=/scratch/u035/project/trio_whole_exome/analysis/output
export BATCH_ID=20211005_Ansari_Morad
export BATCH_NUM=20211005
export PLATE_ID=19373
export VERSION_N=v1
export PROJECT_ID=20211006_19373a
export SOURCE_DIR=${SOURCE_DIR}/${VERSION_N}_${BATCH_NUM}
/home/u035/project/scripts/NHS_WES_trio_setup.sh &> v1_20211005_Ansari_Morad.NHS_WES_trio_setup.log
```

DECIPHER_INTERNAL_IDs.txt copied from `20211006_19373_438214`

```
cd PED
rm 20211005_Ansari_Morad_19373_438169.ped 20211005_Ansari_Morad_19373_438236.ped 20211005_Ansari_Morad_19373_451595.ped
cd ../VCF/
rm 19373_438169-gatk-haplotype-annotated.vcf.gz 19373_438236-gatk-haplotype-annotated.vcf.gz 19373_451595-gatk-haplotype-annotated.vcf.gz 
```

## Trio prioritization

```
SOURCE_DIR=/scratch/u035/project/trio_whole_exome/analysis/output
BATCH_ID=20211005_Ansari_Morad
BATCH_NUM=20211005
PLATE_ID=19373
VERSION_N=v1
PROJECT_ID=20211006_19373a

qsub -v SOURCE_DIR=${SOURCE_DIR}/${VERSION_N}_${BATCH_NUM},BATCH_ID=${BATCH_ID},PLATE_ID=${PLATE_ID},PROJECT_ID=${PROJECT_ID},VERSION_N=${VERSION_N},INDEX=1 /home/u035/project/scripts/process_NHS_WES_trio.sh
```

## IGV snapshots

`/home/u035/ameynert/igv/prefs.properties`

```
SAM.SHADE_BASE_QUALITY=true
SAM.GROUP_OPTION=SAMPLE
SAM.DOWNSAMPLE_READS=false
SAM.SORT_OPTION=STRAND
SAM.COLOR_BY=READ_STRAND
LAST_TRACK_DIRECTORY=/scratch/u035/project/trio_whole_exome/analysis/20211006_19373/DECIPHER/IGV
DEFAULT_GENOME_KEY=hg38
SAM.SHOW_CENTER_LINE=true

##RNA
SAM.SHADE_BASE_QUALITY=true
SAM.SORT_OPTION=STRAND

##THIRD_GEN
SAM.SHADE_BASE_QUALITY=true
SAM.SORT_OPTION=STRAND
```

```
cd /scratch/u035/project/trio_whole_exome/analysis/20211006_19373a/DECIPHER/IGV
cat *.snapshot.FLT.txt > batch.snapshots.txt
cat bamout_*.snapshot.txt > batch.bamout.snapshots.txt
```

```
/home/u035/project/software/IGV_Linux_2.8.9/igv.sh
```

* View > Preferences > Alignments - make sure “Downsample reads” is unchecked
* View > Preferences > Alignments – make sure “Show center line” is checked
* run Tools > Run Batch Scripts for the whole batch – first the one based on the original, then the one based on the realigned BAMs (order is important)

## Gather up results

```
PLATE_ID=19373
PROJECT_ID=20211006_19373a
VERSION_N=v1
cd /scratch/u035/project/trio_whole_exome/analysis/${PROJECT_ID}
mkdir ${PLATE_ID}_${VERSION_N}_results
cd LOG
qsub -v PLATE_ID=${PLATE_ID},PROJECT_ID=${PROJECT_ID},VERSION_N=${VERSION_N} -J 1-4 /home/u035/project/scripts/gather_NHS_WES_trio_results.sh
```
