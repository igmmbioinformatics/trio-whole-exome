#!/bin/bash
#SBATCH --cpus-per-task=1
#SBATCH --mem=2GB
#SBATCH --time=01:00:00
#SBATCH --job-name=redo_hunt_novel_quad
#SBATCH --output=redo_hunt_novel_quad.%A_%a.out
#SBATCH --error=redo_hunt_novel_quad.%A_%a.err

#       Author: MH
#       last modified: AUG 29, 2023


SCRIPTS_DIR=/home/u035/u035/shared/scripts/bin
#^#SCRIPTS_DIR=/home/u035/u035/shared/rerun_bulk


PYTHON3=/home/u035/u035/shared/software/bcbio/anaconda/bin/python3




# check input given to script
echo "INPUT_DIR = ${INPUT_DIR}"         # the path to the bulk reanalysis folder with the links to source VCF and PED files,
                                        # e.g. /home/u035/u035/shared/results/20220418_reanalysis
echo "PROJECT_ID = ${PROJECT_ID}"       # this the the folder (${BASE}/${PROJECT_ID}) where the prioritization analysis will be done


### folder structure for the downstream analysis - created by redo_setup_quad.sh ###
BASE=/home/u035/u035/shared/analysis/work
WORK_DIR=${BASE}/${PROJECT_ID}
NHS_DIR=${WORK_DIR}/results


# other files to be used
FAMILY_IDS=${WORK_DIR}/quad_FAM_IDs.txt						# created by redo_setup_quad.sh
FAM_PRO_FILE=${WORK_DIR}/quad_FAM_PRO.txt					# created by redo_setup_quad.sh


OLD_DEC_DIR=${WORK_DIR}/previous_DECIPHER_upload
NEW_DEC_DIR=${WORK_DIR}/DECIPHER




FAMILY_ID=`head -n ${SLURM_ARRAY_TASK_ID} ${FAMILY_IDS} | tail -n 1`			# contains only the family IDs (e.g.385295)
FAM_DIR=${NHS_DIR}/${FAMILY_ID}								# already created by redo_gather_quad_results.sh



# check if novel variants are found during the reanalysis
#time ${PYTHON3} ${SCRIPTS_DIR}/redo_hunt_novel_shared_vars.py ${OLD_DEC_DIR} ${NEW_DEC_DIR} ${FAMILY_ID}
time ${PYTHON3} ${SCRIPTS_DIR}/redo_hunt_novel_quad_vars.py ${OLD_DEC_DIR} ${NEW_DEC_DIR} ${FAM_PRO_FILE} ${FAMILY_ID}



## copy the novel DECIPHER file into the "results" folder (even if empty)
cp ${WORK_DIR}/DECIPHER/novel_*_${FAMILY_ID}_*DECIPHER_v11.xlsx ${FAM_DIR}


## copy the LOG files
chmod 644 ${WORK_DIR}/LOG/redo_hunt_novel_quad.*_${SLURM_ARRAY_TASK_ID}.*
cp ${WORK_DIR}/LOG/redo_hunt_novel_quad.*_${SLURM_ARRAY_TASK_ID}.err ${FAM_DIR}
cp ${WORK_DIR}/LOG/redo_hunt_novel_quad.*_${SLURM_ARRAY_TASK_ID}.out ${FAM_DIR}




echo ""
echo ""
echo "OK: Results for novel variants found in ${FAMILY_ID} during re-analysis are stored in ${FAM_DIR}"

