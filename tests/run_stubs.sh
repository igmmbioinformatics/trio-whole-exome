#!/bin/bash

source scripts/nextflow_detached.sh
bcbio=$PWD/scripts/bcbio_nextgen.py

common_args="-stub-run -profile testing,stubs -ansi-log false"
test_exit_status=0

rm -r work/*/*

echo "--- Test case 1: simple trio ---" &&
echo "Variant calling" &&
run_nextflow ../main.nf \
    $common_args \
    --workflow variant_calling \
    --pipeline_project_id test_stub \
    --ped_file assets/input_data/ped_files/batch_1.ped \
    --sample_sheet assets/input_data/sample_sheets/batch_1.tsv &&

echo "Variant prioritisation setup" &&
run_nextflow ../main.nf \
    $common_args \
    --workflow variant_prioritisation_setup \
    --pipeline_project_id test_stub \
    --variant_prioritisation_version v1 \
    --ped_file outputs/test_stub_v1/params/batch_1.ped \
    --exclude_families 12345_000003,12345_000004 &&

cp assets/input_data/sample_sheets/DECIPHER_INTERNAL_IDs_stubs.txt outputs/test_stub_v1/prioritization/v1/DECIPHER_INTERNAL_IDs.txt &&

echo "Variant prioritisation" &&
run_nextflow ../main.nf \
    $common_args \
    --workflow variant_prioritisation \
    --pipeline_project_id test_stub \
    --variant_prioritisation_version v1 \
    --ped_file outputs/test_stub_v1/params/batch_1.ped

output_dir='outputs/test_stub_v1/prioritization/v1'

for pro_fam in 000001_000001 000004_000002
do
    for ext in DD15.COV.txt REC_SNP_COV.txt DD15.sample_{cumulative_coverage_{counts,proportions},interval_{statistics,summary},statistics,summary}
    do
        ls $output_dir/COV/$pro_fam.$ext
    done

    ls $output_dir/DECIPHER/${pro_fam}_{DEC_FLT.csv,DECIPHER_v10.xlsx}
    ls $output_dir/DECIPHER/IGV/$pro_fam.snapshot.FLT.txt
    ls $output_dir/DECIPHER/IGV/bamout_$pro_fam.snapshot.txt
done

for fam_id in 000001 000002
do
    ls $output_dir/BAMOUT/${fam_id}/${fam_id}_chr{2,4,7,16,22,X}_12345678.bamout.{bam,bai,vcf,vcf.idx}
    ls $output_dir/G2P/12345_${fam_id}_LOG_DIR/12345_${fam_id}_inter_out.txt{,_summary.html}
    ls $output_dir/G2P/12345_${fam_id}_LOG_DIR/12345_${fam_id}.report.{html,txt}
    ls $output_dir/VCF/12345_${fam_id}.ready.vcf.gz
    ls $output_dir/VASE/12345_${fam_id}.ready.denovo.vcf
done

ls $output_dir/{DECIPHER_INTERNAL_IDS,{FAM,PRO}_IDs,FAM_PRO}.txt
ls $output_dir/VCF/12345_000001.ready{,.00000{1,2,3}_000001}.vcf.gz
ls $output_dir/VCF/12345_000002.ready{,.00000{4,5,6}_000002}.vcf.gz

# should be bams
! ls outputs/test_stub_v1/families/*/*/*.cram &&
ls outputs/test_stub_v1/families/*/*/*.bam &&

echo "Cram compression" &&
run_nextflow ../main.nf \
    $common_args \
    --workflow cram_compression \
    --compress_dir outputs/test_stub_v1/families &&

# should be crams, not bams
! ls outputs/test_stub_v1/families/*/*/*.bam &> /dev/null &&
ls outputs/test_stub_v1/families/*/*/*.cram &> /dev/null &&
echo "Cram decompression" &&
run_nextflow ../main.nf \
    $common_args \
    --workflow cram_decompression \
    --compress_dir outputs/test_stub_v1/families &&

# should be bams again
! ls outputs/test_stub_v1/families/*/*/*.cram &> /dev/null &&
ls outputs/test_stub_v1/families/*/*/*.bam &> /dev/null

test_case_exit_status=$?
echo "Test case exited with status $test_case_exit_status"
test_exit_status=$(( $test_exit_status + $test_case_exit_status ))

echo "--- Test case 2: MD5 errors ---" &&
run_nextflow ../main.nf \
    $common_args \
    --workflow variant_calling \
    --pipeline_project_id test_stub_md5_errors \
    --pipeline_project_version v1 \
    --ped_file assets/input_data/ped_files/batch_2_md5_errors.ped \
    --sample_sheet assets/input_data/sample_sheets/batch_2_md5_errors.tsv

test_case_exit_status=$?
echo "Test case exited with status $test_case_exit_status"
if [ $test_case_exit_status -eq 0 ]
then
    test_exit_status=$(( $test_exit_status + 1 ))
fi

echo ""
echo "Tests finished with exit status $test_exit_status"
exit $test_exit_status
